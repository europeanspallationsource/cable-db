#!/usr/bin/python
#
#Requirements to run this script : python and pg8000
#To install pg8000 : pip install pg8000
#
import pg8000 as pg
from config import *

print('Connecting do database')

#Connection settings are in config.py
connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()

print('DELETING...')
print('DELETING CABLE-OWNERS')
cursor.execute("TRUNCATE cable_owners CASCADE")
print('DELETING CABLES')
cursor.execute("TRUNCATE cable CASCADE")
print('DELETING ENDPOINTS')
cursor.execute("TRUNCATE endpoint CASCADE")
print('DELETING CABLETYPEs')
cursor.execute("TRUNCATE cabletype CASCADE")
print('DELETING ROUTING ROWS')
cursor.execute("TRUNCATE routingrow CASCADE")
print('DELETING ROUTINGS')
cursor.execute("TRUNCATE routing CASCADE")
print('DELETING CONNECTORS')
cursor.execute("TRUNCATE connector CASCADE")
print('DELETING ARTIFACTS')
cursor.execute("TRUNCATE artifact CASCADE")
print('DELETING MANUFACTURERS')
cursor.execute("TRUNCATE manufacturer CASCADE")

connection.commit()
print('Closing connection...')

cursor.close()
connection.close()

print('Done')


################################################
#This is code for cleaning only data created by populate script. it is not maintained, and is here only in case of a redesign of a cleanup script.
# import pg8000 as pg
# from cableDBStressTestClasses import *
# from config import *
# 
# #clears data from database
# 
# owner = "test" # used to prevent loosing data
# 
# 
# #####################
# ####MANUFACTURERS####
# #####################
# manufacturerNames = []
# for i in range(manufacturers):
#     manufacturerNames.append(Manufacturer(i).name)
# ################
# ####ROUTINGS####
# ################
# 
# routingNames = []
# for i in range(routings):
#     routingNames.append(Routing(i, owner).name)
# ##################
# ####CONNECTORS####
# ##################
# connectorNames = []
# for i in range(connectors):
#     connectorNames.append(Connector(i).name)
# 
#     
# #################
# ####ARTIFACTS####
# #################
# artifactNames = []
# for i in range(artifacts):
#     artifactNames.append(Artifact(i, owner).name)
#     
# ##################
# ####CABLETYPES####
# ##################
# cabletypeNames = []
# for i in range(cableTypes):
#     cabletypeNames.append(CableType(i, 0, 0).name)
# 
# #################
# ####ENDPOINTS####
# #################
# endpointBuildings = []
# for i in range(endpoints):
#     endpointBuildings.append(Endpoint(i, 0).building)
#         
# 
# ##############
# ####CABLES####
# ##############
# 
# cableSeqnumbers = []
# for i in range(cables):
#     cableSeqnumbers.append(Cable(i , 0, 0, 0, 0, owner).seqnumber)    
# 
# ###############
# ####DATABSE####
# ###############
# print('Connecting do database')
# connection = pg.connect(user = "cabledb_dev", password = "cabledb_dev", host = "localhost", port = 5432, database = "cabledb_dev")
# cursor = connection.cursor()
# 
# print('Getting cable IDs')
# cursor.execute("SELECT id FROM cable WHERE owner = '" + owner + "' AND cableclass = '"+ Cable.cableClass +  "' AND seqnumber IN ('" + "', '".join(cableSeqnumbers) + "')")
# cableIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# print('Getting endpoint IDs')
# cursor.execute("SELECT id FROM endpoint WHERE building IN ('" + "', '".join(endpointBuildings) + "')")
# endpointIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# print('Getting cableType IDs')
# cursor.execute("SELECT id FROM cabletype WHERE name IN ('" + "', '".join(cabletypeNames) + "')")
# cableTypeIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# 
# 
# print('DELETING...')
# print('DELETING ROUTING ROWS')
# if cableIDs:
#     cursor.execute("DELETE FROM routingrow WHERE cable IN ('" + "', '".join(cableIDs) + "')")
# print('DELETING CABLES')
# if cableIDs:
#     cursor.execute("DELETE FROM cable WHERE id IN ('" + "', '".join(cableIDs) + "')")
# print('DELETING ENDPOINTS')
# if endpointIDs:
#     cursor.execute("DELETE FROM endpoint WHERE id IN ('" + "', '".join(endpointIDs) + "')")
# print('DELETING CABLETYPEs')
# if cableTypeIDs:
#     cursor.execute("DELETE FROM cabletype WHERE id IN ('" + "', '".join(cableTypeIDs) + "')")
# print('DELETING ROUTINGS')1
# if routingNames:
#     cursor.execute("DELETE FROM routing WHERE owner = '" + owner + "' AND name IN ('" + "', '".join(routingNames) + "')")
# print('DELETING CONNECTORS')
# if connectorNames:
#     cursor.execute("DELETE FROM connector WHERE name IN ('" + "', '".join(connectorNames) + "')")
# print('DELETING ARTIFACTS')
# if artifactNames:
#     cursor.execute("DELETE FROM artifact WHERE name IN ('" + "', '".join(artifactNames) + "')")
# print('DELETING MANUFACTURERS')
# if manufacturerNames:
#     cursor.execute("DELETE FROM manufacturer WHERE name IN ('" + "', '".join(manufacturerNames) + "')")
# 
# connection.commit()
# print('Closing connection...')
# 
# cursor.close()
# connection.close()
# 
# print('Done')
