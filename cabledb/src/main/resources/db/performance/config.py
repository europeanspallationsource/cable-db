#Settings for database stress testing
#
#####################################
#Amount of population data settings
# Minimum amount of any imported entity is 1.
cablesN = 50000
cableTypesN = cablesN//100
connectorsN = cablesN//100
manufacturersN = cablesN//1000
routingsN = cablesN//100
percentOfCableTypesWithArtifacts = 100# percentage of cables types with artifacts
percentOfCablesWithArtifacts = 50 # percentage of cables with artifacts
endpointsPerCable = 2   #amount of endpoints per cable  
routingRowsPerCable = 5 #amount of routings per cable
#Database connection settings
databaseUser = 'cabledb'
databasePassword = 'cabledb'
databaseHost = 'localhost'
databasePort = 5432
databaseName = 'cabledb'