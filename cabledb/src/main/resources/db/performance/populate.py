#!/usr/bin/python
#
#Requirements to run this script : python and pg8000
#To install pg8000 : pip install pg8000
#
import pg8000 as pg
from classes import *
from config import *

#######################################
###All user settings are in config.py##
#######################################
populateInParts = 10

owner_name = "test" # also used when deleting to prevent loosing data
print('Starting population')
print('Populating support entities')
print('    Connecting do database')
connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()
##################
####CONNECTORS####
##################
print('    Creating ' + str(connectorsN) +  ' connectors')
connectors = []          
for i in range(connectorsN):
    connectors.append(Connector(i))
    
connectors = insertBulk(cursor, connectors)
#################
####ARTIFACTS####
#################
cableTypeArtifactsN = int(cableTypesN * percentOfCableTypesWithArtifacts / 100)
print('    Creating ' + str(cableTypeArtifactsN) +  ' cable type artifacts')
cableTypeArtifacts = []
for i in range(cableTypeArtifactsN):
    cableTypeArtifacts.append(Artifact(i, owner_name))
    
cableTypeArtifacts = insertBulk(cursor, cableTypeArtifacts)
    
#####################
####MANUFACTURERS####
#####################
print('    Creating ' + str(manufacturersN) + ' manufacturers')
manufacturers = []
for i in range(manufacturersN):
    manufacturers.append(Manufacturer(i)) 
     
manufacturers = insertBulk(cursor, manufacturers)
##################
####CABLETYPES####
##################
cableTypes = []
print('    Creating ' + str(cableTypesN) +  ' cableTypes')
for i in range(cableTypesN):
    manufacturer_id = getFromList(manufacturers, i).id
    datasheet_id = getFromList(cableTypeArtifacts, i).id
    
    cableTypes.append(CableType(i, manufacturer_id, datasheet_id))
    
cableTypes = insertBulk(cursor, cableTypes)
    
del manufacturers # not needed anymore, so we give garbage collector possibility to free space
del cableTypeArtifacts
################
####ROUTINGS####
################
routings = []
print('    Creating ' + str(routingsN) + ' routings')
for i in range(routingsN):
    routings.append(Routing(i, owner_name))

routings = insertBulk(cursor, routings)


print('    Commiting to database')
connection.commit()
cursor.close()
connection.close()


cablesPart = cablesN // populateInParts
print('Populating main entities')
for j in range(populateInParts + 1):
    connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
    cursor = connection.cursor()
    print(str(j + 1) + '. part of population')
    
    if j == populateInParts:
        cablesPart = cablesN % populateInParts
        
    endpointsN = cablesPart * endpointsPerCable
    routingRowsN = cablesPart * routingRowsPerCable
    cableArtifactsN = int(cablesPart * percentOfCablesWithArtifacts / 100)
    if endpointsN == 0 or cablesPart == 0 or routingRowsN == 0 or cableArtifactsN == 0 :
        break;
    #################
    ####ARTIFACTS####
    #################
    cableArtifacts = []
    print('    Creating ' + str(cableTypeArtifactsN) +  ' cable type artifacts')
    for i in range(cableTypeArtifactsN):
        cableArtifacts.append(Artifact(i, owner_name))
        
    cableArtifacts = insertBulk(cursor, cableArtifacts)
    #################
    ####ENDPOINTS####
    #################
    endpoints = []
    print('    Creating ' + str(endpointsN) +  ' endpoints')
    for i in range(j * endpointsN, (j+1) * endpointsN):
        connector_id = getFromList(connectors, i).id
        
        endpoints.append(Endpoint(i, connector_id))
    
    endpoints = insertBulk(cursor, endpoints)
    ##############
    ####CABLES####
    ##############
    cables = []
    for i in range(j * cablesPart, (j+1) * cablesPart):
        k = i - j*cablesPart
        cabletype_id = getFromList(cableTypes, i).id
        endpointa_id = getFromList(endpoints, k + 1).id
        endpointb_id = getFromList(endpoints, k).id
        qualityreport_id = getFromList(cableArtifacts, i).id
        
        cables.append(Cable(i , cabletype_id, endpointa_id, endpointb_id, qualityreport_id))
    
    cables = insertBulk(cursor, cables)
    print('    Created ' + str(len(cables)) +  ' cables')
    
    ##############
    ####OWNERS####
    ##############
    owners = []
    for i in range(j * cablesPart, (j+1) * cablesPart):
        k = i - j*routingRowsN
        cable = getFromList(cables, k).id
        owner = owner_name
        owners.append(CableOwners(cable, owner))
    
    insertBulk(cursor, owners,False)
    
    ###################
    ####ROUTINGROWS####
    ###################
    routingRows = []
    for i in range(j * routingRowsN, (j + 1) * routingRowsN):
        k = i - j*routingRowsN
        position = k//cablesPart
        cable = getFromList(cables, k).id
        routing = getFromList(routings, i).id
        routingRows.append(RoutingRow(position, cable, routing))
    
    routingRows = insertBulk(cursor, routingRows)
    print('    Created ' + str(len(routingRows)) +  ' routingRows')
    

    ###################
    #connection.rollback()
    
    print('    Commiting to database')
    connection.commit()
    cursor.close()
    connection.close()



print('Populating redundant columns')
connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()
########################
##CABLE Routings column#
########################
cursor.execute("UPDATE cable SET routingsstring = names.routing_names FROM (SELECT STRING_AGG(routing.name, ', ' order by routingrow.position) AS routing_names,routingrow.cable AS cable_id FROM routing JOIN routingrow ON routingrow.routing = routing.id GROUP BY (cable_id)) names WHERE cable.id = names.cable_id")


cursor.execute("UPDATE cable SET autocalculatedlength = 'NO' WHERE autocalculatedlength = 'false'")
cursor.execute("UPDATE cable SET autocalculatedlength = 'YES' WHERE autocalculatedlength = 'true'")



connection.commit()
cursor.close()
connection.close()  
print('    Commiting to database')
  

print('Done')
print('Created a total of:')
print('    ' + str(connectorsN) +  ' connectors')
print('    ' + str(cableTypeArtifactsN + cablesN * percentOfCablesWithArtifacts/100) +  ' artifacts')
print('    ' + str(manufacturersN) + ' manufacturers')
print('    ' + str(cableTypesN) +  ' cableTypes')
print('    ' + str(cablesN * endpointsPerCable) +  ' endpoints')
print('    ' + str(cablesN) +  ' cables')
print('    ' + str(routingsN) + ' routings')
print('    ' + str(cablesN * routingRowsPerCable) +  ' routingRows')