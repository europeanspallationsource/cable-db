from datetime import datetime
import random

def createString(word, i):
    ''' returns a substring of word appended with a number according to index i'''
    return word[:(i % len(word)) + 1] + str(i//len(word))   

def getFromList(aList, idx):
    ''' return element with index idx from aList if idx is greater that length of a list it returns idx - k*length'''
    if len(aList) != 0:
        return aList[idx % len(aList)] 
    
def insertBulk(cursor, objects, getId=True):
    ''' Inserts objects data into databse through connection provided by cursor and returns ids'''
    table = objects[0].table
    columns = objects[0].columns
    values = [object.getValue() for object in objects]
    if getId:
        cursor.execute("INSERT INTO " + table  + " (" + ", ".join(columns) + ") " + "VALUES " +  ",".join( "('" + "', '".join(value) + "')" for value in values) + " RETURNING id")
        ids =  [id[0] for id in cursor.fetchall()]
        for (id, object) in zip(ids, objects):
            object.id = id
        return objects
    else:
        cursor.execute("INSERT INTO " + table  + " (" + ", ".join(columns) + ") " + "VALUES " +  ",".join( "('" + "', '".join(value) + "')" for value in values))

class Base():
    def __init__(self):
        self.id = -1
      
    def insertIntoDatabase(self, cursor):
        ''' Inserts object data into databse through connection provided by cursor and returns id'''
        cursor.execute("INSERT INTO " + self.__class__.table  + " (" + ", ".join(self.__class__.columns) + ") " + "VALUES " + "('" + "', '".join(self.getValue()) + "')"  + " RETURNING id")
        self.id = [id[0] for id in cursor.fetchall()][0]
        return self.id
    

class Manufacturer(Base):
    columns = ["address", "country", "created", "email", "modified", "name", "phonenumber", "status"]
    table = "manufacturer"
    
    def __init__(self, i):
        '''params:
            i - int used for dta generation
        '''
        self.address = createString(Manufacturer.columns[0], i)
        self.country = createString(Manufacturer.columns[1], i)
        self.created = str(datetime.now())
        self.email = createString(Manufacturer.columns[3], i) + "@" + createString(Manufacturer.columns[3][::-1], i) + createString(".manufactuer", i)
        self.modified = str(datetime.now()) 
        self.name = createString(Manufacturer.columns[5], i)
        self.phonenumber = str(random.randint(000000,999999))
        self.status = "APPROVED" if i % 2 == 0 else "DELETED"

    def getValue(self):
        return  [self.address, self.country, self.created, self.email, self.modified, self.name, self.phonenumber, self.status]
    
    
class Routing(Base):
    columns = ["name", "description", "created", "modified", "owner", "active", "length"]#, "routingclass"]
    table = "routing"
    routingClass = "Z"
    
    def __init__(self, i, owner):
        '''params:
            i - int used for data generation
            owner - str owner of the routing
        '''
        self.name = createString(Routing.columns[0], i)
        self.description = createString(Routing.columns[1], i)
        self.created = str(datetime.now())
        self.modified = str(datetime.now()) 
        self.active = "true" if i % 2 == 0 else "false"
        self.length = str(random.random()*500)
        #self.routingClass = Routing.routingClass
        self.owner = owner
    def getValue(self):
        return [self.name, self.description, self.created, self.modified, self.owner, self.active, self.length]#, self.routingClass]
    
    
class Connector(Base):
    columns = ["description", "created", "connectorType", "modified", "name", "active"]
    table = "connector"
    
    def __init__(self, i):
        '''params:
            i - int used for dta generation
        '''
        self.description = createString(Connector.columns[0], i)
        self.created = str(datetime.now())
        self.connectorType = createString(Connector.columns[2], i)
        self.modified = str(datetime.now()) 
        self.name = createString(Connector.columns[4], i)
        self.active = "t" if i % 2 == 0 else "f"
        
    def getValue(self):
        return [self.description, self.created,self.connectorType, self.modified, self.name, self.active]
    
class Artifact(Base):
    columns = ["description", "name", "uri", "rooturi", "modifiedat" , "modifiedby"]
    table = "artifact"
    URI = "~/wildfly-8.2.1.Final/blobStore/"
    artifacturis = ["2017/2/13/78750bec-be7b-4b10-8c59-73f1cb854a49"]
    
    def __init__(self, i, owner):
        '''params:
            i - int used for dta generation
            owner - str of the owner of this artifact
        '''
        self.name = createString(Artifact.columns[0], i)
        self.description = createString(Artifact.columns[1], i)
        self.uri = Artifact.artifacturis[i%len(Artifact.artifacturis)]
        self.rooturi = Artifact.URI    
        self.modifiedat = str(datetime.now()) 
        self.modifiedby = owner
    
    def getValue(self):
        return [self.description, self.name, self.uri, self.rooturi, self.modifiedat, self.modifiedby]
    
class CableType(Base):
    columns = ["active", "description", "comments", "flammability", "insulation", "jacket", "name", "installationType", "service", "tid", "weight", "diameter", "voltage"]
    table = "cabletype"
    installationTypes = ["UNKNOWN", "INDOOR", "OUTDOOR"]
    
    def __init__(self, i ,manufacturerId, artifactId):
        '''params:
            i - int used for data generation
            manufacturedId - int database id of the cablType manufactuer 
            artifactId - int database id of the cableType datasheet
        '''
        self.active = "t" if i % 2 == 0 else "f"
        self.description = createString(CableType.columns[1], i)
        self.comments = createString(CableType.columns[2], i)
        self.flammability = createString(CableType.columns[3], i)
        self.insulation = createString(CableType.columns[4], i)
        self.jacket = createString(CableType.columns[5], i)
        self.name = createString(CableType.columns[6], i)
        self.installationType = CableType.installationTypes[i%len(CableType.installationTypes)]
        self.service = createString(CableType.columns[7], i)
        self.tid = str(random.random()*10000)
        self.weight = str(random.random()*5000)
        self.diameter = str(random.random()*1000)
        self.voltage = str(random.random()*1000)
        
    def getValue(self):
        return [self.active, self.description, self.comments, self.flammability, self.insulation, self.jacket, self.name, self.installationType, self.service, self.tid, self.weight, self.diameter, self.voltage]
    
class Endpoint(Base):
    columns = ["building", "device", "drawing", "label", "rack", "validity", "connector_id"]
    table = "endpoint"
    device = "sc0-sb0:ds0-dv0-" # begining of device name (parents of device in naming service)
    devices = 100 # Assumption that naming sevice contains at least 100 of devices with such names 
    
    def __init__(self, i, connectorId):
        '''params:
            i - int used for dta generation
            connectorId - int database id of the connector
        '''
        self.building = createString(Endpoint.columns[0], i)
        self.device = Endpoint.device + str(i % Endpoint.devices).zfill(6)
        self.drawing = "http://www." + createString(Endpoint.columns[2], i) + ".com"
        self.label = self.device + createString(Endpoint.columns[3], i)
        self.rack = self.device
        self.validity = "VALID" if i % 2 == 0 else "DANGLING"
        self.connector_id = str(connectorId)
    
    def getValue(self):
        return  [self.building, self.device, self.drawing, self.label, self.rack, self.validity, self.connector_id]
    
class Cable(Base):
    columns = ["cableclass", "created", "installationby", "modified", "seqnumber", "status", "subsystem", "system", "terminationby", "validity", "cabletype_id", "endpointa_id", "endpointb_id", "autocalculatedlength", "length", "container", "qualityreport_id"]
    table = "cable"
    subsystem = "9"
    system = "9"
    cableClass = "Z"

    def __init__(self, i, cabletypeId, endpointAId, endpointBId, artifactId):
        '''params:
            i - int used for data generation
            cabletypeId - int database id of cableType
            endpointAId - int database id of endpoint A
            endpointBId - int database id of endpoint B
            artifactId - int database id of quality report
        '''
        self.cableclass = Cable.cableClass
        self.created = str(datetime.now()) 
        self.installationby = str(datetime.now()) 
        self.modified = str(datetime.now())
        self.seqnumber = str(500000 + i).zfill(6)
        self.status = "APPROVED" if i % 2 == 0 else "DELETED"
        self.subsystem = Cable.subsystem
        self.system = Cable.system
        self.terminationby = str(datetime.now())
        self.validity = "VALID" if i % 2 == 0 else "DANGLING"
        self.cabletype_id = str(cabletypeId)
        self.endpointa_id = str(endpointAId)
        self.endpointb_id = str(endpointBId)
        self.qualityreport_id = str(artifactId)
        self.autoCalculated = "false"
        self.length = str(random.random()*500)
        j = random.randint(1,500)
        self.container = "" if i == j else Cable.system + Cable.subsystem + Cable.cableClass + str(500000 + j).zfill(6)
        
    def getValue(self):
        return [self.cableclass, self.created, self.installationby, self.modified, self.seqnumber, self.status, self.subsystem, self.system, self.terminationby, self.validity, self.cabletype_id, self.endpointa_id, self.endpointb_id, self.autoCalculated, self.length, self.container, self.qualityreport_id]

class CableOwners(Base):
    columns = ["cable_id" , "owners"]
    table= "cable_owners"
    
    def __init__(self, cableId, owners):
        '''params:
            cableId - int database id of the cable
            owners - name of the owner
        '''
        self.cable = str(cableId)
        self.owner = str(owners)
    
    def getValue(self):
        return [self.cable, self.owner]

    
class RoutingRow(Base):
    columns = ["position" , "cable" , "routing"]
    table= "routingrow"
    
    def __init__(self, position, cableId, routingId):
        '''params:
            position - int position of the routing
            cableId - int database id of the cable
            routingId .- int database id of the routing
        '''
        self.position = str(position)
        self.cable = str(cableId)
        self.routing = str(routingId)
    
    def getValue(self):
        return [self.position, self.cable, self.routing]
