/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.openepics.cable.services.Names;

/**
 * This is a request manager for reporting the state of the application, including validity of connections to other
 * services the application uses.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class ApplicationStateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7915157653569748651L;

    @Inject
    private Names names;

    private boolean namesServiceWarning;

    /** Set initial dialog state display. */
    @PostConstruct
    public void init() {
        namesServiceWarning = names.isError();
    }

    /** @return true if the names service warning is displayed */
    public boolean isNamesServiceWarning() {
        return namesServiceWarning;
    }

    /**
     * @param namesServiceWarning
     *            the display state of names service warning
     */
    public void setNamesServiceWarning(boolean namesServiceWarning) {
        this.namesServiceWarning = namesServiceWarning;
    }
}
