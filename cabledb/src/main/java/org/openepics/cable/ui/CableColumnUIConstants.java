/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

public interface CableColumnUIConstants {

    public static final String DEFINE_WHICH_SYSTEM_THE_CABLE_IS_PART_OF_AND_HOW_IT_IS_USED =
            " define which system the cable is part of and how it is used.";

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The cable name.";
    public static final String NAME_STYLECLASS = "fixed_width108";
    public static final String NAME_STYLE = "text-align:center";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String MODIFIED_VALUE = "modified";
    public static final String MODIFIED_TOOLTIP = "Date when cable was modified.";
    public static final String MODIFIED_STYLECLASS = "fixed_width160";
    public static final String MODIFIED_STYLE = null;
    public static final String MODIFIED_FILTERMODE = "contains";
    public static final String MODIFIED_FILTERSTYLE = "width: 100%;";

    public static final String CABLETYPE_VALUE = "cableType";
    public static final String CABLETYPE_URL = "cableTypeUrl";
    public static final String CABLETYPE_TOOLTIP = "The type of the cable.";
    public static final String CABLETYPE_STYLECLASS = "fixed_width128";
    public static final String CABLETYPE_STYLE = null;
    public static final String CABLETYPE_FILTERMODE = "contains";
    public static final String CABLETYPE_FILTERSTYLE = "width: 100%;";

    public static final String CONTAINER_VALUE = "container";
    public static final String CONTAINER_URL = "containerUrl";
    public static final String CONTAINER_TOOLTIP = "The container cable number.";
    public static final String CONTAINER_STYLECLASS = "fixed_width108";
    public static final String CONTAINER_STYLE = null;
    public static final String CONTAINER_FILTERMODE = "contains";
    public static final String CONTAINER_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_NAME_VALUE = "endpointDeviceNameA";
    public static final String DEVICE_A_URL = "endpointDeviceAUrl";
    public static final String DEVICE_A_NAME_TOOLTIP = "The name of the first endpoint device.";
    public static final String DEVICE_A_NAME_STYLECLASS = "fixed_width208";
    public static final String DEVICE_A_NAME_STYLE = null;
    public static final String DEVICE_A_NAME_FILTERMODE = "contains";
    public static final String DEVICE_A_NAME_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_BUILDING_VALUE = "endpointBuildingA";
    public static final String DEVICE_A_BUILDING_TOOLTIP = "The building where the first endpoint device resides.";
    public static final String DEVICE_A_BUILDING_STYLECLASS = "fixed_width140";
    public static final String DEVICE_A_BUILDING_STYLE = null;
    public static final String DEVICE_A_BUILDING_FILTERMODE = "contains";
    public static final String DEVICE_A_BUILDING_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_RACK_VALUE = "endpointRackA";
    public static final String DEVICE_A_RACK_TOOLTIP = "The rack where the first endpoint device resides.";
    public static final String DEVICE_A_RACK_STYLECLASS = "fixed_width150";
    public static final String DEVICE_A_RACK_STYLE = null;
    public static final String DEVICE_A_RACK_FILTERMODE = "contains";
    public static final String DEVICE_A_RACK_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_CONNECTOR_VALUE = "endpointConnectorA";
    public static final String DEVICE_A_CONNECTOR_URL = "endpointConnectorAUrl";
    public static final String DEVICE_A_CONNECTOR_TOOLTIP = "The first device connector";
    public static final String DEVICE_A_CONNECTOR_STYLECLASS = "fixed_width140";
    public static final String DEVICE_A_CONNECTOR_STYLE = null;
    public static final String DEVICE_A_CONNECTOR_FILTERMODE = "contains";
    public static final String DEVICE_A_CONNECTOR_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_WIRING_VALUE = "endpointWiringA";
    public static final String DEVICE_A_WIRING_TOOLTIP = "The wiring of connector A.";
    public static final String DEVICE_A_WIRING_STYLECLASS = "fixed_width286";
    public static final String DEVICE_A_WIRING_STYLE = null;
    public static final String DEVICE_A_WIRING_FILTERMODE = "contains";
    public static final String DEVICE_A_WIRING_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_A_LABEL_VALUE = "endpointLabelA";
    public static final String DEVICE_A_LABEL_TOOLTIP = "The first device user label text.";
    public static final String DEVICE_A_LABEL_STYLECLASS = "fixed_width450";
    public static final String DEVICE_A_LABEL_STYLE = null;
    public static final String DEVICE_A_LABEL_FILTERMODE = "contains";
    public static final String DEVICE_A_LABEL_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_NAME_VALUE = "endpointDeviceNameB";
    public static final String DEVICE_B_URL = "endpointDeviceBUrl";
    public static final String DEVICE_B_NAME_TOOLTIP = "The name of the second endpoint device.";
    public static final String DEVICE_B_NAME_STYLECLASS = "fixed_width208";
    public static final String DEVICE_B_NAME_STYLE = null;
    public static final String DEVICE_B_NAME_FILTERMODE = "contains";
    public static final String DEVICE_B_NAME_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_BUILDING_VALUE = "endpointBuildingB";
    public static final String DEVICE_B_BUILDING_TOOLTIP = "The building where the second endpoint device resides.";
    public static final String DEVICE_B_BUILDING_STYLECLASS = "fixed_width140";
    public static final String DEVICE_B_BUILDING_STYLE = null;
    public static final String DEVICE_B_BUILDING_FILTERMODE = "contains";
    public static final String DEVICE_B_BUILDING_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_RACK_VALUE = "endpointRackB";
    public static final String DEVICE_B_RACK_TOOLTIP = "The rack where the second endpoint device resides.";
    public static final String DEVICE_B_RACK_STYLECLASS = "fixed_width150";
    public static final String DEVICE_B_RACK_STYLE = null;
    public static final String DEVICE_B_RACK_FILTERMODE = "contains";
    public static final String DEVICE_B_RACK_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_CONNECTOR_VALUE = "endpointConnectorB";
    public static final String DEVICE_B_CONNECTOR_URL = "endpointConnectorBUrl";
    public static final String DEVICE_B_CONNECTOR_TOOLTIP = "The second device connector";
    public static final String DEVICE_B_CONNECTOR_STYLECLASS = "fixed_width140";
    public static final String DEVICE_B_CONNECTOR_STYLE = null;
    public static final String DEVICE_B_CONNECTOR_FILTERMODE = "contains";
    public static final String DEVICE_B_CONNECTOR_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_WIRING_VALUE = "endpointWiringB";
    public static final String DEVICE_B_WIRING_TOOLTIP = "The wiring of connector B.";
    public static final String DEVICE_B_WIRING_STYLECLASS = "fixed_width286";
    public static final String DEVICE_B_WIRING_STYLE = null;
    public static final String DEVICE_B_WIRING_FILTERMODE = "contains";
    public static final String DEVICE_B_WIRING_FILTERSTYLE = "width: 100%;";

    public static final String DEVICE_B_LABEL_VALUE = "endpointLabelB";
    public static final String DEVICE_B_LABEL_TOOLTIP = "The second device user label text.";
    public static final String DEVICE_B_LABEL_STYLECLASS = "fixed_width450";
    public static final String DEVICE_B_LABEL_STYLE = null;
    public static final String DEVICE_B_LABEL_FILTERMODE = "contains";
    public static final String DEVICE_B_LABEL_FILTERSTYLE = "width: 100%;";

    public static final String SYSTEM_VALUE = "system";
    public static final String SYSTEM_TOOLTIP = "Cable system. Together with the Su and Cl"
            + DEFINE_WHICH_SYSTEM_THE_CABLE_IS_PART_OF_AND_HOW_IT_IS_USED;
    public static final String SYSTEM_STYLECLASS = "fixed_width48";
    public static final String SYSTEM_STYLE = "text-align:center";
    public static final String SYSTEM_FILTERMODE = "exact";
    public static final String SYSTEM_FILTERSTYLE = "width: 100%;";

    public static final String SUBSYSTEM_VALUE = "subsystem";
    public static final String SUBSYSTEM_TOOLTIP = "Cable class. Together with the Sy and Cu"
            + DEFINE_WHICH_SYSTEM_THE_CABLE_IS_PART_OF_AND_HOW_IT_IS_USED;
    public static final String SUBSYSTEM_STYLECLASS = "fixed_width48";
    public static final String SUBSYSTEM_STYLE = "text-align:center";
    public static final String SUBSYSTEM_FILTERMODE = "exact";
    public static final String SUBSYSTEM_FILTERSTYLE = "width: 100%;";

    public static final String CLASS_VALUE = "cableClass";
    public static final String CLASS_TOOLTIP = "Cable system. Together with the Su and Cl"
            + DEFINE_WHICH_SYSTEM_THE_CABLE_IS_PART_OF_AND_HOW_IT_IS_USED;
    public static final String CLASS_STYLECLASS = "fixed_width48";
    public static final String CLASS_STYLE = "text-align:center";
    public static final String CLASS_FILTERMODE = "exact";
    public static final String CLASS_FILTERSTYLE = "width: 100%;";

    public static final String ROUTINGS_VALUE = "routingsString";
    public static final String ROUTINGS_TOOLTIP = "The routing value of cable.";
    public static final String ROUTINGS_STYLECLASS = "fixed_width80";
    public static final String ROUTINGS_STYLE = null;
    public static final String ROUTINGS_FILTERMODE = "contains";
    public static final String ROUTINGS_FILTERSTYLE = "width: 100%;";

    public static final String OWNERS_VALUE = "ownersString";
    public static final String OWNERS_TOOLTIP = "The users that are responsible for this cable,"
            + " usually the same person that is performing the upload.";
    public static final String OWNERS_STYLECLASS = "fixed_width140";
    public static final String OWNERS_STYLE = null;
    public static final String OWNERS_FILTERMODE = "exact";
    public static final String OWNERS_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "status";
    public static final String STATUS_TOOLTIP = "Cable status.";
    public static final String STATUS_STYLECLASS = "fixed_width128";
    public static final String STATUS_STYLE = null;
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = null;

    public static final String INSTALLATION_BY_VALUE = "installationBy";
    public static final String INSTALLATION_BY_TOOLTIP =
            "The date by which the installation of this cable is performed.";
    public static final String INSTALLATION_BY_STYLECLASS = "fixed_width160";
    public static final String INSTALLATION_BY_STYLE = null;
    public static final String INSTALLATION_BY_FILTERMODE = "contains";
    public static final String INSTALLATION_BY_FILTERSTYLE = "width: 100%;";

    public static final String TERMINATION_BY_VALUE = "terminationBy";
    public static final String TERMINATION_BY_TOOLTIP = "The date by which the cable is terminated.";
    public static final String TERMINATION_BY_STYLECLASS = "fixed_width160";
    public static final String TERMINATION_BY_STYLE = null;
    public static final String TERMINATION_BY_FILTERMODE = "contains";
    public static final String TERMINATION_BY_FILTERSTYLE = "width: 100%;";

    public static final String QUALITY_REPORT_VALUE = "qualityReport";
    public static final String QUALITY_REPORT_TOOLTIP = "The quality report artifact.";
    public static final String QUALITY_REPORT_STYLECLASS = "fixed_width286";
    public static final String QUALITY_REPORT_STYLE = null;
    public static final String QUALITY_REPORT_FILTERMODE = "contains";
    public static final String QUALITY_REPORT_FILTERSTYLE = "width: 100%;";

    public static final String BASELENGTH_VALUE = "baseLength";
    public static final String BASELENGTH_TOOLTIP = "The base length of cable used when autocalculating length.";
    public static final String BASELENGTH_STYLECLASS = "fixed_width128";
    public static final String BASELENGTH_STYLE = null;
    public static final String BASELENGTH_FILTERMODE = "contains";
    public static final String BASELENGTH_FILTERSTYLE = "width: 100%;";

    public static final String LENGTH_VALUE = "length";
    public static final String LENGTH_TOOLTIP = "The actual length of the cable in meters.";
    public static final String LENGTH_STYLECLASS = "fixed_width128";
    public static final String LENGTH_STYLE = null;
    public static final String LENGTH_FILTERMODE = "contains";
    public static final String LENGTH_FILTERSTYLE = "width: 100%;";

    public static final String AUTOCALCULATEDLENGTH_VALUE = "autoCalculatedLength";
    public static final String AUTOCALCULATEDLENGTH_TOOLTIP =
            "Indicates if length was auto calculated from routing points.";
    public static final String AUTOCALCULATEDLENGTH_STYLECLASS = "fixed_width128";
    public static final String AUTOCALCULATEDLENGTH_STYLE = null;
    public static final String AUTOCALCULATEDLENGTH_FILTERMODE = "contains";
    public static final String AUTOCALCULATEDLENGTH_FILTERSTYLE = "width: 100%;";

    public static final String COMMENTS_VALUE = "comments";
    public static final String COMMENTS_URL = "commentsUrl";
    public static final String COMMENTS_TOOLTIP = "Comment for the cable.";
    public static final String COMMENTS_STYLECLASS = "fixed_width286";
    public static final String COMMENTS_STYLE = null;
    public static final String COMMENTS_FILTERMODE = "contains";
    public static final String COMMENTS_FILTERSTYLE = "width: 100%;";

    public static final String REVISION_VALUE = "revision";
    public static final String REVISION_TOOLTIP = "The revision of the cable.";
    public static final String REVISION_STYLECLASS = "fixed_width92";
    public static final String REVISION_STYLE = null;
    public static final String REVISION_FILTERMODE = "contains";
    public static final String REVISION_FILTERSTYLE = "width: 100%;";

    public static final String EMPTY_FILTER_DROPDOWN_VALUE = "<empty>";
    public static final String NON_DELETED_DROPDOWN_VALUE = "NON-DELETED";
}
