/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.DisplayViewColumn;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.LocationTag;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.Routing;
import org.openepics.cable.model.RoutingRow;
import org.openepics.cable.services.ArtifactService;
import org.openepics.cable.services.AuthenticationServiceStartup;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.CableValidationService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.DateUtil;
import org.openepics.cable.services.DisplayViewService;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.RoutingService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryService;
import org.openepics.cable.services.UserDirectoryServiceCache;
import org.openepics.cable.services.dl.CableImportExportService;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.ui.lazymodels.CableLazyModel;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.EncodingUtility;
import org.openepics.cable.util.URLUtility;
import org.openepics.cable.util.Utility;

import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import org.openepics.cable.model.GenericArtifact;

/**
 * This is the backing requests bean for cables.xhtml.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@ManagedBean
@ViewScoped
public class CableRequestManager implements Serializable { // NOSONAR squid:S1200 Coupled to over 20 classes due to-

    // performing service validations
    private static final long serialVersionUID = 8161254904185694693L;

    private static final String CABLE_DB_GROWL = "cableDBGrowl";

    private static final String EMPTY_STRING = "";
    private static final String NAMING_DEVICE_PAGE = "devices.xhtml?i=2&deviceName=";
    private static final String CCDB_DEVICE_PAGE = "?name=";
    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final int MAX_AUTOCOMPLETE = 20;
    private static final Logger LOGGER = Logger.getLogger(CableRequestManager.class.getName());
    private static final List<CableUI> EMPTY_LIST = new ArrayList<>();

    private final String chessURL = getChessURLCableProperties();

    private List<CableColumnUI> columns;
    private List<String> columnTemplate = CableColumnUI.getAllColumns();
    @Inject
    private transient ArtifactRequestManager artifactRequestManager;
    @Inject
    private transient ArtifactService artifactService;
    @Inject
    private transient CableService cableService;
    @Inject
    private transient CableTypeService cableTypeService;
    @Inject
    private transient ConnectorService connectorService;
    @Inject
    private transient RoutingService routingService;
    @Inject
    private transient CableImportExportService cableImportExportService;
    @Inject
    private Names names;

    @Inject
    private SessionService sessionService;

    @Inject
    private transient AuthenticationServiceStartup authenticationServiceStartup;
    @Inject
    private transient UserDirectoryService userDirectoryService;
    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;
    @Inject
    private transient CableValidationService cableValidationService;
    @Inject
    private QueryService queryService;
    @Inject
    private DisplayViewService displayViewService;

    private CableLazyModel lazyModel;

    private List<CableUI> cables;
    private List<CableUI> deletedCables;
    private List<CableUI> selectedCables = EMPTY_LIST;
    private List<CableType> cableTypes;
    private List<Connector> connectors;
    private List<Routing> routings;

    private byte[] fileToBeImported;
    private LoaderResult<Cable> importResult;
    private String importFileName;
    private String selectedDevice;

    private CableUI selectedCable;
    private GenericArtifact uploadedArtifact;
    private CableColumnUI selectedArtifactColumn;
    private Cable oldCable;
    private QueryUI selectedQuery;
    private DisplayViewUI selectedDisplayView;
    private String requestedCableName;
    private boolean isAddPopupOpened;
    private boolean isCableRequested;
    private RoutingRow selectedRoutingRow;
    private int cablesToExportSize = 0;
    private boolean routingRowTableMaximized;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private String longOverlayURL;

    private List<Routing> avaliableRoutings;

    private HashMap<String, Cable> oldCablesMap;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    public CableRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = CableColumnUI.values().length + 1;
        columnVisibility = new ArrayList<Boolean>();
        for (int i=0; i<numberOfColumns; i++) {
            columnVisibility.add(Boolean.TRUE);
        }
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {
        // perform service availability check and validation when ui first displayed
        authenticationServiceStartup.validate();
        userDirectoryService.validate();
        cableValidationService.validateAtStartup();

        isAddPopupOpened = false;
        cables = new ArrayList<>();
        Map<String, Object> defaultFilter = new HashMap<String, Object>();
        defaultFilter.put(CableColumnUI.STATUS.getValue(), CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE);
        lazyModel = new CableLazyModel(cableService, defaultFilter, this);
        selectedCables.clear();
        selectedCable = null;

        clearImportState();
        requestedCableName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                .getParameter("cableName");
        createDynamicColumns();
        refreshCables();

        // prepare datatable
        //     cookies
        //         rows per page
        //         column visibility
        //     row number (in all rows) for requested entry (if any)
        Cookie[] cookies =
                ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                switch (cookie.getName()) {
                    case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                        initPaginationPageSize(cookie.getValue());
                        break;
                    case CookieUtility.CD_CABLE_COLUMN_VISIBILITY:
                        initColumnVisibility(cookie.getValue());
                        break;
                    default:
                        break;
                }
            }
        }
        rowNumber = rowNumber(requestedCableName);
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        if (!StringUtils.isEmpty(visibility)) {
            visibility = EncodingUtility.decode(visibility);
            String[] split = visibility.split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfColumns) {
                for (int i=0; i<numberOfColumns; i++) {
                    columnVisibility.set(i, Boolean.valueOf(split[i]));
                }
            }
        }
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param cableName
     * @return
     */
    private int rowNumber(String cableName) {
        // note
        //     currently not consider filter

        CableUI cableToSelect = null;
        this.isCableRequested = false;
        if (requestedCableName != null) {
            isCableRequested = true;
            cableToSelect = getCableFromCableName(requestedCableName);
        }

        selectedCables.clear();
        if (cableToSelect != null) {
            selectedCable = cableToSelect;
            selectedCables.add(selectedCable);

            int elementPosition = 0;
            for (CableUI cable : cables) {
                if (cable.getName().equals(requestedCableName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private CableUI getCableFromCableName(final String cableName) {
        if (cableName == null || cableName.isEmpty()) {
            return null;
        }

        CableUI cableToSelect = null;
        for (CableUI cable : cables) {
            if (cable.getName().equals(cableName)) {
                cableToSelect = cable;
            }
        }

        return cableToSelect;
    }

    private void refreshCables() {
        lazyModel.setQuery(selectedQuery);
        selectedCables = EMPTY_LIST;
        selectedCable = null;
    }

    public String getRequestedCableName() {
        return requestedCableName;
    }

    public void setRequestedCableName(String requestedCableName) {
        this.requestedCableName = requestedCableName;
    }

    /** @return true if the current user can import cables, else false */
    public boolean canImportCables() {
        return cableImportExportService.canImportCables();
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /** @return the cables to be displayed */
    public List<CableUI> getCables() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return cables;
    }

    /** @return the currently selected cables, cannot return null */
    public List<CableUI> getSelectedCables() {
        return selectedCables;
    }

    /**
     * @param selectedCables
     *            the cables to select
     */
    public void setSelectedCables(List<CableUI> selectedCables) {
        this.selectedCables = selectedCables != null ? selectedCables : EMPTY_LIST;
        LOGGER.fine("Setting selected cables: " + this.selectedCables.size());
    }

    /** Clears the current cable selection. */
    public void clearSelectedCables() {
        LOGGER.fine("Invoked clear cable selection.");
        setSelectedCables(null);
    }

    /**
     * Returns the cables to be exported, which are the currently filtered and selected cables, or all filtered cables
     * without deleted ones if none selected.
     *
     * @return the cables to be exported
     */
    public List<Cable> getCablesToExport() {

        if (!sessionService.isLoggedIn())
            return new ArrayList<Cable>();

        List<Cable> cablesToExport;

        HashMap<String, Object> exportFilters = new HashMap<String, Object>();

        // Setting up filter to get all cables but deleted ones.
        exportFilters.putAll(lazyModel.getFilters());
        final Query customQuery = cableImportExportService
                .getNotDeletedQuery(lazyModel.getQueryUI() != null ? lazyModel.getQueryUI().getQuery() : null);

        cablesToExport = cableService.findLazy(0, Integer.MAX_VALUE, lazyModel.getSortField(), lazyModel.getSortOrder(),
                exportFilters, customQuery);

        LOGGER.fine("Returning cables to export: " + cablesToExport.size());
        return cablesToExport;
    }

    /** @return the result of a test or true import */
    public LoaderResult<Cable> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void cableFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked cable file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the cable import from the file that was last uploaded.
     */
    public void cableImportTest() {
        LOGGER.fine("Invoked cable import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableImportExportService.importCables(inputStream, true);
                LOGGER.fine("Import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the cable import from the file that was last uploaded.
     */
    public void cableImport() {
        LOGGER.fine("Invoked cable import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableImportExportService.importCables(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshCables();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Invokes explicit cable validation.
     */
    public void validateCables() {
        names.refresh();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Naming cache updated.", null);
        cableValidationService.runOperationBlocking(sessionService.getLoggedInName());
        refreshCables();
        Utility.updateComponent(CABLE_DB_GROWL);
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Validation of cables finished.", null);
    }

    /** @return the cable sheet with cables that were just imported */
    public StreamedContent getCableSheetWithImportedCables() {

        // submit back only non-deleted cables
        final List<Cable> createdOrUpdatedCables = new ArrayList<>();
        for (Cable cable : importResult.getAffected()) {
            createdOrUpdatedCables.add(cable);
        }
        return new DefaultStreamedContent(cableImportExportService.exportCables(createdOrUpdatedCables), "xlsx",
                importFileName);
    }

    /** @return the cable sheet with the cables from cablesToExport list */
    public StreamedContent getCableSheetWithCablesToExport() {

        final List<Cable> cablesToExport = getCablesToExport();

        return new DefaultStreamedContent(cableImportExportService.exportCables(cablesToExport), "xlsx",
                "cdb_cables.xlsx");
    }

    /** @return the cable numbering sheet URL */
    public String getCableNumberingSheetURL() {
        final String url = CableProperties.getInstance().getCableNumberingDocumentURL();
        LOGGER.fine("Cable Numbering Document URL: " + url);
        return url;
    }

    /** @return the date format string to use to display dates */
    public String getDateFormatString() {
        return DateUtil.DATE_FORMAT_STRING;
    }

    /**
     * @return selected device.
     */
    public String getSelectedDevice() {
        return selectedDevice;
    }

    /**
     * Set selected device.
     *
     * @param selectedDevice
     *            selected device
     */
    public void setSelectedDevice(String selectedDevice) {
        this.selectedDevice = selectedDevice;
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        PrimeFaces.current().executeScript("var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"+CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE+"', "+getRows()+", {expires:"+CookieUtility.PERSISTENCE_DAYS+"});}");
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
            PrimeFaces.current().executeScript("var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"+CookieUtility.CD_CABLE_COLUMN_VISIBILITY+"', '"+columnVisibility2String()+"', {expires:"+CookieUtility.PERSISTENCE_DAYS+"});}");
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex
     * @return
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected cables and row selection.
     */
    public void unselectAllRows() {
        clearSelectedCables();

        onRowSelect();
    }

    public void onRowSelect() {
        if (selectedCables != null && !selectedCables.isEmpty()) {
            if (selectedCables.size() == 1) {
                selectedCable = selectedCables.get(0);
            } else {
                selectedCable = null;
            }
        } else {
            selectedCable = null;
        }
    }

    /** @return <code>true</code> if a single cable is selected, <code>false</code> otherwise */
    public boolean isSingleCableSelected() {
        return (selectedCables != null) && (selectedCables.size() == 1)
                && selectedCables.get(0).getStatus() != CableStatus.DELETED;
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    public void prepareAddPopup() {
        routingRowTableMaximized = false;
        if (sessionService.isLoggedIn()) {
            List<String> owners = Arrays.asList(sessionService.getLoggedInName());
            selectedCable = new CableUI(new Cable(getSystems()[0], getSubsystems()[0], getCableClasses()[0], 0, owners,
                    new Date(), new Date()));
            selectedCable.setEndpointA(new Endpoint(getDeviceNames().get(0)));
            selectedCable.setEndpointB(new Endpoint(getDeviceNames().get(0)));
            isAddPopupOpened = true;
        }
    }

    /**
     * Processes cable adding.
     */
    public void onCableAdd() {
        artifactService.updateArtifact(selectedCable.getQualityReport());
        artifactService.updateArtifact(selectedCable.getEndpointWiringA());
        artifactService.updateArtifact(selectedCable.getEndpointWiringB());
        cableService.createCable(selectedCable.getSystem(), selectedCable.getSubsystem(), selectedCable.getCableClass(),
                selectedCable.getOwners(), selectedCable.getStatus(), selectedCable.getCableType(),
                selectedCable.getContainer(), selectedCable.getEndpointA(),
                selectedCable.getEndpointB(), selectedCable.getRoutingRows(), selectedCable.getInstallationByDate(),
                selectedCable.getTerminationByDate(), selectedCable.getQualityReport(),
                selectedCable.getAutoCalculatedLength(), selectedCable.getBaseLength(), selectedCable.getLength(),
                selectedCable.getComments(), selectedCable.getRevision(), sessionService.getLoggedInName(), true);
        refreshCables();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Cable added.", null);
        Utility.updateComponent(CABLE_DB_GROWL);
    }

    public void prepareUploadPopup() {
        uploadedArtifact = new GenericArtifact();
        selectedArtifactColumn = getArtifactColumns().get(0);
        oldCablesMap = new HashMap<>();
        List<Cable> oldCables = cableService
                .getCablesByName(selectedCables.stream().map(m -> m.getName())
                        .collect(Collectors.toList()), true, true);
        selectedCables = cableService
                .getCablesByName(selectedCables.stream().map(m -> m.getName())
                        .collect(Collectors.toList()), false, true)
                .stream().map(CableUI::new).collect(Collectors.toList());
        oldCables.stream().forEach(m -> oldCablesMap.put(m.getName(), m));
    }

    public List<CableColumnUI> getArtifactColumns() {
        List<CableColumnUI> artifactColumns = new ArrayList<>();
        artifactColumns.add(CableColumnUI.DEVICE_A_WIRING);
        artifactColumns.add(CableColumnUI.DEVICE_B_WIRING);
        return artifactColumns;
    }

    public void prepareEditPopup() {
        routingRowTableMaximized = false;
        Preconditions.checkState(isSingleCableSelected());
        Preconditions.checkNotNull(selectedCable);
        // We create a duplicate of the selected cable from database to prevent wrong data display on close, and to
        // preserve changes for history logging.
        oldCable = selectedCable.getCable();
        selectedCable = new CableUI(cableService.getCableById(selectedCable.getCable().getId()));
        isAddPopupOpened = false;
    }

    /**
     * Updates an existing cable with new information from the dialog.
     */
    public void onCableEdit() {
        Preconditions.checkState(isSingleCableSelected());
        Preconditions.checkNotNull(selectedCable);
        final String cableName = oldCable.getName();
        artifactService.updateArtifact(selectedCable.getQualityReport());
        artifactService.updateArtifact(selectedCable.getEndpointWiringA());
        artifactService.updateArtifact(selectedCable.getEndpointWiringB());
        selectedCable.getRoutingRows();
        formatCable(selectedCable);
        final Cable newCable = selectedCable.getCable();
        cableService.updateCable(newCable, oldCable, sessionService.getLoggedInName(), true);
        refreshCables();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Cable '" + cableName + "' updated.", null);
        Utility.updateComponent(CABLE_DB_GROWL);
    }

    /**
     * @return the list of deleted cables.
     */
    public List<CableUI> getDeletedCables() {
        return deletedCables;
    }

    public void resetValues() {
        deletedCables = null;
    }

    /**
     * The method builds a list of cables that are already deleted. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkCablesForDeletion() {
        Preconditions.checkNotNull(selectedCables);
        Preconditions.checkState(!selectedCables.isEmpty());

        deletedCables = Lists.newArrayList();
        for (final CableUI cableToDelete : selectedCables) {
            if (cableToDelete.getStatus() == CableStatus.DELETED) {
                deletedCables.add(cableToDelete);
            }
        }
    }

    /**
     * Event handler which handles the cable delete.
     */
    public void onCableDelete() {
        Preconditions.checkNotNull(deletedCables);
        Preconditions.checkState(deletedCables.isEmpty());
        Preconditions.checkNotNull(selectedCables);
        Preconditions.checkState(!selectedCables.isEmpty());
        int deletedCablesCounter = 0;
        for (final CableUI cableToDelete : selectedCables) {
            final Cable deleteCable = cableToDelete.getCable();
            cableService.deleteCable(deleteCable, sessionService.getLoggedInName());
            deletedCablesCounter++;
        }
        clearSelectedCables();
        deletedCables = null;
        refreshCables();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Deleted " + deletedCablesCounter + " cables.", null);
    }

    /**
     * @return available systems.
     */
    public String[] getSystems() {
        return CableNumbering.getSystemNumbers();
    }

    /**
     * @return available subsystems.
     */
    public String[] getSubsystems() {
        return CableNumbering.getSubsystemNumbers();
    }

    /**
     * @return available cables.
     */
    public String[] getCableClasses() {
        return CableNumbering.getClassLetters();
    }

    /**
     * @param systemNumber
     *            system number
     *
     * @return system name which corresponds to system number.
     */
    public String retrieveSystemLabel(String systemNumber) {
        return CableNumbering.getSystemLabel(systemNumber);
    }

    /**
     * @param subsystemNumber
     *            subsystem number
     *
     * @return subsystem name which corresponds to subsystem number.
     */
    public String retrieveSubsystemLabel(String subsystemNumber) {
        if (selectedCable != null) {
            return CableNumbering.getSubsystemLabel(selectedCable.getSystem(), subsystemNumber);
        }
        return EMPTY_STRING;
    }

    /**
     * @param cableClass
     *            cable class letter
     *
     * @return cable class name which corresponds to cable class letter.
     */
    public String retrieveCableClassLabel(String cableClass) {
        return CableNumbering.getClassLabel(cableClass);
    }

    /**
     * @return true if system is selected otherwise false.
     */
    public boolean getIsSystemSelected() {
        if (selectedCable != null) {
            return selectedCable.getSystem() != null && !selectedCable.getSystem().isEmpty();
        }
        return false;
    }

    /** @return the selected cable. */
    public CableUI getSelectedCable() {
        return selectedCable;
    }

    /** @return all available device names from naming service. */
    public List<String> getDeviceNames() {
        return names.getActiveNames().stream().collect(Collectors.toList());
    }

    public List<String> completeCableType(String query) {
        if (cableTypes == null) {
            cableTypes = cableTypeService.getAllCableTypes();
        }
        List<String> results = new ArrayList<String>();

        for (CableType cableType : cableTypes) {
            String name = cableType.getName();
            if (cableType.isActive() && name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    public List<String> completeContainer(String query) {
        List<String> results = new ArrayList<String>();
        if (!sessionService.isLoggedIn())
            return results;

        List<CableUI> notSelectedCables = new ArrayList<CableUI>(cables);
        notSelectedCables.remove(selectedCable);

        for (CableUI cableUI : notSelectedCables) {
            if (cableUI.getStatus() != CableStatus.DELETED) {
                String name = cableUI.getName();
                if (name.toLowerCase().contains(query.toLowerCase())) {
                    results.add(name);
                }
                if (results.size() >= MAX_AUTOCOMPLETE) {
                    break;
                }
            }
        }
        return results;
    }

    public List<String> completeDeviceNames(String query) {
        List<String> results = new ArrayList<String>();
        for (String name : getDeviceNames()) {
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    public List<String> completeConnectors(String query) {
        if (connectors == null) {
            connectors = connectorService.getConnectors();
        }
        List<String> results = new ArrayList<String>();

        for (Connector connector : connectors) {
            String name = connector.getName();
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * @return cable owners on selected cable
     */
    public List<String> getSelectedCableOwners() {
        return selectedCable != null ? new ArrayList<String>(selectedCable.getOwners())
                : Collections.<String> emptyList();
    }

    /**
     * @param cableOwners
     *            cable owners to set on selected cable
     */
    public void setSelectedCableOwners(List<String> cableOwners) {
        if (selectedCable != null) {
            selectedCable.setOwners(cableOwners);
        }
    }

    public List<String> completeUsers(String query) {
        List<String> users = new ArrayList<>(userDirectoryServiceCache.getAllUsernames());

        List<String> results = new ArrayList<String>();

        for (String user : users) {
            String name = user;
            if (name.toLowerCase().contains(query.toLowerCase()) && !selectedCable.getOwners().contains(name)) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /** @return list of cable statuses that can be set to current cable. */
    public List<CableStatus> getPossibleCableStatus() {
        ArrayList<CableStatus> statuses = new ArrayList<CableStatus>();
        if (selectedCable != null) {
            switch (selectedCable.getStatus()) {
            // If cable's status is INSERT it can be only propagated by administrator
            case INSERTED:
                statuses.add(CableStatus.INSERTED);
                if (sessionService.canAdminister()) {
                    statuses.add(CableStatus.ROUTED);
                    statuses.add(CableStatus.APPROVED);
                }
                break;
            // If cable's status is ROUTED it can be only propagated to status APPROVED by administrator or cable owner
            case ROUTED:
                statuses.add(CableStatus.ROUTED);
                if (selectedCable.getOwners().contains(sessionService.getLoggedInName())
                        || sessionService.canAdminister()) {
                    statuses.add(CableStatus.APPROVED);
                }
                break;
            case APPROVED:
                if (sessionService.canAdminister()) {
                    statuses.add(CableStatus.ROUTED);
                }
                statuses.add(CableStatus.APPROVED);
                break;
            default:
                statuses.add(selectedCable.getStatus());
            }
        }
        return statuses;
    }

    public List<Routing> completeRouting(String query) {
        if (routings == null) {
            routings = routingService.getActiveRoutings();
        }
        List<Routing> results = new ArrayList<Routing>();
        if (!sessionService.isLoggedIn())
            return results;

        for (Routing routing : routings) {
            if (!routing.getCableClasses().contains(selectedCable.getCableClass())) {
                continue;
            }
            String name = routing.getName();
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(routing);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        avaliableRoutings = results;
        return avaliableRoutings;
    }

    public List<String> getRoutingNames() {
        return routingService.getActiveRoutings().stream().map(routing -> routing.getName())
                .collect(Collectors.toList());
    }

    /** @return selected cable cable type. */
    public String getSelectedCableCableType() {
        if (selectedCable == null || selectedCable.getCableType() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getCableType().getName();
    }

    /**
     * Set selected cable cable type.
     *
     * @param name
     *            cable type name
     */
    public void setSelectedCableCableType(String name) {
        if (name != null && !name.isEmpty()) {
            CableType cableType = cableTypeService.getCableType(name);
            selectedCable.setCableType(cableType);
        } else {
            selectedCable.setCableType(null);
        }
    }

    /** @return selected cable connectorA. */
    public String getSelectedCableConnectorA() {
        if (selectedCable == null || selectedCable.getEndpointConnectorA() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getEndpointConnectorA().getName();
    }

    /**
     * Set selected cable connector A.
     *
     * @param name
     *            connector name
     */
    public void setSelectedCableConnectorA(String name) {
        if (name != null && !name.isEmpty()) {
            Connector connector = connectorService.getConnector(name);
            selectedCable.setEndpointConnectorA(connector);
        } else {
            selectedCable.setEndpointConnectorA(null);
        }
    }

    /** @return selected routingRow routing. */
    public String getSelectedRoutingRowRouting() {
        if (selectedRoutingRow == null || selectedRoutingRow.getRouting() == null) {
            return EMPTY_STRING;
        }
        return selectedRoutingRow.getRouting().getName();
    }

    /** @return selected cable connectorB. */
    public String getSelectedCableConnectorB() {
        if (selectedCable == null || selectedCable.getEndpointConnectorB() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getEndpointConnectorB().getName();
    }

    /**
     * Set selected cable connector B.
     *
     * @param name
     *            connector name
     */
    public void setSelectedCableConnectorB(String name) {
        if (name != null && !name.isEmpty()) {
            Connector connector = connectorService.getConnector(name);
            selectedCable.setEndpointConnectorB(connector);
        } else {
            selectedCable.setEndpointConnectorB(null);
        }
    }

    /**
     * Validates if entered value is valid system.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not valid system.
     */
    public void isValidSystem(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isValidSystem(value);
    }

    /**
     * Validates if entered value is valid subsystem.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not valid subsystem.
     */
    public void isValidSubsystem(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isValidSubsystem(value);
    }

    /**
     * Validates if entered value is not valid cable class.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not valid cable class.
     */
    public void isValidCableClass(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isValidCableClass(value);
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isFloatEntered(value);
    }

    /**
     * Validates if entered value is URL.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not URL
     */
    public void isURLEntered(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isURLEntered(value);
    }

    /** @return true if the current user can edit cables, else false */
    public boolean getEditCable() {
        return sessionService.canAdminister() || sessionService.canManageOwnedCables();
    }

    public boolean isEditButtonEnabled() {
        if (selectedCable == null || selectedCable.getStatus() == CableStatus.DELETED) {
            return false;
        }
        if (sessionService.canAdminister()) {
            return true;
        } else if (sessionService.canManageOwnedCables()) {
            return selectedCable.getOwners().contains(sessionService.getLoggedInName());
        }
        return false;
    }

    public boolean isUploadButtonEnabled() {
        if (selectedCables == null || selectedCables.isEmpty()) {
            return false;
        }
        if (!sessionService.canAdminister()) {
            if (!sessionService.canManageOwnedCables()) {
                return false;
            } else {
                for (CableUI cable : selectedCables) {
                    if (!cable.getOwners().contains(sessionService.getLoggedInName())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean isChangeCableOwnerAllowed() {
        return sessionService.canAdminister();
    }

    public boolean isDeleteButtonEnabled() {
        if (selectedCables == null || selectedCables.isEmpty()) {
            return false;
        }
        for (CableUI cableToDelete : selectedCables) {
            if (cableToDelete.getStatus() == CableStatus.DELETED) {
                return false;
            }
        }
        if (sessionService.canAdminister()) {
            return true;
        } else if (sessionService.canManageOwnedCables()) {
            boolean foundNotOwned = false;
            for (CableUI cableToDelete : selectedCables) {
                if (!cableToDelete.getOwners().contains(sessionService.getLoggedInName())) {
                    foundNotOwned = true;
                    break;
                }
            }
            if (!foundNotOwned)
                return true;
        }
        return false;
    }

    public boolean isExportButtonEnabled() {
        if (cables.isEmpty()) {
            return false;
        } else if (cables.size() < NUMBER_OF_ENTITIES_PER_PAGE) {
            for (CableUI cable : cables) {
                if (cable.getStatus() != CableStatus.DELETED) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Redirects to configuration cable database device extremities.
     *
     * @param actionEvent
     *            action event
     */
    public void redirectToCCDB(ActionEvent actionEvent) {
        final String url = CableProperties.getInstance().getCCDBURL();
        redirect(url, CCDB_DEVICE_PAGE);
    }

    /**
     * Redirects to naming service device definition extremities.
     *
     * @param actionEvent
     *            action event
     */
    public void redirectToNamingService(ActionEvent actionEvent) {
        final String url = CableProperties.getInstance().getNamingURL();
        redirect(url, NAMING_DEVICE_PAGE);
    }

    /**
     * Redirects to the device extremities.
     *
     * @param url
     *            URL to redirect to
     * @param page
     *            page to redirect to
     */
    public void redirect(String url, String page) {
        if (url != null && !url.isEmpty()) {
            final StringBuilder redirectionUrl = new StringBuilder(url);
            if (redirectionUrl.charAt(redirectionUrl.length() - 1) != '/') {
                redirectionUrl.append('/');
            }
            redirectionUrl.append(page).append(selectedDevice);
            try {
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.redirect(redirectionUrl.toString().trim());
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
    }

    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<String> selectItems = new ArrayList<>();

        FacesContext context = FacesContext.getCurrentInstance();
        CableColumnUI column = (CableColumnUI) UIComponent.getCurrentComponent(context).getAttributes().get("column");
        switch (column) {
        case STATUS:
            selectItems.add(CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE);
            selectItems.add(CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE);
            for (CableStatus cableStatus : CableStatus.values()) {
                selectItems.add(cableStatus.getDisplayName());
            }
            break;
        case AUTOCALCULATEDLENGTH:
            selectItems.add(CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE);
            for (CableAutoCalculatedLength autoCalculatedLength : CableAutoCalculatedLength.values()) {
                selectItems.add(autoCalculatedLength.getDisplayName());
            }
            break;
        default:
            break;
        }
        return selectItems;
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    public void executeQuery(ActionEvent e) {
        refreshCables();
        queryService.updateQueryExecutionDate(selectedQuery.getQuery());
    }

    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            queryService.updateQueryExecutionDate(selectedQuery.getQuery());
            refreshCables();
        }
    }

    public void resetQuery() {
        selectedQuery = null;
        refreshCables();
    }

    public String getNumberOfFilteredItems() {
        if (cables.size() == NUMBER_OF_ENTITIES_PER_PAGE) {
            return String.valueOf(cables.size()) + "+";
        } else {
            return String.valueOf(cables.size());
        }

    }

    public boolean isCableRequested() {
        return isCableRequested;
    }

    /** Resets column template to default */
    private void resetColumnTemplate() {
        columnTemplate = CableColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /** Resets displayView to default */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in cable data table
     *
     * @return columns
     */
    public List<CableColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<CableColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();

            CableColumnUI column = CableColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot().findComponent(":cableTableForm:cableTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /**
     * Returns currently selected display view.
     *
     * @return selectedDisplayView
     */
    public DisplayViewUI getSelectedDisplayView() {
        return selectedDisplayView;
    }

    /**
     * Sets selected display view as current
     *
     * @param selectedDisplayView
     *            selectedDisplayView
     */
    public void setSelectedDisplayView(DisplayViewUI selectedDisplayView) {
        this.selectedDisplayView = selectedDisplayView;
    }

    /** Executed currently selected displayView */
    public void executeSelectedDisplayView() {
        executeDisplayView(getSelectedDisplayView());
    }

    /** @return number of column in current display view. */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    public void setCables(List<CableUI> cables) {
        this.cables = cables;
    }

    /**
     * Executes displayView.
     *
     * @param displayView
     *            displayView to execute
     */
    public void executeDisplayView(DisplayViewUI displayView) {
        displayView.getDisplayView().updateExecutionDate(new Date());
        displayViewService.updateDisplayView(displayView.getDisplayView());
        List<String> newColumnTemplate = new ArrayList<String>();
        List<DisplayViewColumn> columns = displayView.getDisplayViewColumns();
        Collections.sort(columns);

        for (DisplayViewColumn column : columns) {
            newColumnTemplate.add(column.getColumnName());
        }
        setColumnTemplate(newColumnTemplate);
        createDynamicColumns();
    }

    /** @return the lazy loading data model */
    public LazyDataModel<CableUI> getLazyModel() {
        return lazyModel;
    }

    /**
     * Formats the given cable by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedCable
     *            cable to format
     */
    private void formatCable(CableUI selectedCable) {
        selectedCable.setCableClass(Utility.formatWhitespaces(selectedCable.getCableClass()));
        selectedCable.setContainer(Utility.formatWhitespaces(selectedCable.getContainer()));
        selectedCable.setEndpointBuildingA(Utility.formatWhitespaces(selectedCable.getEndpointBuildingA()));
        selectedCable.setEndpointBuildingB(Utility.formatWhitespaces(selectedCable.getEndpointBuildingB()));
        selectedCable.setEndpointDeviceNameA(Utility.formatWhitespaces(selectedCable.getEndpointDeviceNameA()));
        selectedCable.setEndpointDeviceNameB(Utility.formatWhitespaces(selectedCable.getEndpointDeviceNameB()));
        selectedCable.setEndpointLabelA(Utility.formatWhitespaces(selectedCable.getEndpointLabelA()));
        selectedCable.setEndpointLabelB(Utility.formatWhitespaces(selectedCable.getEndpointLabelB()));
        selectedCable.setEndpointRackA(Utility.formatWhitespaces(selectedCable.getEndpointRackA()));
        selectedCable.setEndpointRackB(Utility.formatWhitespaces(selectedCable.getEndpointRackB()));
        selectedCable.setSubsystem(Utility.formatWhitespaces(selectedCable.getSubsystem()));
        selectedCable.setSystem(Utility.formatWhitespaces(selectedCable.getSystem()));
        selectedCable.setComments(Utility.formatWhitespaces(selectedCable.getComments()));
        //  no add/edit of revision, thus no format of revision
    }

    public RoutingRow getSelectedRoutingRow() {
        return selectedRoutingRow;
    }

    public void setSelectedRoutingRow(RoutingRow selectedRoutingRow) {
        this.selectedRoutingRow = selectedRoutingRow;
    }

    /** Swap position of current routing row and the one before it. */
    public void moveRoutingRowUp() {
        if (selectedCable.getRoutingRows() == null || selectedCable.getRoutingRows().size() <= 1) {
            return;
        }
        int position = selectedRoutingRow.getPosition();
        Optional<RoutingRow> result = selectedCable.getRoutingRows().stream()
                .filter(routingRowUI -> routingRowUI.getPosition() == position - 1).findFirst();
        if (result.isPresent()) {
            result.get().setPosition(position);
            selectedRoutingRow.setPosition(position - 1);
        }
        // Sort by position
        Collections.sort(selectedCable.getRoutingRows(), (a, b) -> (b.getPosition() < a.getPosition()) ? 0 : 1);

        calculateLength();
    }

    /** Swap position of current routing row and the one before it. */
    public void moveRoutingRowDown() {
        if (selectedCable.getRoutingRows() == null || selectedCable.getRoutingRows().size() <= 1) {
            return;
        }
        int position = selectedRoutingRow.getPosition();
        Optional<RoutingRow> result = selectedCable.getRoutingRows().stream()
                .filter(routingRow -> routingRow.getPosition() == position + 1).findFirst();
        if (result.isPresent()) {
            result.get().setPosition(position);
            selectedRoutingRow.setPosition(position + 1);
        }
        calculateLength();
    }

    /** Add column to currently selected displayView */
    public void addRoutingRow() {
        if (getRoutings().isEmpty()) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error!", "Please define routings under the routing page first."));
            return;
        }
        if (selectedCable.getRoutingRows() == null) {
            selectedCable.setRoutingRows(new ArrayList<RoutingRow>());
        }
        int position = selectedCable.getRoutingRows().size();
        RoutingRow routingRow = new RoutingRow(getRoutings().get(0), selectedCable.getCable(), position);
        selectedCable.getRoutingRows().add(routingRow);
        calculateLength();
    }

    /** Remove currently selected displayViewColumn */
    public void removeSelectedRoutingRow() {
        // Sort by position
        Collections.sort(selectedCable.getRoutingRows(), (a, b) -> (b.getPosition() < a.getPosition()) ? 0 : 1);

        if (selectedRoutingRow != null) {
            int position = selectedRoutingRow.getPosition();
            selectedCable.getRoutingRows().remove(position);
            // decrement position of all routing rows that were behind deleted one.
            for (int i = position; i < selectedCable.getRoutingRows().size(); i++) {
                selectedCable.getRoutingRows().get(i)
                        .setPosition(selectedCable.getRoutingRows().get(i).getPosition() - 1);
            }
        }
        // deleting last routing row
        if (selectedCable.getRoutingRows().isEmpty()) {
            selectedRoutingRow = null;
        }
        calculateLength();
    }

    private List<Routing> getRoutings() {
        if (routings == null) {
            routings = routingService.getActiveRoutings();
        }
        return routings;
    }

    public int getCablesToExportSize() {
        return cablesToExportSize;
    }

    public void updateCablesToExportSize() {
        this.cablesToExportSize = getCablesToExport().size();
    }

    /** @return true if current user is admin else false */
    public boolean getIsAdmin() {
        return sessionService.canAdminister();
    }

    /** Calculates selected cable length from its routing points and sets it. */
    public void calculateLength() {
        calculateLength(selectedCable);
    }

    /** Refreshed selected cable length based on changed state of auto-calculated length. */
    public void autocalculateChanged() {
        if (selectedCable.getAutoCalculatedLength() == CableAutoCalculatedLength.YES) {
            calculateLength(selectedCable);
        } else {
            // On auto-calculate length disabled revert to starting cable length
            Cable cable = selectedCable.getCable();
            selectedCable.setLength(cable != null ? cable.getLength() : 0);
        }
    }

    /**
     * Calculates cable length from its routing points and sets it.
     *
     * @param cable
     *            cable on which to calculate.
     */
    public void calculateLength(CableUI cable) {
        if (cable.getAutoCalculatedLength() == CableAutoCalculatedLength.YES) {
            float length = cable.getBaseLength();
            for (RoutingRow routingRow : cable.getRoutingRows()) {
                length += routingRow.getRouting().getLength();
            }
            cable.setLength(length);
        }
    }

    public boolean isRoutingRowTableMaximized() {
        return routingRowTableMaximized;
    }

    public void swapRoutingRowTableMaximized() {
        this.routingRowTableMaximized = !this.routingRowTableMaximized;
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay conten text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets the URL for the long general popup.
     *
     * @param longOverlayURL
     *            the url to where the popup is pointing.
     */
    public void setLongOverlayURL(String longOverlayURL) {
        this.longOverlayURL = longOverlayURL;
    }

    /** @return URL for the long general popup. */
    public String getLongOverlayURL() {
        return this.longOverlayURL;
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    public List<Routing> getAvaliableRoutings() {
        return avaliableRoutings;
    }

    public void setAvaliableRoutings(List<Routing> avaliableRoutings) {
        this.avaliableRoutings = avaliableRoutings;
    }

    /** Clears the newly created selected cable so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedCable = null;
        }
    }

    private List<String> completeLocationTags(String query, String building) {
        List<LocationTag> tags = cableService.getLocationTags();

        List<String> results = new ArrayList<>();

        for (LocationTag tag : tags) {
            String tagName = tag.toString();
            if (tagName.toLowerCase().contains(query.toLowerCase())) {
                results.add(tagName);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        if (building != null && building.toLowerCase().contains(query.toLowerCase()) && !results.contains(building)) {
            results.add(0, building);
        }

        return results;
    }

    public List<String> completeLocationATags(String query) {
        return completeLocationTags(query, selectedCable.getEndpointA().getBuilding());
    }

    public List<String> completeLocationBTags(String query) {
        return completeLocationTags(query, selectedCable.getEndpointB().getBuilding());
    }

    public GenericArtifact getUploadedArtifact() {
        return uploadedArtifact;
    }

    public void setUploadedArtifact(GenericArtifact uploadedArtifact) {
        this.uploadedArtifact = uploadedArtifact;
    }

    public CableColumnUI getSelectedArtifactColumn() {
        return selectedArtifactColumn;
    }

    public void setSelectedArtifactColumn(CableColumnUI selectedArtifactColumn) {
        this.selectedArtifactColumn = selectedArtifactColumn;
    }

    public void handleWiringAUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedCable.getEndpointA().getWiring();
        if (isArtifactUsedInNonSelectedCables(artifact)) {
            artifact = null;
        }
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedCable.getEndpointA().setWiring(artifact);
    }

    public void handleWiringBUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedCable.getEndpointB().getWiring();
        if (isArtifactUsedInNonSelectedCables(artifact)) {
            artifact = null;
        }
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedCable.getEndpointB().setWiring(artifact);
    }

    public void handleQualityReportUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedCable.getQualityReport();
        if (isArtifactUsedInNonSelectedCables(artifact)) {
            artifact = null;
        }
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedCable.setQualityReport(artifact);
    }

    public void handleArticaftUpload(FileUploadEvent event) {
        GenericArtifact artifact = uploadedArtifact;
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        uploadedArtifact = artifact;
    }

    /**
     * Returns true if the passed artifact is also used in cables that are not currently selected
     *
     * @param artifact
     *            the artifact we are checking
     * @return true if artifact is used elsewhere, false if this artifact is present only on the selected cables.
     */
       public boolean isArtifactUsedInNonSelectedCables(GenericArtifact artifact) {
        if (artifact == null || artifact.getId() == null) {
            return false;
        }
        Set<Long> cablesWithArtifactIDs = cableService.getCablesByArtifactId(artifact.getId()).stream()
                .map(m -> m.getId()).collect(Collectors.toSet());
        Set<Long> selectedCablesIDs = selectedCables.stream().map(m -> m.getCable().getId())
                .collect(Collectors.toSet());

        return !selectedCablesIDs.containsAll(cablesWithArtifactIDs);
    }

    /**
     * Uploads one artifact to all of the selected cables.
     */
    public void uploadArtifactToMultipleCables() {
        Preconditions.checkNotNull(selectedCables);
        Preconditions.checkState(!selectedCables.isEmpty());

        artifactService.updateArtifact(uploadedArtifact);

        for (CableUI cable : selectedCables) {
            final String cableName = cable.getName();
            final Cable newCable = cable.getCable();

            GenericArtifact artifact = null;
            switch (selectedArtifactColumn) {
            case QUALITY_REPORT:
                artifact = newCable.getQualityReport();
                newCable.setQualityReport(uploadedArtifact);
                break;
            case DEVICE_A_WIRING:
                artifact = newCable.getEndpointA().getWiring();
                newCable.getEndpointA().setWiring(uploadedArtifact);
                break;
            case DEVICE_B_WIRING:
                artifact = newCable.getEndpointB().getWiring();
                newCable.getEndpointB().setWiring(uploadedArtifact);
                break;
            }

            cableService.updateCable(newCable, oldCablesMap.get(newCable.getName()), sessionService.getLoggedInName(),
                    true);
            // Deleting old artifact if not used
            if (!isArtifactUsedInNonSelectedCables(artifact) && artifact != null) {
                artifactService.deleteArtifact(artifact);
            }
            Utility.showMessage(FacesMessage.SEVERITY_INFO, "Cable '" + cableName + "' updated.", null);
        }
        refreshCables();
        Utility.updateComponent(CABLE_DB_GROWL);
    }

    /**
     * Return the chess URL or empty string if there is no URL.
     *
     * @return the chess URL
     */
    private String getChessURLCableProperties() {
        //	return the chess URL (with forward slash) or empty string if there is no URL.

        String url = CableProperties.getInstance().getChessURL();
        url = url == null ? "" : url.trim();
        url = !"".equals(url) && !url.endsWith("/") ? url + "/" : url;
        return url;
    }

    /** @return the chess URL */
    public String getChessURL() {
        return chessURL;
    }

    /**
     * Return <tt>boolean</tt> if given information allows link to Chess.
     *
     * @param cableUI The cable UI component
     * @param cableColumnUI The cable column UI component
     * @return Return <tt>boolean</tt> if given information allows link to Chess.
     */
    public boolean linkToChess(final CableUI cableUI, final CableColumnUI cableColumnUI) {
        return !"".equals(getChessURL())
                && cableUI != null
                && !"".equals(StringUtils.defaultString(cableUI.getName()))
                && cableColumnUI != null
                && CableColumnUI.NAME.name().equals(cableColumnUI.name());
    }

    /**
     * Encode a URL and return the encoded value.
     *
     * @param url the url to encode
     * @return the encoded value
     *
     * @see URLUtility#encodeURL(String)
     */
    public String encodeURL(String url) {
        return URLUtility.encodeURL(url);
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Returns number of days to persist storage of cookie.
     *
     * Utility method for webapp, i.e. xhtml pages.
     *
     * @return
     */
    public int getStoragePersistenceDays() {
        return CookieUtility.PERSISTENCE_DAYS;
    }
}
