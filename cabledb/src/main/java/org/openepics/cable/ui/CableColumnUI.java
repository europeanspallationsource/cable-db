/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.openepics.cable.services.dl.CableColumn;

/**
 * Enum wrapper for CableColumn. Contains information for displaying cable column , wrapping only columns that are
 * available to be displayed in cable data table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableColumnUI {
    SYSTEM(CableColumn.SYSTEM, CableColumnUIConstants.SYSTEM_VALUE,
            null, CableColumnUIConstants.SYSTEM_TOOLTIP,
            CableColumnUIConstants.SYSTEM_STYLECLASS, CableColumnUIConstants.SYSTEM_STYLE,
            CableColumnUIConstants.SYSTEM_FILTERMODE, CableColumnUIConstants.SYSTEM_FILTERSTYLE,
            true, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    SUBSYSTEM(CableColumn.SUBSYSTEM, CableColumnUIConstants.SUBSYSTEM_VALUE,
            null, CableColumnUIConstants.SUBSYSTEM_TOOLTIP,
            CableColumnUIConstants.SUBSYSTEM_STYLECLASS, CableColumnUIConstants.SUBSYSTEM_STYLE,
            CableColumnUIConstants.SUBSYSTEM_FILTERMODE, CableColumnUIConstants.SUBSYSTEM_FILTERSTYLE,
            true, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    CLASS(CableColumn.CLASS, CableColumnUIConstants.CLASS_VALUE,
            null, CableColumnUIConstants.CLASS_TOOLTIP,
            CableColumnUIConstants.CLASS_STYLECLASS, CableColumnUIConstants.CLASS_STYLE,
            CableColumnUIConstants.CLASS_FILTERMODE, CableColumnUIConstants.CLASS_FILTERSTYLE,
            true, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    NAME(CableColumn.NAME, CableColumnUIConstants.NAME_VALUE,
            null, CableColumnUIConstants.NAME_TOOLTIP,
            CableColumnUIConstants.NAME_STYLECLASS, CableColumnUIConstants.NAME_STYLE,
            CableColumnUIConstants.NAME_FILTERMODE, CableColumnUIConstants.NAME_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    MODIFIED(CableColumn.MODIFIED, CableColumnUIConstants.MODIFIED_VALUE,
            null, CableColumnUIConstants.MODIFIED_TOOLTIP,
            CableColumnUIConstants.MODIFIED_STYLECLASS, CableColumnUIConstants.MODIFIED_STYLE,
            CableColumnUIConstants.MODIFIED_FILTERMODE, CableColumnUIConstants.MODIFIED_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    CABLE_TYPE(CableColumn.CABLE_TYPE, CableColumnUIConstants.CABLETYPE_VALUE,
            CableColumnUIConstants.CABLETYPE_URL, CableColumnUIConstants.CABLETYPE_TOOLTIP,
            CableColumnUIConstants.CABLETYPE_STYLECLASS, CableColumnUIConstants.CABLETYPE_STYLE,
            CableColumnUIConstants.CABLETYPE_FILTERMODE, CableColumnUIConstants.CABLETYPE_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.URL),
    CONTAINER(CableColumn.CONTAINER, CableColumnUIConstants.CONTAINER_VALUE,
            CableColumnUIConstants.CONTAINER_URL, CableColumnUIConstants.CONTAINER_TOOLTIP,
            CableColumnUIConstants.CONTAINER_STYLECLASS, CableColumnUIConstants.CONTAINER_STYLE,
            CableColumnUIConstants.CONTAINER_FILTERMODE, CableColumnUIConstants.CONTAINER_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DEVICE_A_NAME(CableColumn.DEVICE_A_NAME, CableColumnUIConstants.DEVICE_A_NAME_VALUE,
            CableColumnUIConstants.DEVICE_A_URL, CableColumnUIConstants.DEVICE_A_NAME_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_NAME_STYLECLASS, CableColumnUIConstants.DEVICE_A_NAME_STYLE,
            CableColumnUIConstants.DEVICE_A_NAME_FILTERMODE, CableColumnUIConstants.DEVICE_A_NAME_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.DIALOG),
    DEVICE_A_BUILDING(CableColumn.DEVICE_A_BUILDING, CableColumnUIConstants.DEVICE_A_BUILDING_VALUE,
            null, CableColumnUIConstants.DEVICE_A_BUILDING_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_BUILDING_STYLECLASS, CableColumnUIConstants.DEVICE_A_BUILDING_STYLE,
            CableColumnUIConstants.DEVICE_A_BUILDING_FILTERMODE, CableColumnUIConstants.DEVICE_A_BUILDING_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_A_RACK(CableColumn.DEVICE_A_RACK, CableColumnUIConstants.DEVICE_A_RACK_VALUE,
            null, CableColumnUIConstants.DEVICE_A_RACK_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_RACK_STYLECLASS, CableColumnUIConstants.DEVICE_A_RACK_STYLE,
            CableColumnUIConstants.DEVICE_A_RACK_FILTERMODE, CableColumnUIConstants.DEVICE_A_RACK_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_A_CONNECTOR(CableColumn.DEVICE_A_CONNECTOR, CableColumnUIConstants.DEVICE_A_CONNECTOR_VALUE,
            CableColumnUIConstants.DEVICE_A_CONNECTOR_URL, CableColumnUIConstants.DEVICE_A_CONNECTOR_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_CONNECTOR_STYLECLASS, CableColumnUIConstants.DEVICE_A_CONNECTOR_STYLE,
            CableColumnUIConstants.DEVICE_A_CONNECTOR_FILTERMODE, CableColumnUIConstants.DEVICE_A_CONNECTOR_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_A_WIRING(CableColumn.DEVICE_A_WIRING, CableColumnUIConstants.DEVICE_A_WIRING_VALUE,
            null, CableColumnUIConstants.DEVICE_A_WIRING_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_WIRING_STYLECLASS, CableColumnUIConstants.DEVICE_A_WIRING_STYLE,
            CableColumnUIConstants.DEVICE_A_WIRING_FILTERMODE, CableColumnUIConstants.DEVICE_A_WIRING_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.DOWNLOAD),
    DEVICE_A_USER_LABEL(CableColumn.DEVICE_A_USER_LABEL, CableColumnUIConstants.DEVICE_A_LABEL_VALUE,
            null, CableColumnUIConstants.DEVICE_A_LABEL_TOOLTIP,
            CableColumnUIConstants.DEVICE_A_LABEL_STYLECLASS, CableColumnUIConstants.DEVICE_A_LABEL_STYLE,
            CableColumnUIConstants.DEVICE_A_LABEL_FILTERMODE, CableColumnUIConstants.DEVICE_A_LABEL_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_B_NAME(CableColumn.DEVICE_B_NAME, CableColumnUIConstants.DEVICE_B_NAME_VALUE,
            CableColumnUIConstants.DEVICE_B_URL, CableColumnUIConstants.DEVICE_B_NAME_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_NAME_STYLECLASS, CableColumnUIConstants.DEVICE_B_NAME_STYLE,
            CableColumnUIConstants.DEVICE_B_NAME_FILTERMODE, CableColumnUIConstants.DEVICE_B_NAME_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.DIALOG),
    DEVICE_B_BUILDING(CableColumn.DEVICE_B_BUILDING, CableColumnUIConstants.DEVICE_B_BUILDING_VALUE,
            null, CableColumnUIConstants.DEVICE_B_BUILDING_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_BUILDING_STYLECLASS, CableColumnUIConstants.DEVICE_B_BUILDING_STYLE,
            CableColumnUIConstants.DEVICE_B_BUILDING_FILTERMODE, CableColumnUIConstants.DEVICE_B_BUILDING_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_B_RACK(CableColumn.DEVICE_B_RACK, CableColumnUIConstants.DEVICE_B_RACK_VALUE,
            null, CableColumnUIConstants.DEVICE_B_RACK_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_RACK_STYLECLASS, CableColumnUIConstants.DEVICE_B_RACK_STYLE,
            CableColumnUIConstants.DEVICE_B_RACK_FILTERMODE, CableColumnUIConstants.DEVICE_B_RACK_FILTERSTYLE, false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_B_CONNECTOR(CableColumn.DEVICE_B_CONNECTOR, CableColumnUIConstants.DEVICE_B_CONNECTOR_VALUE,
            CableColumnUIConstants.DEVICE_B_CONNECTOR_URL, CableColumnUIConstants.DEVICE_B_CONNECTOR_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_CONNECTOR_STYLECLASS, CableColumnUIConstants.DEVICE_B_CONNECTOR_STYLE,
            CableColumnUIConstants.DEVICE_B_CONNECTOR_FILTERMODE, CableColumnUIConstants.DEVICE_B_CONNECTOR_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DEVICE_B_WIRING(CableColumn.DEVICE_B_WIRING, CableColumnUIConstants.DEVICE_B_WIRING_VALUE,
            null, CableColumnUIConstants.DEVICE_B_WIRING_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_WIRING_STYLECLASS, CableColumnUIConstants.DEVICE_B_WIRING_STYLE,
            CableColumnUIConstants.DEVICE_B_WIRING_FILTERMODE, CableColumnUIConstants.DEVICE_B_WIRING_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.DOWNLOAD),
    DEVICE_B_USER_LABEL(CableColumn.DEVICE_B_USER_LABEL, CableColumnUIConstants.DEVICE_B_LABEL_VALUE,
            null, CableColumnUIConstants.DEVICE_B_LABEL_TOOLTIP,
            CableColumnUIConstants.DEVICE_B_LABEL_STYLECLASS, CableColumnUIConstants.DEVICE_B_LABEL_STYLE,
            CableColumnUIConstants.DEVICE_B_LABEL_FILTERMODE, CableColumnUIConstants.DEVICE_B_LABEL_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    ROUTINGS(CableColumn.ROUTINGS, CableColumnUIConstants.ROUTINGS_VALUE,
            null, CableColumnUIConstants.ROUTINGS_TOOLTIP,
            CableColumnUIConstants.ROUTINGS_STYLECLASS, CableColumnUIConstants.ROUTINGS_STYLE,
            CableColumnUIConstants.ROUTINGS_FILTERMODE, CableColumnUIConstants.ROUTINGS_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.ROUTING),
    OWNERS(CableColumn.OWNERS, CableColumnUIConstants.OWNERS_VALUE,
            null, CableColumnUIConstants.OWNERS_TOOLTIP,
            CableColumnUIConstants.OWNERS_STYLECLASS, CableColumnUIConstants.OWNERS_STYLE,
            CableColumnUIConstants.OWNERS_FILTERMODE, CableColumnUIConstants.OWNERS_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    STATUS(CableColumn.STATUS, CableColumnUIConstants.STATUS_VALUE,
            null, CableColumnUIConstants.STATUS_TOOLTIP,
            CableColumnUIConstants.STATUS_STYLECLASS, CableColumnUIConstants.STATUS_STYLE,
            CableColumnUIConstants.STATUS_FILTERMODE, CableColumnUIConstants.STATUS_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSTALLATION_DATE(CableColumn.INSTALLATION_DATE, CableColumnUIConstants.INSTALLATION_BY_VALUE,
            null, CableColumnUIConstants.INSTALLATION_BY_TOOLTIP,
            CableColumnUIConstants.INSTALLATION_BY_STYLECLASS, CableColumnUIConstants.INSTALLATION_BY_STYLE,
            CableColumnUIConstants.INSTALLATION_BY_FILTERMODE, CableColumnUIConstants.INSTALLATION_BY_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    TERMINATION_DATE(CableColumn.TERMINATION_DATE, CableColumnUIConstants.TERMINATION_BY_VALUE,
            null, CableColumnUIConstants.TERMINATION_BY_TOOLTIP,
            CableColumnUIConstants.TERMINATION_BY_STYLECLASS, CableColumnUIConstants.TERMINATION_BY_STYLE,
            CableColumnUIConstants.TERMINATION_BY_FILTERMODE, CableColumnUIConstants.TERMINATION_BY_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    QUALITY_REPORT(CableColumn.QUALITY_REPORT, CableColumnUIConstants.QUALITY_REPORT_VALUE,
            null, CableColumnUIConstants.QUALITY_REPORT_TOOLTIP,
            CableColumnUIConstants.QUALITY_REPORT_STYLECLASS, CableColumnUIConstants.QUALITY_REPORT_STYLE,
            CableColumnUIConstants.QUALITY_REPORT_FILTERMODE, CableColumnUIConstants.QUALITY_REPORT_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.DOWNLOAD),
    BASELENGTH(CableColumn.BASELENGTH, CableColumnUIConstants.BASELENGTH_VALUE,
            null, CableColumnUIConstants.BASELENGTH_TOOLTIP,
            CableColumnUIConstants.BASELENGTH_STYLECLASS, CableColumnUIConstants.BASELENGTH_STYLE,
            CableColumnUIConstants.BASELENGTH_FILTERMODE, CableColumnUIConstants.BASELENGTH_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    LENGTH(CableColumn.LENGTH, CableColumnUIConstants.LENGTH_VALUE,
            null, CableColumnUIConstants.LENGTH_TOOLTIP,
            CableColumnUIConstants.LENGTH_STYLECLASS, CableColumnUIConstants.LENGTH_STYLE,
            CableColumnUIConstants.LENGTH_FILTERMODE, CableColumnUIConstants.LENGTH_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    AUTOCALCULATEDLENGTH(CableColumn.AUTOCALCULATEDLENGTH, CableColumnUIConstants.AUTOCALCULATEDLENGTH_VALUE,
            null, CableColumnUIConstants.AUTOCALCULATEDLENGTH_TOOLTIP,
            CableColumnUIConstants.AUTOCALCULATEDLENGTH_STYLECLASS, CableColumnUIConstants.AUTOCALCULATEDLENGTH_STYLE,
            CableColumnUIConstants.AUTOCALCULATEDLENGTH_FILTERMODE,
            CableColumnUIConstants.AUTOCALCULATEDLENGTH_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    COMMENTS(CableColumn.COMMENTS, CableColumnUIConstants.COMMENTS_VALUE,
            CableColumnUIConstants.COMMENTS_URL, CableColumnUIConstants.COMMENTS_TOOLTIP,
            CableColumnUIConstants.COMMENTS_STYLECLASS, CableColumnUIConstants.COMMENTS_STYLE,
            CableColumnUIConstants.COMMENTS_FILTERMODE, CableColumnUIConstants.COMMENTS_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    REVISION(CableColumn.REVISION, CableColumnUIConstants.REVISION_VALUE,
            null, CableColumnUIConstants.REVISION_TOOLTIP,
            CableColumnUIConstants.REVISION_STYLECLASS, CableColumnUIConstants.REVISION_STYLE,
            CableColumnUIConstants.REVISION_FILTERMODE, CableColumnUIConstants.REVISION_FILTERSTYLE,
            false, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private CableColumn parent;
    private String tooltip;
    private String value;
    private String url;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private boolean filterCookie;
    private boolean renderText;
    private boolean renderCommand;
    private boolean renderDownload;
    private boolean filterDropdown;
    private String endpointValue = null;
    private String endpoint = null;
    private CableColumnStyle columnStyle;

    /**
     * CableColumnUI constructor Decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     * @param filterCookie
     *            filterCookie to enable or disable cookie for column filter
     */
    private CableColumnUI(CableColumn parent, String value, String url, String tooltip,
            String styleClass, String style,
            String filterMode, String filterStyle, boolean filterCookie,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.url = url;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.filterCookie = filterCookie;
        this.renderText = false;
        this.renderCommand = false;
        this.renderDownload = false;
        this.filterDropdown = false;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        switch (value) {
        case CableColumnUIConstants.DEVICE_A_NAME_VALUE:
        case CableColumnUIConstants.DEVICE_B_NAME_VALUE:
            this.endpointValue = "device";
            break;
        case CableColumnUIConstants.DEVICE_A_BUILDING_VALUE:
        case CableColumnUIConstants.DEVICE_B_BUILDING_VALUE:
            this.endpointValue = "building";
            break;
        case CableColumnUIConstants.DEVICE_A_RACK_VALUE:
        case CableColumnUIConstants.DEVICE_B_RACK_VALUE:
            this.endpointValue = "rack";
            break;
        case CableColumnUIConstants.DEVICE_A_CONNECTOR_VALUE:
        case CableColumnUIConstants.DEVICE_B_CONNECTOR_VALUE:
            this.endpointValue = "connector";
            break;
        case CableColumnUIConstants.DEVICE_A_WIRING_VALUE:
        case CableColumnUIConstants.DEVICE_B_WIRING_VALUE:
            this.endpointValue = "wiring";
            break;
        case CableColumnUIConstants.DEVICE_A_LABEL_VALUE:
        case CableColumnUIConstants.DEVICE_B_LABEL_VALUE:
            this.endpointValue = "label";
            break;
        case CableColumnUIConstants.STATUS_VALUE:
            this.filterDropdown = true;
            break;
        case CableColumnUIConstants.AUTOCALCULATEDLENGTH_VALUE:
            this.filterDropdown = true;
            break;
        default:
        }
        switch (value) {
        case CableColumnUIConstants.DEVICE_A_BUILDING_VALUE:
        case CableColumnUIConstants.DEVICE_A_CONNECTOR_VALUE:
        case CableColumnUIConstants.DEVICE_A_WIRING_VALUE:
        case CableColumnUIConstants.DEVICE_A_LABEL_VALUE:
        case CableColumnUIConstants.DEVICE_A_NAME_VALUE:
        case CableColumnUIConstants.DEVICE_A_RACK_VALUE:
            endpoint = "endpointA";
            break;
        case CableColumnUIConstants.DEVICE_B_BUILDING_VALUE:
        case CableColumnUIConstants.DEVICE_B_CONNECTOR_VALUE:
        case CableColumnUIConstants.DEVICE_B_WIRING_VALUE:
        case CableColumnUIConstants.DEVICE_B_RACK_VALUE:
        case CableColumnUIConstants.DEVICE_B_NAME_VALUE:
        case CableColumnUIConstants.DEVICE_B_LABEL_VALUE:
            endpoint = "endpointB";
            break;
        }
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return boolean filterCookie */
    public boolean getFilterCookie() {
        return filterCookie;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /** @return boolean renderText */
    public boolean getRenderText() {
        return renderText;
    }

    /** @return boolean renderCommand */
    public boolean getRenderCommand() {
        return renderCommand;
    }

    /** @return boolean renderDownload */
    public boolean getRenderDownload() {
        return renderDownload;
    }

    /**
     * Searches for CableColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return CableColumnUI enum
     */
    public static CableColumnUI convertColumnLabel(String columnLabel) {
        for (CableColumnUI value : CableColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (CableColumnUI value : CableColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public boolean isFilterDropdown() {
        return filterDropdown;
    }

    public String getEndpointValue() {
        return endpointValue;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

    public String getUrl() {
        return url;
    }

}
