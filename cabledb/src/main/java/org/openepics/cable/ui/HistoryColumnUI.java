/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Contains information for displaying and getting data for history column
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum HistoryColumnUI {
    ENTITY_NAME("Entity Name", "Entity name.", "entityName"),
    ENTITY_TYPE("Entity type", "Entity Type.", "entityType"),
    ENTITY_ID("Entity ID", "Entity ID.", "entityId"),
    USER_ID("User", "User.", "userId"),
    OPERATION("Operation", "Operation.", "operation"),
    ENTRY("Change", "Change description.", "entry"),
    TIMESTAMP("Timestamp", "History entry timestamp.", "timestamp");

    private String label;
    private String tooltip;
    private String value;

    /**
     * HistoryColumnUI constructor Stores properties of History Columns.
     *
     * @param label
     *            label of the column
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param value
     *            property for getting data to display
     */
    private HistoryColumnUI(String label, String tooltip, String value) {
        this.label = label;
        this.tooltip = tooltip;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getTooltip() {
        return tooltip;
    }

    public String getValue() {
        return value;
    }

}
