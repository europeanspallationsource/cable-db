/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.Set;

import org.openepics.cable.model.Routing;
import org.openepics.cable.services.DateUtil;

/**
 * <code>RoutingUI</code> is a presentation of {@link Routing} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class RoutingUI implements Serializable {

    private static final long serialVersionUID = 8115229002220900451L;

    private final Routing routing;

    /**
     * Constructs the ui object for empty routing instance.
     */
    public RoutingUI() {
        this.routing = new Routing();
    }

    /**
     * Constructs the ui object for the given routing instance.
     *
     * @param routing
     *            the routing instance
     */
    public RoutingUI(Routing routing) {
        this.routing = routing;
    }

    /** @return the routing instance this wraps */
    public Routing getRouting() {
        return routing;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Routing  Name:");
        sb.append(' ');
        sb.append(routing.getName());
        return sb.toString();
    }

    public Long getId() {
        return routing.getId();
    }

    public String getName() {
        return routing.getName();
    }

    public void setName(String name) {
        routing.setName(name);
    }

    public String getDescription() {
        return routing.getDescription();
    }

    public void setDescription(String description) {
        routing.setDescription(description);
    }

    public String getCableClasses() {
        return routing.getCableClassesAsString();
    }

    public void setCableClasses(Set<String> cableClasses) {
        routing.setCableClasses(cableClasses);
    }

    public String getLocation() {
        return routing.getLocation();
    }

    public void setLocation(String location) {
        routing.setLocation(location);
    }

    public Float getLength() {
        return routing.getLength();
    }

    public void setLength(Float length) {
        routing.setLength(length);
    }

    public String getCreated() {
        return DateUtil.format(routing.getCreated());
    }

    public String getModified() {
        return DateUtil.format(routing.getModified());
    }

    public String getOwner() {
        return routing.getOwner();
    }

    public void setOwner(String owner) {
        routing.setOwner(owner);
    }

    public boolean isActive() {
        return routing.isActive();
    }

    public void setActive(boolean status) {
        routing.setActive(status);
    }

    public String getRevision() {
        return routing.getRevision();
    }

    public void setRevision(String revision) {
        routing.setRevision(revision);
    }

    @Override
    public int hashCode() {
        return routing.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return routing.equals(((RoutingUI) obj).getRouting());
    }
}
