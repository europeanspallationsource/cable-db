/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.List;

import org.openepics.cable.model.DisplayView;
import org.openepics.cable.model.DisplayViewColumn;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.services.DateUtil;

/**
 * DisplayView class wrapper containing additional methods for UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DisplayViewUI implements Serializable {

    private static final long serialVersionUID = -5599461109679532439L;

    private DisplayView displayView;

    /**
     * Constructor
     *
     * @param displayView
     *            to be wrapped
     */
    public DisplayViewUI(DisplayView displayView) {
        this.displayView = displayView;
    }

    /** @return displayView wrapped displayView */
    public DisplayView getDisplayView() {
        return displayView;
    }

    /** @return Id wrapped displayView id */
    public Long getId() {
        return displayView.getId();
    }

    /** @return date of creation */
    public String getCreated() {
        return DateUtil.format(displayView.getCreated());
    }

    /** @return description */
    public String getDescription() {
        return displayView.getDescription();
    }

    /**
     * Set description
     *
     * @param description
     *            description to set
     */
    public void setDescription(String description) {
        displayView.setDescription(description);
    }

    /** @return owner wrapped displayView owner */
    public String getOwner() {
        return displayView.getOwner();
    }

    /** @return entityType */
    public EntityType getEntityType() {
        return displayView.getEntityType();
    }

    /**
     * Set entityType
     *
     * @param entityType
     *            entityType to set
     */
    public void setEntityType(EntityType entityType) {
        displayView.setEntityType(entityType);
    }

    /** @return columns */
    public List<DisplayViewColumn> getDisplayViewColumns() {
        return displayView.getColumns();
    }

    /**
     * Set columns
     *
     * @param columns
     *            columns to set
     */
    public void setDisplayViewColumns(List<DisplayViewColumn> columns) {
        displayView.setColumns(columns);
    }
}
