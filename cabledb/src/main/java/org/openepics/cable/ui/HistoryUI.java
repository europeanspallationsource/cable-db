/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;

import org.openepics.cable.model.History;
import org.openepics.cable.services.DateUtil;

/**
 * <code>HistoryUI</code> is a presentation of {@link History} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class HistoryUI implements Serializable {

    private static final long serialVersionUID = 2259657237212592980L;

    private final History history;

    /**
     * Constructs the UI object for the given history instance.
     *
     * @param history
     *            the history instance
     */
    public HistoryUI(History history) {
        this.history = history;
    }

    /** @return the history instance this wraps */
    public History getHistory() {
        return history;
    }

    /** @return the history timestamp. */
    public String getTimestamp() {
        return DateUtil.format(history.getTimestamp());
    }

    /** @return the history user. */
    public String getUser() {
        return history.getUserId();
    }

    /** @return the history operation. */
    public String getOperation() {
        return history.getOperation().getDisplayName();
    }

    /** @return the history entity name. */
    public String getEntityName() {
        return history.getEntityName();
    }

    /** @return the history entity type. */
    public String getEntityType() {
        return history.getEntityType().getDisplayName();
    }

    /** @return the history entity id. */
    public Long getEntityId() {
        return history.getEntityId();
    }

    /** @return the history entry. */
    public String getEntry() {
        return history.getEntry();
    }
}
