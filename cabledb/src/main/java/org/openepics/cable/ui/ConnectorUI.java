/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.Date;

import org.openepics.cable.model.Connector;
import org.openepics.cable.model.GenericArtifact;
import org.openepics.cable.services.DateUtil;

/**
 * <code>ConnectorUI</code> is a presentation of {@link Connector} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class ConnectorUI implements Serializable {

    private static final long serialVersionUID = 8115229002220900451L;

    /** String representing valid connector status. */
    public static final String STATUS_VALID = "VALID";
    /** String representing obsolete connector status. */
    public static final String STATUS_OBSOLETE = "OBSOLETE";

    private final Connector connector;

    public ConnectorUI() {
        this.connector = new Connector();
    }

    /**
     * Constructs the ui object for the given connector instance.
     *
     * @param connector
     *            the connector instance
     */
    public ConnectorUI(Connector connector) {
        this.connector = connector;
    }

    /** @return the connector instance this wraps */
    public Connector getConnector() {
        return connector;
    }

    /** @return the connector database id. */
    public Long getId() {
        return connector.getId();
    }

    /**
     * @return the creation date of this connector as string
     *
     * @see Connector#getCreated()
     */
    public String getCreated() {
        return DateUtil.format(connector.getCreated());
    }

    public String getName() {
        return connector.getName();
    }

    public void setName(String name) {
        connector.setName(name);
    }

    public String getDescription() {
        return connector.getDescription();
    }

    public void setDescription(String description) {
        connector.setDescription(description);
    }

    public String getType() {
        return connector.getType();
    }

    public void setType(String type) {
        connector.setType(type);
    }

    public void setCreated(Date created) {
        connector.setCreated(created);
    }

    public GenericArtifact getDatasheet() {
        return connector.getDataSheet();
    }

    public void setDatasheet(GenericArtifact dataSheet) {
        connector.setDataSheet(dataSheet);
    }

    public String getDatasheetName() {
        if (connector.getDataSheet() != null) {
            return connector.getDataSheet().getName();
        }
        return "";
    }

    public boolean isActive() {
        return connector.isActive();

    }

    /** @return connector.get {@link #STATUS_VALID} if connector is in active use, {@link #STATUS_OBSOLETE} else */
    public String getValid() {
        return connector.isActive() ? STATUS_VALID : STATUS_OBSOLETE;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Connector Name:");
        sb.append(' ');
        sb.append(getName());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((connector == null) ? 0 : connector.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConnectorUI other = (ConnectorUI) obj;
        if (connector != null)
            return connector.equals(other.connector);
        return true;
    }
}
