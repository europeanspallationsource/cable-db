/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.openepics.cable.model.DisplayView;
import org.openepics.cable.model.DisplayViewColumn;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.services.DisplayViewService;
import org.openepics.cable.services.SessionService;

/**
 * DisplayView back bean.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class DisplayViewRequestManager implements Serializable {

    private static final long serialVersionUID = 4780839104285445681L;

    private static final List<DisplayViewUI> EMPTY_LIST = new ArrayList<>();

    @Inject
    private SessionService sessionService;
    @Inject
    private transient DisplayViewService displayViewService;

    private List<DisplayViewUI> displayViews;
    private List<DisplayViewUI> filteredDisplayViews;
    private List<DisplayViewUI> mostRecentDisplayViews;
    private List<DisplayViewColumn> displayViewColumns;

    private List<DisplayViewColumn> selectedDisplayViewColumns;
    private List<String> validationMessages;
    private DisplayViewUI selectedDisplayView;
    private String description;
    private boolean isDisplayViewSelected;
    private boolean isDisplayViewColumnSelected;
    private boolean isCableDisplayView;

    @PostConstruct
    public void init() {
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        isCableDisplayView = "/cables.xhtml".equals(viewId);
        refreshDisplayViews(null);
        refreshMostRecentDisplayViews();
    }

    /** DisplayView selection listener */
    public void onDisplayViewSelect() {
        if (selectedDisplayView != null) {
            selectDisplayView(selectedDisplayView);
        } else {
            unselectDisplayView();
        }
    }

    /** DisplayViewColumn selection listener */
    public void onDisplayViewColumnSelect() {
        if (selectedDisplayView != null && selectedDisplayViewColumns != null
                && !selectedDisplayViewColumns.isEmpty()) {
            isDisplayViewColumnSelected = true;
        } else {
            selectedDisplayViewColumns = null;
            isDisplayViewColumnSelected = false;
        }
    }

    /** @return displayViews */
    public List<DisplayViewUI> getDisplayViews() {
        if (!sessionService.isLoggedIn()) {
            return EMPTY_LIST;
        }
        if (displayViews.isEmpty()) {
            refreshDisplayViews(null);
        }
        return displayViews;
    }

    /** @return most recent DisplayViews */
    public List<DisplayViewUI> getMostRecentDisplayViews() {
        if (!sessionService.isLoggedIn()) {
            return EMPTY_LIST;
        }
        if (mostRecentDisplayViews.isEmpty()) {
            refreshMostRecentDisplayViews();
        }
        return mostRecentDisplayViews;
    }

    /** @return most recent DisplayView */
    public DisplayViewUI getFirstMostRecentDisplayView() {
        if (!mostRecentDisplayViews.isEmpty()) {
            return mostRecentDisplayViews.get(0);
        }
        return null;
    }

    /** @return second most recent DisplayViews */
    public DisplayViewUI getSecondMostRecentDisplayView() {
        if (mostRecentDisplayViews.size() >= 2) {
            return mostRecentDisplayViews.get(1);
        }
        return null;
    }

    /** @return third most recent DisplayViews */
    public DisplayViewUI getThirdMostRecentDisplayView() {
        if (mostRecentDisplayViews.size() >= 3) {
            return mostRecentDisplayViews.get(2);
        }
        return null;
    }

    /** @return displayViewsColumns */
    public List<DisplayViewColumn> getDisplayViewColumns() {
        return displayViewColumns;
    }

    /**
     * Sets displayViewsColumns
     *
     * @param displayViewColumns
     *            displayViewColumns to set
     */
    public void setDisplayViewColumns(List<DisplayViewColumn> displayViewColumns) {
        this.displayViewColumns = displayViewColumns;
    }

    /** @return filteredDisplayViews */
    public List<DisplayViewUI> getFilteredDisplayViews() {
        return filteredDisplayViews;
    }

    /**
     * Set filteredDisplayViews
     *
     * @param filteredDisplayViews
     *            filteredDisplayViews to set
     */
    public void setFilteredDisplayViews(List<DisplayViewUI> filteredDisplayViews) {
        this.filteredDisplayViews = filteredDisplayViews;
    }

    /** @return selectedDisplayViews */
    public DisplayViewUI getSelectedDisplayView() {
        return selectedDisplayView;
    }

    /**
     * Set selectedDisplayViews
     *
     * @param selectedDisplayView
     *            DisplayView to set as selected
     */
    public void setSelectedDisplayView(DisplayViewUI selectedDisplayView) {
        this.selectedDisplayView = selectedDisplayView;
    }

    /** @return selectedDisplayViewColumns */
    public List<DisplayViewColumn> getSelectedDisplayViewColumns() {
        return selectedDisplayViewColumns;
    }

    /**
     * Set selectedDisplayViewColumns
     *
     * @param selectedDisplayViewColumns
     *            DisplayViewColumns to set as selected
     */
    public void setSelectedDisplayViewColumns(List<DisplayViewColumn> selectedDisplayViewColumns) {
        this.selectedDisplayViewColumns = selectedDisplayViewColumns;
    }

    /** @return description */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     *
     * @param description
     *            description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** @return displayViewSelected */
    public boolean getDisplayViewSelected() {
        return isDisplayViewSelected;
    }

    /**
     * Set displayViewSelected
     *
     * @param isDisplayViewSelected
     *            boolean indicating if any displayView is selected
     */
    public void setDisplayViewSelected(boolean isDisplayViewSelected) {
        this.isDisplayViewSelected = isDisplayViewSelected;
    }

    /** @return displayViewColumnSelected */
    public boolean getDisplayViewColumnSelected() {
        return isDisplayViewColumnSelected;
    }

    /**
     * Set displayViewColumnSelected
     *
     * @param isDisplayViewColumnSelected
     *            boolean indicating if any displayViewColumn is selected
     */
    public void setDisplayViewColumnSelected(boolean isDisplayViewColumnSelected) {
        this.isDisplayViewColumnSelected = isDisplayViewColumnSelected;
    }

    /** @return all valid column names */
    public List<String> getValidColumnNames() {
        if (isCableDisplayView) {
            return CableColumnUI.getAllColumns();
        } else {
            return CableTypeColumnUI.getAllColumns();
        }
    }

    /** @return validationMessages */
    public List<String> getValidationMessages() {
        return validationMessages;
    }

    /** Add displayView */
    public void addDisplayView() {
        if (!sessionService.isLoggedIn()) {
            return;
        }
        EntityType entityType = isCableDisplayView ? EntityType.CABLE : EntityType.CABLE_TYPE;
        List<DisplayViewColumn> defaultDisplayViewColumns = new ArrayList<DisplayViewColumn>();
        DisplayViewColumn displayViewColumn = new DisplayViewColumn();
        displayViewColumn.setPosition(0);
        displayViewColumn.setColumnName(isCableDisplayView ? CableColumnUI.CABLE_TYPE.getColumnLabel()
                : CableTypeColumnUI.NAME.getColumnLabel());
        defaultDisplayViewColumns.add(displayViewColumn);
        DisplayView displayView = displayViewService.saveDisplayView(description, entityType,
                sessionService.getLoggedInName(), defaultDisplayViewColumns);
        refreshDisplayViews(displayView.getId());
        refreshMostRecentDisplayViews();
    }

    /** Remove currently selected displayView */
    public void removeSelectedDisplayView() {
        if (selectedDisplayView != null) {
            displayViewService.deleteDisplayView(selectedDisplayView.getDisplayView());
            refreshDisplayViews(null);
            refreshMostRecentDisplayViews();
            unselectDisplayView();
        }
    }

    /** Edit currently selected displayView */
    public void editSelectedDisplayView() {
        if (selectedDisplayView != null) {
            selectedDisplayView.setDescription(description);
            selectedDisplayView.setEntityType(isCableDisplayView ? EntityType.CABLE : EntityType.CABLE_TYPE);
            DisplayView displayView = displayViewService.updateDisplayView(selectedDisplayView.getDisplayView());
            refreshDisplayViews(displayView.getId());
            refreshMostRecentDisplayViews();
        }
    }

    /** Duplicate currently selected displayView */
    public void duplicateSelectedDisplayView() {
        if (selectedDisplayView != null && sessionService.isLoggedIn()) {
            List<DisplayViewColumn> duplicatedDisplayViewColumns = new ArrayList<DisplayViewColumn>();
            for (DisplayViewColumn displayViewColumn : selectedDisplayView.getDisplayViewColumns()) {
                DisplayViewColumn displayViewColumnDuplicate = new DisplayViewColumn();
                displayViewColumnDuplicate.setColumnName(displayViewColumn.getColumnName());
                displayViewColumnDuplicate.setPosition(displayViewColumn.getPosition());
                duplicatedDisplayViewColumns.add(displayViewColumnDuplicate);
            }
            EntityType entityType = selectedDisplayView.getEntityType();
            DisplayView displayView = displayViewService.saveDisplayView(description, entityType,
                    sessionService.getLoggedInName(), duplicatedDisplayViewColumns);
            refreshDisplayViews(displayView.getId());
            refreshMostRecentDisplayViews();
        }
    }

    /** Add column to currently selected displayView */
    public void addDisplayViewColumn() {
        if (selectedDisplayView != null) {
            DisplayView displayView = selectedDisplayView.getDisplayView();
            int position = displayViewColumns.size();
            DisplayViewColumn displayViewColumn = new DisplayViewColumn(displayView, getValidColumnNames().get(0),
                    position);
            if (selectedDisplayViewColumns != null && selectedDisplayViewColumns.size() == 1) {
                int selectedPosition = selectedDisplayViewColumns.get(0).getPosition();
                displayViewColumns.add(selectedPosition, displayViewColumn);
            } else {
                displayViewColumns.add(displayViewColumn);
            }
            selectedDisplayViewColumns = new ArrayList<>();
            selectedDisplayViewColumns.add(displayViewColumn);
            isDisplayViewColumnSelected = true;
            reorderDisplayViewColumns();
        }
    }

    /** Remove currently selected displayViewColumn */
    public void removeSelectedDisplayViewColumn() {
        if (selectedDisplayView != null && selectedDisplayViewColumns != null) {
            for (DisplayViewColumn displayViewColumn : selectedDisplayViewColumns) {
                displayViewColumns.remove(displayViewColumn);
            }
            reorderDisplayViewColumns();
            selectedDisplayViewColumns = null;
            isDisplayViewColumnSelected = false;
        }
    }

    /** Save currently selected displayView */
    public void saveDisplayView() {
        if (selectedDisplayView != null) {
            validationMessages = new ArrayList<>();
            boolean isValid = validateDisplayViewColumns();
            if (isValid) {
                selectedDisplayView.setDisplayViewColumns(displayViewColumns);
                DisplayView displayView = selectedDisplayView.getDisplayView();
                DisplayView updatedDisplayView = displayViewService.updateDisplayView(displayView);
                refreshDisplayViews(updatedDisplayView.getId());
                refreshMostRecentDisplayViews();
            }
        }
    }

    /** Prepare popup dialog for adding displayView */
    public void prepareAddPopup() {
        description = "";
    }

    /** Prepeare popup dialog for duplicating displayView */
    public void prepareEditDuplicatePopup() {
        if (selectedDisplayView != null) {
            description = selectedDisplayView.getDescription();
        }
    }

    public void onDialogOpen() {
        refreshDisplayViews(null);
        refreshMostRecentDisplayViews();
    }

    public void onDisplayViewExecute() {
        refreshMostRecentDisplayViews();
    }

    /**
     * Check if displayView description is unique.
     *
     * @param ctx
     *            ctx
     * @param component
     *            component
     * @param value
     *            value
     * @throws ValidatorException
     *             if displayView description is not uniqe
     */
    public void isDescriptionUnique(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        if (!sessionService.isLoggedIn()) {
            return;
        }
        EntityType entityType = isCableDisplayView ? EntityType.CABLE : EntityType.CABLE_TYPE;
        if (!displayViewService.isDescriptionUnique(String.valueOf(value), sessionService.getLoggedInName(),
                entityType)) {
            throw new ValidatorException(new FacesMessage("DisplayView description should be unique."));
        }
    }

    public boolean isDisplayViewColumnUpButtonEnabled() {
        return selectedDisplayView != null && selectedDisplayViewColumns != null
                && selectedDisplayViewColumns.size() == 1 && selectedDisplayViewColumns.get(0).getPosition() != 0;
    }

    public boolean isDisplayViewColumnDownButtonEnabled() {
        if (selectedDisplayView == null) {
            return false;
        }

        int chosenPosition = selectedDisplayView.getDisplayViewColumns().size() - 1;
        return selectedDisplayViewColumns != null && selectedDisplayViewColumns.size() == 1
                && selectedDisplayViewColumns.get(0).getPosition() != chosenPosition;
    }

    /** Reorders displayView columns positions */
    private void reorderDisplayViewColumns() {
        for (int i = 0; i < displayViewColumns.size(); i++) {
            displayViewColumns.get(i).setPosition(i);
        }
    }

    /** Swap position of current displayView and the one before it. */
    public void moveDisplayViewColumnUp() {
        if (selectedDisplayViewColumns == null || selectedDisplayViewColumns.size() != 1) {
            return;
        }

        int position = selectedDisplayViewColumns.get(0).getPosition();
        if (position != 0 || position < displayViewColumns.size()) {
            DisplayViewColumn tmp = displayViewColumns.get(position - 1);
            displayViewColumns.set(position - 1, displayViewColumns.get(position));
            displayViewColumns.set(position, tmp);
            reorderDisplayViewColumns();
        }
    }

    /** Swap position of current displayView and the one behind it. */
    public void moveDisplayViewColumnDown() {
        if (selectedDisplayViewColumns == null || selectedDisplayViewColumns.size() != 1) {
            return;
        }

        int position = selectedDisplayViewColumns.get(0).getPosition();
        if (position != displayViewColumns.size() - 1 && position >= 0) {
            DisplayViewColumn tmp = displayViewColumns.get(position + 1);
            displayViewColumns.set(position + 1, displayViewColumns.get(position));
            displayViewColumns.set(position, tmp);
            reorderDisplayViewColumns();
        }
    }

    /**
     * Check if all displayView columns are valid.
     *
     * @return if all displayView columns are valid
     */
    private boolean validateDisplayViewColumns() {
        if (displayViewColumns == null || displayViewColumns.isEmpty()) {
            validationMessages.add("Display view columns can't be empty.");
            return false;
        }

        //  stop at first invalid item
        boolean isDisplayViewValid = true;
        for (int i = 0; i < displayViewColumns.size(); i++) {
            DisplayViewColumn displayViewColumn = displayViewColumns.get(i);
            String columnName = displayViewColumn.getColumnName();
            if (StringUtils.isEmpty(columnName)) {
                validationMessages.add("Column name must be given.");
                isDisplayViewValid = false;
                break;
            }
        }

        if (isDisplayViewValid) {
            validationMessages.add("DisplayView columns are saved.");
        }
        return isDisplayViewValid;
    }

    /** Refresh displayViews */
    private void refreshDisplayViews(Long displayViewId) {
        if (sessionService.isLoggedIn()) {
            displayViews = buildDisplayViewUIs(displayViewId, sessionService.getLoggedInName());
        } else {
            displayViews = EMPTY_LIST;
        }
    }

    /** Refresh most recently executed displayViews. */
    private void refreshMostRecentDisplayViews() {
        if (sessionService.isLoggedIn()) {
            mostRecentDisplayViews = buildMostRecentDisplayViewUIs(sessionService.getLoggedInName());
        } else {
            mostRecentDisplayViews = EMPTY_LIST;
        }
    }

    /** Builds displayViews. */
    private List<DisplayViewUI> buildDisplayViewUIs(Long displayViewId, String owner) {
        List<DisplayViewUI> displayViewUIs = new ArrayList<>();
        for (DisplayView displayView : displayViewService.getDisplayViews(owner)) {
            DisplayViewUI displayViewUI = new DisplayViewUI(displayView);
            if (displayViewId != null) {
                if (displayView.getId().longValue() == displayViewId.longValue()) {
                    selectDisplayView(displayViewUI);
                }
            } else {
                unselectDisplayView();
            }

            if ((isCableDisplayView && displayView.getEntityType() == EntityType.CABLE)
                    || (!isCableDisplayView && displayView.getEntityType() == EntityType.CABLE_TYPE)) {
                displayViewUIs.add(displayViewUI);
            }
        }

        return displayViewUIs;
    }

    /** Sets displayView as selected. */
    private void selectDisplayView(DisplayViewUI displayView) {
        selectedDisplayView = displayView;
        description = selectedDisplayView.getDescription();
        displayViewColumns = selectedDisplayView.getDisplayViewColumns();
        isDisplayViewSelected = true;
        selectedDisplayViewColumns = null;
        isDisplayViewColumnSelected = false;
    }

    /** Unselect displayView. */
    private void unselectDisplayView() {
        selectedDisplayView = null;
        isDisplayViewSelected = false;
        displayViewColumns = new ArrayList<>();
        description = "";
        selectedDisplayViewColumns = null;
        isDisplayViewColumnSelected = false;
    }

    /** Builds most recently executed displayViews. */
    private List<DisplayViewUI> buildMostRecentDisplayViewUIs(String owner) {
        return displayViewService
                .getLastThreeDisplayViews(owner, isCableDisplayView ? EntityType.CABLE : EntityType.CABLE_TYPE).stream()
                .map(DisplayViewUI::new).collect(Collectors.toList());
    }

}
