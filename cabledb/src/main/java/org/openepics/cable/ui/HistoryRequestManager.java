/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.openepics.cable.export.SimpleLogTableExporter;
import org.openepics.cable.export.SimpleTableExporter;
import org.openepics.cable.export.SimpleTableExporterFactory;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.services.HistoryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.ui.lazymodels.HistoryLazyModel;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.EncodingUtility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.Visibility;

/**
 * This is the backing requests bean for history dialogs.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class HistoryRequestManager implements Serializable, SimpleTableExporterFactory {

    private static final long serialVersionUID = -3602673962371076246L;
    private static final Logger LOGGER = Logger.getLogger(HistoryRequestManager.class.getCanonicalName());

    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;

    @Inject
    private SessionService sessionService;
    @Inject
    private transient HistoryService historyService;
    private HistoryLazyModel lazyModel;
    private long entityId = -1;
    private EntityType entityType;
    private List<HistoryUI> history;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;

    public HistoryRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = HistoryColumnUI.values().length;
        columnVisibility = new ArrayList<Boolean>();
        for (int i=0; i<numberOfColumns; i++) {
            columnVisibility.add(Boolean.TRUE);
        }
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes lazy model. */
    @PostConstruct
    public void init() {
        lazyModel = new HistoryLazyModel(historyService, this);

        // prepare datatable
        //     cookies
        //         rows per page
        //         column visibility
        Cookie[] cookies =
                ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                switch (cookie.getName()) {
                    case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                        initPaginationPageSize(cookie.getValue());
                        break;
                    case CookieUtility.CD_HISTORY_COLUMN_VISIBILITY:
                        initColumnVisibility(cookie.getValue());
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        if (!StringUtils.isEmpty(visibility)) {
            visibility = EncodingUtility.decode(visibility);
            String[] split = visibility.split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfColumns) {
                for (int i=0; i<numberOfColumns; i++) {
                    columnVisibility.set(i, Boolean.valueOf(split[i]));
                }
            }
        }
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
            PrimeFaces.current().executeScript("var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"+CookieUtility.CD_HISTORY_COLUMN_VISIBILITY+"', '"+columnVisibility2String()+"', {expires:"+CookieUtility.PERSISTENCE_DAYS+"});}");
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex
     * @return
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    public void prepareCableHistory(Long cableId) {
        LOGGER.log(Level.FINEST, "prepareCableHistory: " + cableId);
        entityType = EntityType.CABLE;
        entityId = cableId;
    }

    public void prepareCableTypeHistory(Long cableTypeId) {
        LOGGER.log(Level.FINEST, "prepareCableTypeHistory: " + cableTypeId);
        entityType = EntityType.CABLE_TYPE;
        entityId = cableTypeId;
    }

    public void prepareManufacturerHistory(Long manufacturerId) {
        LOGGER.log(Level.FINEST, "prepareManufacturerHistory: " + manufacturerId);
        entityType = EntityType.MANUFACTURER;
        entityId = manufacturerId;
    }

    public void prepareConnectorHistory(Long connectorId) {
        LOGGER.log(Level.FINEST, "prepareConnectorHistory: " + connectorId);
        entityType = EntityType.CONNECTOR;
        entityId = connectorId;
    }

    public void prepareRoutingHistory(Long routingId) {
        LOGGER.log(Level.FINEST, "prepareRoutingHistory: " + routingId);
        entityType = EntityType.ROUTING;
        entityId = routingId;
    }

    public void clearHistory() {
        LOGGER.log(Level.FINEST, "clearHistory");
        entityType = null;
        entityId = -1;
    }

    /** @return the list of all operation values. */
    public List<SelectItem> getOperationValues() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        for (EntityTypeOperation operation : EntityTypeOperation.values()) {
            selectItems.add(new SelectItem(operation, operation.getDisplayName()));
        }
        return selectItems;
    }

    /** @return the list of all entity types values. */
    public List<SelectItem> getEntityTypeValues() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        for (EntityType type : EntityType.values()) {
            selectItems.add(new SelectItem(type, type.getDisplayName()));
        }
        return selectItems;
    }

    @Override
    public SimpleTableExporter getSimpleTableExporter() {
        return new SimpleLogTableExporter(lazyModel.load(0, Integer.MAX_VALUE, lazyModel.getSortField(),
                lazyModel.getSortOrder(), lazyModel.getFilters()));
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        // unselect all selected rows (if any)
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        PrimeFaces.current().executeScript("var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"+CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE+"', "+getRows()+", {expires:"+CookieUtility.PERSISTENCE_DAYS+"});}");
    }

    /** @return the lazy loading data model */
    public LazyDataModel<HistoryUI> getLazyModel() {
        return lazyModel;
    }

    public long getEntityId() {
        return entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public List<HistoryUI> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryUI> history) {
        this.history = history;
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        // not format text as handled by UI
        this.longTextOverlayContent = longTextOverlayContent;
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

}
