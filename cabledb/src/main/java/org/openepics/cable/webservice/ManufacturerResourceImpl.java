/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.ManufacturerElement;
import org.openepics.cable.jaxb.ManufacturerResource;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.ManufacturerStatus;
import org.openepics.cable.services.ManufacturerService;

/**
 * This resource provides bulk approved and specific manufacturer data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("manufacturer")
public class ManufacturerResourceImpl implements ManufacturerResource {

    @Inject
    private ManufacturerService manufacturerService;

    @Override
    public List<ManufacturerElement> getAllManufacturers() {
        final List<ManufacturerElement> manufacturerElements = new ArrayList<>();

        for (final Manufacturer manufacturer : manufacturerService.getManufacturers()) {
            if (manufacturer.getStatus() == ManufacturerStatus.DELETED)
                continue;
            manufacturerElements.add(getManufacturerElement(manufacturer));
        }
        return manufacturerElements;
    }

    @Override
    public ManufacturerElement getManufacturer(String name) {
        final Manufacturer manufacturer = manufacturerService.getManufacturer(name);
        if (manufacturer == null)
            return null;
        if (manufacturer.getStatus() == ManufacturerStatus.DELETED)
            return null;

        return getManufacturerElement(manufacturer);
    }

    /**
     * Creates an instance of {@link ManufacturerElement} from database model object {@link Manufacturer}.
     *
     * @param manufacturer
     *            the database model object
     * @return the JAXB object
     */
    public static ManufacturerElement getManufacturerElement(Manufacturer manufacturer) {
        ManufacturerElement manufacturerElement = new ManufacturerElement();
        manufacturerElement.setName(manufacturer.getName());
        manufacturerElement.setAddress(manufacturer.getAddress());
        manufacturerElement.setCountry(manufacturer.getCountry());
        manufacturerElement.setEmail(manufacturer.getEmail());
        manufacturerElement.setPhoneNumber(manufacturer.getPhoneNumber());
        manufacturerElement.setStatus(manufacturer.getStatus().getDisplayName());
        return manufacturerElement;
    }
}
