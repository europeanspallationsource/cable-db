/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableResource;
import org.openepics.cable.jaxb.CableTypeElement;
import org.openepics.cable.jaxb.EndpointElement;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.GenericArtifact;
import org.openepics.cable.services.CableRBACDefinitions;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.util.Utility;

import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;

/**
 * This is implementation of {@link CableResource} interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableResourceImpl implements CableResource {

    private static final Logger LOGGER = Logger.getLogger(CableResourceImpl.class.getName());

    private static final String INTERNAL_ERROR = "Internal error while processing REST request";

    private static Response getOkResponse(String message) {
        return Response.ok().entity(new ResponseMessage(message)).build();
    }

    private static Response getForbiddenResponse() {
        return Response.status(Response.Status.FORBIDDEN)
                .entity(new ResponseMessage("Insufficient permissions for requested operation.")).build();
    }

    private static Response getBadResponse(String message) {
        return Response.status(Response.Status.BAD_REQUEST).entity(new ResponseMessage(message)).build();
    }

    private static Response getInternalErrorResponse(Exception exception) {
        StringWriter errorStackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(errorStackTrace));
        return Response.serverError()
                .entity(new ResponseMessage(exception.getMessage() + "\n" + errorStackTrace.toString())).build();
    }

    @Inject
    private CableService cableService;
    @Inject
    private CableTypeService cableTypeService;


    @Override
    public List<CableElement> getAllCables(List<String> field, String regExp) {

        LOGGER.fine("regExp " + regExp);

        List<CableColumn> columns = new ArrayList<CableColumn>();
        for (String fieldString : field) {
            CableColumn cableColumn = CableColumn.convertColumnLabel(fieldString);
            if (cableColumn == null) {
                return null;
            }
            columns.add(cableColumn);
        }

        columns.remove(CableColumn.STATUS);

        final List<CableElement> cableElements = new ArrayList<>();
        LOGGER.finest("Retrieving cables by regular expression");
        for (final Cable cable : cableService.getFilteredCablesByRegExp(columns, regExp)) {
            if (cable.getStatus() == CableStatus.DELETED)
                continue;
            cableElements.add(getCableElement(cable));
        }
        LOGGER.finest("	All active cables retrieved");
        return cableElements;
    }

    @Override
    public CableElement getCable(String number) {
        final Cable cable = cableService.getCableByName(number);
        if (cable == null)
            return null;
        if (cable.getStatus() == CableStatus.DELETED)
            return null;

        return getCableElement(cable);
    }

    @Override
    public Response addCable(String token, List<CableElement> cables) {

        SecurityFacade securityFacade = null;
        try {
            securityFacade = new SecurityFacade();
            if (!isTokenValid(securityFacade, token)) {
                return getForbiddenResponse();
            }

            for (CableElement cableElement : cables) {
                if (!hasCablePermission(securityFacade, cableElement, null)) {
                    return getForbiddenResponse();
                }
            }
            for (CableElement cableElement : cables) {
                createCable(cableElement, securityFacade.getLocalToken().getUsername());
            }
            return getOkResponse("Cables added.");

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, INTERNAL_ERROR, e);
            return getInternalErrorResponse(e);
        } finally {
            if (securityFacade != null) {
                securityFacade.destroy();
            }
        }
    }

    @Override
    public Response editCable(String token, List<CableElement> cables) {

        SecurityFacade securityFacade = null;
        try {
            securityFacade = new SecurityFacade();
            if (!isTokenValid(securityFacade, token)) {
                return getForbiddenResponse();
            }

            for (CableElement cableElement : cables) {
                Cable cable = cableService.getCableByName(cableElement.getName());
                if (cable == null) {
                    return getBadResponse("Cable with number " + cableElement.getName() + " could not be found.");
                }

                if (!hasCablePermission(securityFacade, cableElement, cable)) {
                    return getForbiddenResponse();
                }
            }

            for (CableElement cableElement : cables) {
                if (CableStatus.DELETED.toString().equals(cableElement.getStatus())) {
                    deleteCable(cableElement, securityFacade.getLocalToken().getUsername());
                } else {
                    updateCable(cableElement, securityFacade.getLocalToken().getUsername());
                }
            }
            return getOkResponse("Cables updated/deleted.");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, INTERNAL_ERROR, e);
            return getInternalErrorResponse(e);
        } finally {
            if (securityFacade != null) {
                securityFacade.destroy();
            }
        }
    }

    /**
     * Creates an instance of {@link CableElement} from database model object {@link Cable}.
     *
     * @param cable
     *            the database model object
     * @return the JAXB object
     */
    public static CableElement getCableElement(Cable cable) {
        CableElement cableElement = new CableElement();
        cableElement.setId(cable.getId());
        cableElement.setName(cable.getName());
        cableElement.setSystem(cable.getSystem());
        cableElement.setSubsystem(cable.getSubsystem());
        cableElement.setCableClass(cable.getCableClass());
        cableElement.setSeqNumber(cable.getSeqNumber());
        cableElement.setOwners(cable.getOwnersString());
        if (cable.getCableType() != null) {
            cableElement.setCableType(CableTypeResourceImpl.getCableTypeElement(cable.getCableType()));
        }
        cableElement.setContainer(cable.getContainer());
        cableElement.setEndpointA(getEndpointElement(cable.getEndpointA()));
        cableElement.setEndpointB(getEndpointElement(cable.getEndpointB()));
        cableElement.setRoutings(cable.getRoutingsString());
        cableElement.setInstallationBy(cable.getInstallationBy());
        cableElement.setTerminationBy(cable.getTerminationBy());
        if (cable.getQualityReport() != null) {
            cableElement.setQualityReport(cable.getQualityReport().getName());
        }
        cableElement.setAutoCalculatedLength(cable.getAutoCalculatedLength() == CableAutoCalculatedLength.YES);
        cableElement.setBaseLength(cable.getBaseLength());
        cableElement.setLength(cable.getLength());
        cableElement.setComments(cable.getComments());
        cableElement.setCreated(cable.getCreated());
        cableElement.setModified(cable.getModified());
        cableElement.setStatus(String.valueOf(cable.getStatus()));
        cableElement.setValidity(String.valueOf(cable.getValidity()));
        cableElement.setRevision(cable.getRevision());
        return cableElement;
    }

    /**
     * Creates an instance of {@link EndpointElement} from database model object {@link Endpoint}.
     *
     * @param endpoint
     *            the database model object
     * @return the JAXB object
     */
    public static EndpointElement getEndpointElement(Endpoint endpoint) {
        EndpointElement endpointElement = new EndpointElement();
        endpointElement.setDevice(endpoint.getDevice());
        endpointElement.setBuilding(endpoint.getBuilding());
        endpointElement.setRack(endpoint.getRack());
        endpointElement.setConnector(endpoint.getConnector() != null ? endpoint.getConnector().getName() : "");
        endpointElement.setLabel(endpoint.getLabel());
        endpointElement.setValidity(String.valueOf(endpoint.getValidity()));
        return endpointElement;
    }

    public CableType getCableType(CableTypeElement cableTypeElement) {
        if (cableTypeElement == null) {
            return null;
        }

        String cableTypeName = getNormalizedNull(cableTypeElement.getName());
        return cableTypeService.getCableType(cableTypeName);
    }

    public static Endpoint getEndpoint(EndpointElement endpointElement) {
        return new Endpoint(endpointElement.getDevice(), endpointElement.getBuilding(), endpointElement.getRack(),
                new Connector(endpointElement.getConnector(), new Date(), new Date()), null,
                endpointElement.getLabel());
    }

    private boolean isTokenValid(SecurityFacade securityFacade, String token) throws SecurityFacadeException {
        if (token == null || token.isEmpty()) {
            return false;
        }

        securityFacade.setToken(token.toCharArray());
        return securityFacade.isTokenValid();
    }

    private static String getNormalizedNull(String string) {
        if (string == null || string.isEmpty() || "null".equalsIgnoreCase(string)) {
            return null;
        } else {
            return string;
        }
    }

    private void createCable(CableElement cableElement, String userId) throws IllegalArgumentException {
        final GenericArtifact qualityReport =
                new GenericArtifact(cableElement.getQualityReport(),
                cableElement.getQualityReportUri(), null);

        cableService.createCable(cableElement.getSystem(), cableElement.getSubsystem(), cableElement.getCableClass(),
                Utility.splitStringIntoList(cableElement.getOwners()),
                CableStatus.convertToCableStatus(cableElement.getStatus()), getCableType(cableElement.getCableType()),
                cableElement.getContainer(),
                getEndpoint(cableElement.getEndpointA()),
                getEndpoint(cableElement.getEndpointB()), null, // getRoutingRows(cableElement.getRoutingRows()),
                cableElement.getInstallationBy(), cableElement.getTerminationBy(), qualityReport,
                CableAutoCalculatedLength.convertToCableAutoCalculatedLength(cableElement.isAutoCalculatedLength()),
                cableElement.getBaseLength(), cableElement.getLength(), cableElement.getComments(),
                cableElement.getRevision(), userId, true);
    }

    private void updateCable(CableElement cableElement, String userId) throws IllegalArgumentException {

        Cable oldCable = cableService.getCableByName(cableElement.getName(), true);
        Cable cable = cableService.getCableByName(cableElement.getName());

        final GenericArtifact qualityReport = new GenericArtifact(cableElement.getQualityReport(), null,
                null);

        cable.setSystem(cableElement.getSystem() != null ? cableElement.getSystem() : cable.getSystem());
        cable.setSubsystem(cableElement.getSubsystem() != null ? cableElement.getSubsystem() : cable.getSubsystem());
        cable.setCableClass(
                cableElement.getCableClass() != null ? cableElement.getCableClass() : cable.getCableClass());
        cable.setSeqNumber(cableElement.getSeqNumber() != null ? cableElement.getSeqNumber() : cable.getSeqNumber());
        cable.setOwners(cableElement.getOwners() != null ? Utility.splitStringIntoList(cableElement.getOwners())
                : cable.getOwners());
        cable.setStatus(CableStatus.convertToCableStatus(cableElement.getStatus()));
        cable.setCableType(getCableType(cableElement.getCableType()));
        cable.setContainer(cableElement.getContainer() != null ? cableElement.getContainer() : cable.getContainer());
        cable.setInstallationBy(cableElement.getInstallationBy() != null ? cableElement.getInstallationBy()
                : cable.getInstallationBy());
        cable.setTerminationBy(
                cableElement.getTerminationBy() != null ? cableElement.getTerminationBy() : cable.getTerminationBy());
        cable.setQualityReport(qualityReport);
        cable.setAutoCalculatedLength(
                CableAutoCalculatedLength.convertToCableAutoCalculatedLength(cableElement.isAutoCalculatedLength()));
        cable.setLength(cableElement.getLength() != null ? cableElement.getLength() : cable.getLength());
        cable.setBaseLength(cableElement.getBaseLength());
        cable.setComments(cableElement.getComments() != null ? cableElement.getComments() : cable.getComments());
        cable.setRevision(cableElement.getRevision() != null ? cableElement.getRevision() : cable.getRevision());

        updateEndpoint(cable.getEndpointA(), cableElement.getEndpointA());
        updateEndpoint(cable.getEndpointB(), cableElement.getEndpointB());

        cableService.updateCable(cable, oldCable, userId, true);
    }

    private void updateEndpoint(Endpoint endpoint, EndpointElement endpointElement) {
        if (endpointElement != null) {
            endpoint.update(endpointElement.getDevice() != null ? endpointElement.getDevice() : endpoint.getDevice(),
                    endpointElement.getBuilding() != null ? endpointElement.getBuilding() : endpoint.getBuilding(),
                    endpointElement.getRack() != null ? endpointElement.getRack() : endpoint.getRack(),
                    endpointElement.getConnector() != null
                            ? new Connector(endpointElement.getConnector(), new Date(), new Date())
                            : endpoint.getConnector(),
                    null, endpointElement.getLabel() != null ? endpointElement.getLabel() : endpoint.getLabel());
        }
    }

    private void deleteCable(CableElement cableElement, String userId) {
        Cable cable = cableService.getCableByName(cableElement.getName());
        cableService.deleteCable(cable, userId);
    }

    private boolean hasCablePermission(SecurityFacade securityFacade, CableElement cableElement, Cable cable)
            throws IllegalArgumentException, SecurityFacadeException {
        if (securityFacade == null) {
            return false;
        }

        boolean canAdminister = securityFacade.hasPermission(CableRBACDefinitions.CABLE_DB_RESOURCE,
                CableRBACDefinitions.ADMINISTER_CABLE_DB_PERMISSION);
        boolean canManageOwnedCables = securityFacade.hasPermission(CableRBACDefinitions.CABLE_DB_RESOURCE,
                CableRBACDefinitions.MANAGE_OWNED_CABLES_PERMISSION);

        if (canAdminister) {
            return true;
        }

        if (!canManageOwnedCables) {
            return false;
        }

        String username = securityFacade.getToken().getUsername();

        // if cable has no owner set assume the logged in user if the user is a cable user
        if (cableElement.getOwners() == null) {
            cableElement.setOwners(username);
        }

        String existingOwners = cable != null ? cable.getOwnersString() : username;
        String newOwners = cableElement.getOwners() != null ? cableElement.getOwners() : username;

        return username.equals(existingOwners) && username.equals(newOwners);
    }
}
