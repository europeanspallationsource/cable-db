/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableEndpointsElement;
import org.openepics.cable.jaxb.CableEndpointsResource;
import org.openepics.cable.jaxb.CableResource;
import org.openepics.cable.jaxb.EndpointElement;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.dl.CableColumn;

/**
 * This is implementation of {@link CableResource} interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableEndpointsResourceImpl implements CableEndpointsResource {

    private static final Logger LOGGER = Logger.getLogger(CableEndpointsResourceImpl.class.getName());

    @Inject
    private CableService cableService;

    @Override
    public List<CableEndpointsElement> getAllCableEndpoints(String newerThan) {

        LOGGER.fine("Modified: " + newerThan);

        final List<CableEndpointsElement> cableElements = new ArrayList<>();
        LOGGER.finest("Retrieving cables newer than 'modified'");
        final Query query;

        if (newerThan != null) {
            final List<QueryCondition> conditions = new ArrayList<QueryCondition>();
            query = new Query("modifiedQuery", EntityType.CABLE, null, null, conditions);
            conditions.add(new QueryCondition(query, QueryParenthesis.NONE, CableColumn.MODIFIED.getColumnLabel(),
                    QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO, newerThan, QueryParenthesis.NONE,
                    QueryBooleanOperator.NONE, 0));
        } else {
            query = null;
        }
        final List<Cable> cables = cableService.getCableEndpoints(query);
        for (final Cable cable : cables) {
            cableElements.add(getCableEndpointElement(cable));
        }
        LOGGER.finest("	All cable endpoints retrieved");
        return cableElements;
    }

    @Override
    public CableEndpointsElement getCableEndpoints(String number) {
        final Cable cable = cableService.getCableByName(number);
        if (cable == null)
            return null;
        return getCableEndpointElement(cable);
    }

    /**
     * Creates an instance of {@link CableElement} from database model object {@link Cable}.
     *
     * @param cable
     *            the database model object
     * @return the JAXB object
     */
    public static CableEndpointsElement getCableEndpointElement(Cable cable) {
        CableEndpointsElement cableElement = new CableEndpointsElement();
        cableElement.setName(cable.getName());
        cableElement.setEndpointA(getEndpointElement(cable.getEndpointA()));
        cableElement.setEndpointB(getEndpointElement(cable.getEndpointB()));
        cableElement.setModified(cable.getModified());
        cableElement.setStatus(String.valueOf(cable.getStatus()));
        cableElement.setValidity(String.valueOf(cable.getValidity()));
        return cableElement;
    }

    /**
     * Creates an instance of {@link EndpointElement} from database model object {@link Endpoint}.
     *
     * @param endpoint
     *            the database model object
     * @return the JAXB object
     */
    public static EndpointElement getEndpointElement(Endpoint endpoint) {
        EndpointElement endpointElement = new EndpointElement();
        endpointElement.setDevice(endpoint.getDevice());
        endpointElement.setBuilding(endpoint.getBuilding());
        endpointElement.setRack(endpoint.getRack());
        endpointElement.setConnector(endpoint.getConnector() != null ? endpoint.getConnector().getName() : "");
        endpointElement.setLabel(endpoint.getLabel());
        endpointElement.setValidity(String.valueOf(endpoint.getValidity()));
        return endpointElement;
    }

    public static Endpoint getEndpoint(EndpointElement endpointElement) {
        return new Endpoint(endpointElement.getDevice(), endpointElement.getBuilding(), endpointElement.getRack(),
                new Connector(endpointElement.getConnector(), new Date(), new Date()), null,
                endpointElement.getLabel());
    }

}
