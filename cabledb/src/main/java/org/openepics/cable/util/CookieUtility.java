/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

public class CookieUtility {

    /*
     * Persistence
     *     age
     */

    public static final int PERSISTENCE_DAYS = 365;

    /*
     * Delimiter for multiple entries
     */

    public static final String DELIMITER_ENTRIES = ",";

    /*
     * Cookie names
     *     project, tab, type, purpose, detail
     */

    // cables tab
    //     column, filter
    //     column, visibility
    //     pagination, page size
    public static final String CD_CABLE_COLUMN_FILTER_SYSTEM = "CD_CABLE_COLUMN_FILTER_SYSTEM";
    public static final String CD_CABLE_COLUMN_FILTER_SUBSYSTEM = "CD_CABLE_COLUMN_FILTER_SUBSYSTEM";
    public static final String CD_CABLE_COLUMN_FILTER_CLASS = "CD_CABLE_COLUMN_FILTER_CLASS";
    public static final String CD_CABLE_COLUMN_VISIBILITY = "CD_CABLE_COLUMN_VISIBILITY";
    public static final String CD_CABLE_PAGINATION_PAGE_SIZE = "CD_CABLE_PAGINATION_PAGE_SIZE";

    // cable types tab
    //     column, visibility
    public static final String CD_CABLETYPE_COLUMN_VISIBILITY = "CD_CABLETYPE_COLUMN_VISIBILITY";

    // routings tab
    //     column, visibility
    public static final String CD_ROUTING_COLUMN_VISIBILITY = "CD_ROUTING_COLUMN_VISIBILITY";

    // connectors tab
    //     column, visibility
    public static final String CD_CONNECTOR_COLUMN_VISIBILITY = "CD_CONNECTOR_COLUMN_VISIBILITY";

    // manufacturers tab
    //     column, visibility
    public static final String CD_MANUFACTURER_COLUMN_VISIBILITY = "CD_MANUFACTURER_COLUMN_VISIBILITY";

    // history/log tab
    //     column, visibility
    public static final String CD_HISTORY_COLUMN_VISIBILITY = "CD_HISTORY_COLUMN_VISIBILITY";

}
