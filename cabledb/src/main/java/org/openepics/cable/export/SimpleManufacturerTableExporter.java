/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.services.dl.ManufacturerColumn;
import org.openepics.cable.ui.ManufacturerUI;
import org.openepics.cable.util.Utility;

public class SimpleManufacturerTableExporter extends SimpleTableExporter {

    private List<ManufacturerUI> selectedManufacturers;
    private List<ManufacturerUI> filteredManufacturers;

    public SimpleManufacturerTableExporter(List<ManufacturerUI> selectedManufacturers,
            List<ManufacturerUI> filteredManufacturers) {
        this.selectedManufacturers = selectedManufacturers;
        this.filteredManufacturers = filteredManufacturers;

        setIncludeHeaderRow(true);
    }

    @Override
    protected String getTableName() {
        return "Manufacturers";
    }

    @Override
    protected String getFileName() {
        return "cdb_manufacturers";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow(ManufacturerColumn.NAME.getColumnLabel(), ManufacturerColumn.ADDRESS.getColumnLabel(),
                ManufacturerColumn.PHONE.getColumnLabel(), ManufacturerColumn.EMAIL.getColumnLabel(),
                ManufacturerColumn.COUNTRY.getColumnLabel());
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<ManufacturerUI> exportData = Utility.isNullOrEmpty(filteredManufacturers) ? selectedManufacturers
                : filteredManufacturers;
        for (final ManufacturerUI record : exportData) {
            exportTable.addDataRow(record.getName(), record.getAddress(), record.getPhoneNumber(), record.getEmail(),
                    record.getCountry());
        }
    }

    @Override
    protected String getExcelTemplatePath() {
        // Manufacturers do not have a template
        return null;
    }

    @Override
    protected int getExcelDataStartRow() {
        // Manufacturers do not have a template
        return 0;
    }
}
