/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import org.openepics.cable.model.NameStatus;
import org.openepics.names.jaxb.DeviceNameElement;

/**
 * Interface for providing custom implementation to get names.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public interface Names extends Serializable {

    /**
     * @return <code>true</code> if there was an error constructing a list of names, <code>false</code> otherwise
     */
    public boolean isError();

    /**
     * Refreshes the names, for example by clearing cache and reloading
     */
    public void refresh();

    /**
     * Returns a map of all names
     * 
     * @return a map of names
     */
    public Map<String, DeviceNameElement> getAllNames();

    /**
     * Returns a set of all active names
     * 
     * @return a set of names
     */
    public Set<String> getActiveNames();

    /**
     * Returns the status of the name.
     *
     * @param name
     *            the name
     *
     * @return status of the name
     */
    public NameStatus getNameStatus(String name);

    /**
     * Returns a name with the given uuid. If the name status is OBSOLETE, null is returned.
     *
     * @param uuid
     *            uuid of the name
     *
     * @return name of the device or null if not found or obsolete
     */
    public String getNameByUuid(String uuid);

    /**
     * Returns a uuid of the name.
     *
     * @param name
     *            device name
     *
     * @return UUID of the name or null if name not found
     */
    public String getUuidByName(String name);
}
