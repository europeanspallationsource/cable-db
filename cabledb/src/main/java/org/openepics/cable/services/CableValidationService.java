/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.google.common.base.Preconditions;

/**
 * This is a service that performs periodic validation of the cable database.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@DependsOn("DatabaseMigration")
@Lock(LockType.READ)
public class CableValidationService extends ConcurrentOperationServiceBase {

    private static final Logger LOGGER = Logger.getLogger(CableValidationService.class.getName());
    private static final String VALIDATION_USER = "CableValidationService";
    private static final int VALIDATION_BUNCH = 1000;
    // the time to wait between individual validations (60 min )
    private static final String VALIDATION_INTERVAL = "0";

    private static boolean startupValidation;

    @Resource
    private SessionContext ctx;
    @Inject
    private CableService cableService;

    /** Creates an interval timer that periodically invokes validations. */
    @PostConstruct // NOSONAR method used by framework
    private void init() {}

    /**
     * Performs the validation once. This should be invoked when application is started and dependent services are up.
     */
    public synchronized void validateAtStartup() {

        if (startupValidation) {
            return;
        }
        // no validation
        startupValidation = true;
    }

    /**
     * Performs periodic cable validation.
     *
     */
    @Schedule(minute = VALIDATION_INTERVAL, hour = "*", persistent = false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void validate() {
        runOperationNonBlocking(VALIDATION_USER);
    }

    @Override
    protected String getServiceName() {
        return "Cable Validation Service";
    }

    @Override
    protected String getOperation() {
        return "Validation";
    }

    /**
     * Validation of cables. Note <tt>@Lock(LockType.WRITE)</tt> for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 1);
        String userId = vargs[0];
        return validateCablesInBunches(userId);
    }

    /**
     * Validates the consistency of cable data and updates the data and/or status.
     *
     * @param userId
     *            id of the user invoking validation
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private boolean validateCablesInBunches(String userId) {
        int start = 0;
        // We do validation until we run out of cables to validate
        while (cableService.getAndValidateCables(start, VALIDATION_BUNCH, userId)) {
            start += VALIDATION_BUNCH;
            LOGGER.log(Level.INFO, "##Validation of a bunch completed.");
        }
        return true;
    }

}
