/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.Connector;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.GenericArtifact;
import org.openepics.cable.model.Query;
import org.openepics.cable.services.dl.ConnectorColumn;

/**
 * <code>ConnectorService</code> is the service layer that handles individual connector operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class ConnectorService {

    private static final Logger LOGGER = Logger.getLogger(ConnectorService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;

    /** @return a list of all connectors */
    public List<Connector> getConnectors() {
        return em.createQuery("SELECT c FROM Connector c", Connector.class).getResultList();
    }

    /**
     * @param customQuery
     *            custom query for filtering connectors
     * @param query
     *            query to update
     *
     * @return a list of filtered connectors
     */
    public List<Connector> getFilteredConnectors(String customQuery, Query query) {
        List<Connector> filteredConnectors = new ArrayList<Connector>();
        if (customQuery != null && !customQuery.isEmpty()) {
            filteredConnectors = em.createQuery(customQuery, Connector.class).getResultList();
        }
        return filteredConnectors;
    }

    /**
     * Returns all connectors for which at least one of the specified fields matches the given regular expression.
     *
     * @param fields
     *            the list of string fields to check
     * @param regExp
     *            regular expression containing only literals, wildcard (*) and single character (?)
     *
     * @return a list of matching connectors
     */
    public List<Connector> getFilteredConnectors(Collection<ConnectorColumn> fields, String regExp) {
        // convert regular expression to JPA SQL LIKE format
        if (regExp != null && !regExp.isEmpty()) {
            regExp = regExp.replace('*', '%').replace('?', '_');
        } else {
            regExp = "%";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT c FROM Connector c");

        boolean first = true;
        for (ConnectorColumn field : fields) {

            if (first) {
                stringBuilder.append(" WHERE");
                first = false;
            } else {
                stringBuilder.append(" OR");
            }

            stringBuilder.append(field.getFieldName());
            stringBuilder.append(" LIKE :regExp");
        }

        String query = stringBuilder.toString();

        List<Connector> filteredConnectors = new ArrayList<Connector>();
        filteredConnectors = em.createQuery(query, Connector.class).setParameter("regExp", regExp).getResultList();
        return filteredConnectors;
    }

    /**
     * Returns a Connector with the specified name.
     *
     * @param name
     *            name of the connector
     * @return the connector, or null if the connector with such name cannot be found
     */
    public Connector getConnector(String name) {
        try {
            return em.createQuery("SELECT c FROM Connector c WHERE c.name = :name", Connector.class)
                    .setParameter("name", name).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Connector with name: " + name + " not found", e);
            return null;
        }
    }

    /**
     * Creates connector.
     *
     * @param name
     *            the name of the connector
     * @param description
     *            description belonging to the connector
     * @param type
     *            the type of the connector
     * @param userId
     *            username of user creating the connector, for history record
     * @param dataSheet
     *            datasheet artifact
     *
     * @return a new Connector instance.
     */
    public Connector createConnector(String name, String description, String type,
            GenericArtifact dataSheet, String userId) {

        final Date created = new Date();
        final Date modified = created;
        final Connector connector = new Connector(name, description, type, created, modified, dataSheet);
        if (dataSheet != null) {
            em.persist(dataSheet);
        }
        em.persist(connector);
        historyService.createHistoryEntry(EntityTypeOperation.CREATE, connector.getName(), EntityType.CONNECTOR,
                connector.getId(), "", "", userId);

        return connector;
    }

    /**
     * Updates the attributes on the given connector.
     *
     * @param connector
     *            the connector with modified attributes to save to the database
     * @param oldConnector
     *            the connector before modification
     * @param userId
     *            username of user updating the connector, for history record
     */
    public void updateConnector(Connector connector, Connector oldConnector, String userId) {
        connector.setModified(new Date());
        if (connector.getDataSheet() != null) {
            if (connector.getDataSheet().getId() != null) {
                em.merge(connector.getDataSheet());
            } else {
                em.persist(connector.getDataSheet());
            }
        }
        em.merge(connector);
        historyService.createHistoryEntry(EntityTypeOperation.UPDATE, connector.getName(), EntityType.CONNECTOR,
                connector.getId(), getChangeString(connector, oldConnector), "", userId);
    }

    /**
     * Marks the connector as inactive in the database.
     *
     * @param connector
     *            the connector to delete
     * @return true if the connector was deleted, false if the connector was already deleted
     * @param userId
     *            username of user deleting the connector, for history record
     */
    public boolean deleteConnector(Connector connector, String userId) {
        em.merge(connector);

        if (!connector.isActive()) {
            return false;
        }

        connector.setActive(false);
        em.merge(connector);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE, connector.getName(), EntityType.CONNECTOR,
                connector.getId(), "", "", userId);

        return true;
    }

    /**
     * Generates and returns string with all changed connector attributes.
     *
     * @param connector
     *            new connector
     * @param oldConnector
     *            old connector
     *
     * @return string with all changed connector attributes.
     */
    private String getChangeString(Connector connector, Connector oldConnector) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes("Name", connector.getName(), oldConnector.getName()));
        sb.append(HistoryService.getDiffForAttributes("Description", connector.getDescription(),
                oldConnector.getDescription()));
        sb.append(HistoryService.getDiffForAttributes("Type", connector.getType(), oldConnector.getType()));
        return sb.toString();
    }
}
