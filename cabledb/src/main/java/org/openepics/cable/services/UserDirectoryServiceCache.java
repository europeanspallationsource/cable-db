/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.io.Serializable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import com.google.common.base.Preconditions;

/**
 * A session bean holding caching the data retrieved from {@link UserDirectoryService}. This class ensures that the
 * possibly slow read operations from underlying services are not performed more than once in a given session.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
public class UserDirectoryServiceCache extends ConcurrentOperationServiceBase implements Serializable {

    private static final long serialVersionUID = 1540890790002448065L;
    private static final Logger LOGGER = Logger.getLogger(UserDirectoryServiceCache.class.getName());
    // the time to wait before updating user directory service cache (20 min )
    private static final String UPDATE_INTERVAL = "*/20";

    private Set<String> allUsernames;
    private Set<String> allAdministratorUsernames;

    @Inject
    private transient UserDirectoryService userDirectoryService;

    @PostConstruct
    protected void initialise() {
        super.initialise();
    }

    /**
     * Update user directory service cache.
     *
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        runOperationNonBlocking();
    }

    @Override
    protected String getServiceName() {
        return "User Directory Service Cache";
    }

    @Override
    protected String getOperation() {
        return "Cache update";
    }

    /**
     * Helper method that invokes cache updating and returns true if updating is not already in progress, else false
     *
     * @return true once done
     */
    protected boolean updateCache() {
        LOGGER.log(Level.INFO, "Updating user directory service cache.");
        Set<String> newUsernames = userDirectoryService.getAllUsernames();
        Set<String> newAdministratorUsernames = userDirectoryService.getAllAdministratorUsernames();
        allUsernames = newUsernames;
        allAdministratorUsernames = newAdministratorUsernames;
        LOGGER.log(Level.INFO, "User directory service cache has been updated.");
        return true;
    }

    /**
     * Update cache (usernames, administrator user names). Note <tt>@Lock(LockType.WRITE)</tt>
     * for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 0);
        return updateCache();
    }

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set
     */
    public Set<String> getAllUsernames() {
        if (!initialized) {
            runOperationBlocking();
        }
        return allUsernames;
    }

    /**
     * Returns a set of names of all users that have administer permission of the cable database.
     *
     * @return the set
     */
    public Set<String> getAllAdministratorUsernames() {
        if (!initialized) {
            runOperationBlocking();
        }
        return allAdministratorUsernames;
    }

    /**
     * Retrieves the email address for the given user.
     *
     * @param username
     *            the user name
     * @return the user email
     */
    public String getEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getEmail(username);
    }

    /**
     * Retrieves the full name of the given user.
     *
     * @param username
     *            the user name
     * @return the user full name
     */
    public String getUserFullName(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullName(username);
    }

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username
     *            the user name
     * @return the user full name and email
     */
    public String getUserFullNameAndEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullNameAndEmail(username);
    }
}
