/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableName;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.Routing;
import org.openepics.cable.model.RoutingRow;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.RoutingService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceCache;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.Utility;

/**
 * <code>CableLoader</code> is {@link DataLoader} that creates and persists cables from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Stateless
public class CableLoader extends DataLoader<Cable> {

    private static final Logger LOGGER = Logger.getLogger(CableLoader.class.getName());

    private static final String INVALID = "Invalid ";
    private static final String WHITESPACES_COMMA_WHITESPACES = "\\s*,\\s*";

    @Inject
    private CableService cableService;
    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private ConnectorService connectorService;
    @Inject
    private RoutingService routingService;
    @Inject
    private Names names;
    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;

    @Inject
    private SessionService sessionService;

    private Set<String> validEndpointNames;
    private Set<String> validUsernames;
    private Map<String, CableType> cableTypes = new HashMap<>();
    private Map<String, Connector> connectors = new HashMap<>();
    private Map<String, Routing> routings = new HashMap<>();
    private Map<String, Cable> originalCables = new HashMap<>();
    private Map<String, Cable> cables = new HashMap<>();

    private List<Cable> cablesToCreate;
    private List<Cable> cablesToDelete;
    private List<Cable> cablesToUpdate;
    private List<Cable> oldCablesToUpdate;

    // used for measuring time performance
    private long time;
    private long totalTime;

    /**
     * Default constructor.
     */
    public CableLoader() {
        super();
    }

    @Override
    public LoaderResult<Cable> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();
        validEndpointNames = names.getActiveNames();
        validUsernames = userDirectoryServiceCache.getAllUsernames();
        cableTypes.clear();
        for (CableType cableType : cableTypeService.getAllCableTypes()) {
            cableTypes.put(cableType.getName(), cableType);
        }
        connectors.clear();
        for (Connector connector : connectorService.getConnectors()) {
            connectors.put(connector.getName(), connector);
        }
        routings.clear();
        for (Routing routing : routingService.getActiveRoutings()) {
            routings.put(routing.getName(), routing);
        }

        LoaderResult<Cable> result = super.load(stream, test);

        totalTime += getPassedTime();

        LOGGER.info("Import time: " + totalTime + " ms");
        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {

        try {
            switch (record.getCommand()) {
            case CREATE:
                createCable(record);
                break;
            case UPDATE:
                updateCable(record);
                break;
            case DELETE:
                deleteCable(record);
                break;
            default:
                // do nothing
                break;
            }
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            report.addMessage(new ValidationMessage(e.getMessage(), true));
            stopLoad = true;
            return;
        }
    }

    @Override
    public void updateRecordList(Iterable<DSRecord> records) {
        List<String> cableNames = new ArrayList<>();
        try {
            for (DSRecord record : records) {

                if (DSCommand.UPDATE.equals(record.getCommand()) || DSCommand.DELETE.equals(record.getCommand())) {
                    String name = null;
                    name = Utility.formatWhitespaces(getMandatoryField(record, CableColumn.NAME).toString());
                    cableNames.add(name);
                }

            }
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            report.addMessage(new ValidationMessage(e.getMessage(), true));
            stopLoad = true;
            return;
        }

        List<Cable> oldCableList = cableService.getCablesByName(cableNames, true, true);
        originalCables.clear();
        for (Cable cable : oldCableList) {
            originalCables.put(cable.getName(), cable);
        }
        List<Cable> cableList = cableService.getCablesByName(cableNames, false, true);
        cables.clear();
        for (Cable cable : cableList) {
            cables.put(cable.getName(), cable);
        }

        cablesToCreate = new ArrayList<Cable>();
        cablesToUpdate = new ArrayList<Cable>();
        oldCablesToUpdate = new ArrayList<Cable>();
        cablesToDelete = new ArrayList<Cable>();

        totalTime +=

                getPassedTime();

        super.updateRecordList(records);

        List<Cable> newCables = cableService.createCables(cablesToCreate, sessionService.getLoggedInName(), false);
        cableService.updateCables(cablesToUpdate, oldCablesToUpdate, sessionService.getLoggedInName(), false);
        cableService.deleteCables(cablesToDelete, sessionService.getLoggedInName());

        for (

        Cable cable : newCables) {
            report.addAffected(cable);
        }
    }

    /** Processes the record for cable creation. */
    private void createCable(DSRecord record) {

        final List<String> owners = Utility.splitStringIntoList(
                Utility.formatWhitespaces(getMandatoryField(record, CableColumn.OWNERS).toString()));
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage(
                    "You do not have permission to add cables you don't own" + " " + getUserAndOwnersInfo(owners) + ".",
                    record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        checkCableAttributeValidityForCreate(record);

        if (report.isError())
            return;

        final String system = CableNumbering
                .getSystemNumber(Utility.formatWhitespaces(getMandatoryField(record, CableColumn.SYSTEM).toString()));
        final String subsystem = CableNumbering.getSubsystemNumber(
                Utility.formatWhitespaces(getMandatoryField(record, CableColumn.SUBSYSTEM).toString()));
        final String cableClass = CableNumbering.getCableClassLetter(
                Utility.formatWhitespaces(getMandatoryField(record, CableColumn.CLASS).toString()));

        final String cableTypeString = Utility
                .formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.CABLE_TYPE)));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(Utility.formatWhitespaces(cableTypeString));
        }
        final String connectorAString = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.DEVICE_A_CONNECTOR, null)));
        final String connectorBString = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.DEVICE_B_CONNECTOR, null)));
        Connector connectorA = null;
        Connector connectorB = null;
        if (connectorAString != null && !connectorAString.isEmpty()) {
            connectorA = connectors.get(connectorAString);
        }
        if (connectorBString != null && !connectorBString.isEmpty()) {
            connectorB = connectors.get(Utility.formatWhitespaces(connectorAString));
        }
        final String container = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.CONTAINER, null)));

        final Endpoint endpointA = new Endpoint(
                Utility.formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.DEVICE_A_NAME))),
                Utility.formatWhitespaces(
                        objectToString(getOptionalField(record, CableColumn.DEVICE_A_BUILDING, null))),
                Utility.formatWhitespaces(objectToString(getOptionalField(record, CableColumn.DEVICE_A_RACK, null))),
                connectorA, null, Utility.formatWhitespaces(
                        objectToString(getOptionalField(record, CableColumn.DEVICE_A_USER_LABEL, null))));

        final Endpoint endpointB = new Endpoint(
                Utility.formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.DEVICE_B_NAME))),
                Utility.formatWhitespaces(
                        objectToString(getOptionalField(record, CableColumn.DEVICE_B_BUILDING, null))),
                Utility.formatWhitespaces(objectToString(getOptionalField(record, CableColumn.DEVICE_B_RACK, null))),
                connectorB, null, Utility.formatWhitespaces(
                        objectToString(getOptionalField(record, CableColumn.DEVICE_B_USER_LABEL, null))));

        final Date installationBy = toDate(getOptionalField(record, CableColumn.INSTALLATION_DATE, null));
        final Date terminationBy = toDate(getOptionalField(record, CableColumn.TERMINATION_DATE, null));
        final CableAutoCalculatedLength autoCalculatedLength = CableAutoCalculatedLength
                .convertToCableAutoCalculatedLength(
                        objectToString(getOptionalField(record, CableColumn.AUTOCALCULATEDLENGTH, "NO")));
        Float baseLength = toFloat(getOptionalField(record, CableColumn.BASELENGTH, 0));
        Float length = toFloat(getOptionalField(record, CableColumn.LENGTH, 0));
        final String comments = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.COMMENTS, null)));

        List<RoutingRow> routingRows = new ArrayList<>();
        String routingsString = objectToString(getOptionalField(record, CableColumn.ROUTINGS, null));
        if (!isNullOrEmpty(routingsString)) {
            final List<String> routingList = Arrays.asList(routingsString.split(WHITESPACES_COMMA_WHITESPACES));
            routingRows = new ArrayList<RoutingRow>();
            for (int i = 0; i < routingList.size(); i++) {
                routingRows.add(new RoutingRow(routings.get(routingList.get(i)), null, i));

            }
        }

        if (autoCalculatedLength == CableAutoCalculatedLength.YES) {
            float calculatedLength = baseLength;
            for (RoutingRow routingRow : routingRows) {
                calculatedLength += routingRow.getRouting().getLength();
            }
            if (length != null && !length.equals(calculatedLength)) {
                report.addMessage(new ValidationMessage("Entered Length will be overriden by auto calculated length",
                        false, record.getRowLabel(), CableColumn.LENGTH.getColumnLabel()));
            }
            length = calculatedLength;
        }

        CableStatus newStatus = CableStatus.INSERTED;
        final String status = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.STATUS, null)));
        if (!isNullOrEmpty(status)) {
            newStatus = CableStatus.convertToCableStatus(status);
            if (newStatus != CableStatus.INSERTED && !sessionService.canAdminister()) {
                report.addMessage(new ValidationMessage(
                        "New cable STATUS can be set to other than INSERTED only by administrator.", true,
                        record.getRowLabel(), null));
            }
        }

        // revision is considered volatile column/field
        //     volatility handled by getOptionalField
        //     @see CableColumn#REVISION
        //     @see CableLoader#getOptionalField
        final String revision =
                Utility.formatWhitespaces(objectToString(getOptionalField(record, CableColumn.REVISION, null)));

        if (!test) {
            Cable cable = new Cable(system, subsystem, cableClass, null, owners, newStatus, cableType, container,
                    endpointA, endpointB, new Date(), new Date(), routingRows, installationBy, terminationBy,
                        null, autoCalculatedLength, length, comments, revision);
            cable.setBaseLength(baseLength);
            cablesToCreate.add(cable);
            createRows++;
        }

        report.addMessage(new ValidationMessage("Adding new cable.", false, record.getRowLabel(), null));

    }

    /** @return true if the current user has permission to modify cable with a all of the given owners, else false */
    private boolean hasCablePermission(List<String> owners) {
        return sessionService.canAdminister()
                || (sessionService.canManageOwnedCables() && owners.contains(sessionService.getLoggedInName()));
    }

    /** Processes the record for cable update. */
    private void updateCable(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
                                                // validation

        if (!checkCableName(record))
            return;

        final String cableName = Utility.formatWhitespaces(getMandatoryField(record, CableColumn.NAME).toString());
        final Cable cable = cables.get(cableName);
        final Cable oldCable = originalCables.get(cableName);

        checkCableAttributeValidityForUpdate(record, cable);

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(
                    new ValidationMessage(getCableNameInfo(cableName) + " cannot be updated as it is already deleted.",
                            true, record.getRowLabel(), null, cableName));
            return;
        }

        final List<String> owners = cable.getOwners();
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage("You do not have permission to update cables you don't own" + " "
                    + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        final String system = CableNumbering.getSystemNumber(Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.SYSTEM, cable.getSystem()))));
        final String subsystem = CableNumbering.getSubsystemNumber(Utility.formatWhitespaces(
                objectToString(getOptionalField(record, CableColumn.SUBSYSTEM, cable.getSubsystem()))));
        final String cableClass = CableNumbering.getCableClassLetter(Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.CLASS, cable.getCableClass()))));

        boolean cableNameChange = false;
        if (!cable.getSystem().equals(system) || !cable.getSubsystem().equals(subsystem)
                || !cable.getCableClass().equals(cableClass)) {
            cableNameChange = true;
        }

        if (report.isError())
            return;

        final Integer seqNumber = cable.getSeqNumber();

        final String cableTypeString = Utility.formatWhitespaces(
                objectToString(getOptionalField(record, CableColumn.CABLE_TYPE, cable.getCableType())));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(cableTypeString);
        }
        final String connectorAString = Utility.formatWhitespaces(objectToString(
                getOptionalField(record, CableColumn.DEVICE_A_CONNECTOR, cable.getEndpointA().getConnector())));
        final String connectorBString = Utility.formatWhitespaces(objectToString(
                getOptionalField(record, CableColumn.DEVICE_B_CONNECTOR, cable.getEndpointB().getConnector())));
        Connector connectorA = null;
        Connector connectorB = null;
        if (connectorAString != null && !connectorAString.isEmpty()) {
            connectorA = connectors.get(connectorAString);
        }
        if (connectorBString != null && !connectorBString.isEmpty()) {
            connectorB = connectors.get(connectorBString);
        }
        final String container = Utility.formatWhitespaces(
                objectToString(getOptionalField(record, CableColumn.CONTAINER, cable.getContainer())));
        final List<String> newOwners = Utility.splitStringIntoList(Utility.formatWhitespaces(
                objectToString(getOptionalField(record, CableColumn.OWNERS, cable.getOwnersString()))));

        List<RoutingRow> routingRows = new ArrayList<RoutingRow>();
        String routingsString = objectToString(getOptionalField(record, CableColumn.ROUTINGS, null));
        if (!isNullOrEmpty(routingsString)) {
            List<String> routingList = Arrays.asList(routingsString.split(WHITESPACES_COMMA_WHITESPACES));
            for (int i = 0; i < routingList.size(); i++) {
                routingRows.add(new RoutingRow(routings.get(routingList.get(i)), cable, i));
            }
        }
        if (routingRows.isEmpty()) {
            routingRows = cable.getRoutingRows();
        }
        final CableAutoCalculatedLength autoCalculatedLength = CableAutoCalculatedLength
                .convertToCableAutoCalculatedLength(objectToString(
                        getOptionalField(record, CableColumn.AUTOCALCULATEDLENGTH, cable.getAutoCalculatedLength())));
        Float baseLength = toFloat(getOptionalField(record, CableColumn.BASELENGTH, cable.getBaseLength()));
        Float length = toFloat(getOptionalField(record, CableColumn.LENGTH, cable.getLength()));
        final String comments = Utility.formatWhitespaces(
                objectToString(getOptionalField(record, CableColumn.COMMENTS, cable.getComments())));

        if (autoCalculatedLength == CableAutoCalculatedLength.YES) {
            float calculatedLength = baseLength;
            for (RoutingRow routingRow : routingRows) {
                calculatedLength += routingRow.getRouting().getLength();
            }
            if (length != null && !length.equals(calculatedLength)) {
                report.addMessage(new ValidationMessage("Entered Length will be overriden by auto calculated length",
                        false, record.getRowLabel(), CableColumn.LENGTH.getColumnLabel()));
            }
            length = calculatedLength;
        }

        CableStatus status = CableStatus
                .convertToCableStatus(objectToString(getOptionalField(record, CableColumn.STATUS, cable.getStatus())));

        if (status != cable.getStatus()) {
            if (status == CableStatus.INSERTED) {
                report.addMessage(getErrorMessage("Cable status cannot be changed back to INSERTED.", record,
                        CableColumn.STATUS));
            } else if (status == CableStatus.DELETED) {
                report.addMessage(
                        getErrorMessage("Cable status cannot be changed to DELETED.", record, CableColumn.STATUS));
            } else if (cable.getStatus() == CableStatus.INSERTED) {
                if (!sessionService.canAdminister())
                    report.addMessage(getErrorMessage("Only admin can change INSERTED cable status.", record,
                            CableColumn.STATUS));
            } else if (cable.getStatus() == CableStatus.ROUTED) {
                if (status != CableStatus.APPROVED) {
                    report.addMessage(getErrorMessage("This cable status can only be changed to APPROVED.", record,
                            CableColumn.STATUS));
                } else if (!(cable.getOwners().contains(sessionService.getLoggedInName())
                        || sessionService.canAdminister())) {
                    report.addMessage(getErrorMessage("This cable status can only be changed by owner or admin.",
                            record, CableColumn.STATUS));
                }
            } else if (cable.getStatus() == CableStatus.APPROVED
                    && (!sessionService.canAdminister() || status != CableStatus.ROUTED)) {
                report.addMessage(getErrorMessage("This cable status can only be changed to ROUTED by admin.", record,
                        CableColumn.STATUS));
            } else if (status == null) {
                status = cable.getStatus();
                report.addMessage(new ValidationMessage("Cable status not recognized. Will remain unchanged", false,
                        record.getRowLabel(), CableColumn.STATUS.getColumnLabel()));
            }
        }

        // revision is considered volatile column/field
        //     volatility handled by getOptionalField
        //     @see CableColumn#REVISION
        //     @see CableLoader#getOptionalField
        final String revision =
                Utility.formatWhitespaces(
                        objectToString(getOptionalField(record, CableColumn.REVISION, cable.getRevision())));

        if (report.isError())
            return;

        cable.setSystem(system);
        cable.setSubsystem(subsystem);
        cable.setCableClass(cableClass);
        cable.setSeqNumber(seqNumber);
        cable.setOwners(newOwners);
        cable.setStatus(status);
        cable.setCableType(cableType);
        cable.setContainer(container);
        cable.setRoutingRows(routingRows);
        cable.setInstallationBy(
                toDate(getOptionalField(record, CableColumn.INSTALLATION_DATE, cable.getInstallationBy())));
        cable.setTerminationBy(
                toDate(getOptionalField(record, CableColumn.TERMINATION_DATE, cable.getTerminationBy())));
        cable.setAutoCalculatedLength(autoCalculatedLength);
        cable.setLength(length);
        cable.setBaseLength(baseLength);
        cable.setComments(comments);
        cable.setRevision(revision);

        cable.getEndpointA().update(
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_A_NAME, cable.getEndpointA().getDevice()))),
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_A_BUILDING, cable.getEndpointA().getBuilding()))),
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_A_RACK, cable.getEndpointA().getRack()))),
                connectorA, null, Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_A_USER_LABEL, cable.getEndpointA().getLabel()))));
        cable.getEndpointB().update(
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_B_NAME, cable.getEndpointB().getDevice()))),
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_B_BUILDING, cable.getEndpointB().getBuilding()))),
                Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_B_RACK, cable.getEndpointB().getRack()))),
                connectorB, null, Utility.formatWhitespaces(objectToString(
                        getOptionalField(record, CableColumn.DEVICE_B_USER_LABEL, cable.getEndpointB().getLabel()))));

        if (!test) {
            cablesToUpdate.add(cable);
            oldCablesToUpdate.add(oldCable);
            updateRows++;
            report.addAffected(cable);
        }
        if (!cableNameChange) {
            report.addMessage(
                    new ValidationMessage("Updating cable '" + cableName + "'.", false, record.getRowLabel(), null));
        } else {
            report.addMessage(new ValidationMessage(
                    "Warning: updating cable '" + cableName + "' will change its current cable name.", false,
                    record.getRowLabel(), null));
        }

    }

    /** Processes the record for cable deletion. */
    private void deleteCable(DSRecord record) {

        if (!checkCableName(record))
            return;

        final String cableName = Utility.formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.NAME)));
        final Cable cable = originalCables.get(cableName);

        final List<String> owners = new ArrayList<String>(cable.getOwners());
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage("You do not have permission to delete cables you don't own" + " "
                    + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(new ValidationMessage(getCableNameInfo(cableName) + " is already deleted.", true,
                    record.getRowLabel(), cableName));
        }

        if (report.isError())
            return;

        if (!test) {
            cablesToDelete.add(cable);
            deleteRows++;
            report.addAffected(cable);
        }
        report.addMessage(new ValidationMessage("Deleting cable with number " + cableName + ".", false,
                record.getRowLabel(), null));
    }

    /** Check that the cable attributes are valid. */
    private void checkCableAttributeValidityForCreate(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
        final String systemLabel = objectToString(getMandatoryField(record, CableColumn.SYSTEM));
        final String subsystemLabel = objectToString(getMandatoryField(record, CableColumn.SUBSYSTEM));
        final String cableClassLabel = objectToString(getMandatoryField(record, CableColumn.CLASS));
        final String cableType = objectToString(getMandatoryField(record, CableColumn.CABLE_TYPE));
        final List<String> owners = Utility.splitStringIntoList(
                Utility.formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.OWNERS))));
        final String status = Utility.formatWhitespaces(objectToString(getMandatoryField(record, CableColumn.STATUS)));
        final String endpointADeviceName = objectToString(getMandatoryField(record, CableColumn.DEVICE_A_NAME));
        final String endpointBDeviceName = objectToString(getMandatoryField(record, CableColumn.DEVICE_B_NAME));

        checkCableAttributeValidity(record, systemLabel, subsystemLabel, cableClassLabel, cableType, owners, status,
                endpointADeviceName, endpointBDeviceName);
    }

    /** Check that the cable attributes are valid. */
    private void checkCableAttributeValidityForUpdate(DSRecord record, Cable cable) { // NOSONAR Cyclomatic complexity
                                                                                      // due to serial
        final String systemLabel = objectToString(getOptionalField(record, CableColumn.SYSTEM, null));
        final String subsystemLabel = objectToString(getOptionalField(record, CableColumn.SUBSYSTEM, null));
        final String cableClassLabel = objectToString(getOptionalField(record, CableColumn.CLASS, null));
        final String cableType = objectToString(getOptionalField(record, CableColumn.CABLE_TYPE, null));
        final List<String> owners = Utility.splitStringIntoList(
                Utility.formatWhitespaces(objectToString(getOptionalField(record, CableColumn.OWNERS, null))));
        final String status = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.STATUS, null)));
        final String endpointADeviceName = objectToString(getOptionalField(record, CableColumn.DEVICE_A_NAME, null));
        final String endpointBDeviceName = objectToString(getOptionalField(record, CableColumn.DEVICE_B_NAME, null));

        checkCableAttributeValidity(record, systemLabel, subsystemLabel, cableClassLabel, cableType, owners, status,
                endpointADeviceName, endpointBDeviceName);
    }

    /** Check that the cable attributes are valid. */
    private void checkCableAttributeValidity(DSRecord record, String systemLabel, String subsystemLabel,
            String cableClassLabel, String cableType, List<String> owners, String status, String endpointADeviceName,
            String endpointBDeviceName) { // NOSONAR
                                          // Cyclomatic
                                          // complexity due
                                          // to serial

        final String connectorAname = objectToString(getOptionalField(record, CableColumn.DEVICE_A_CONNECTOR, null));
        final String connectorBname = objectToString(getOptionalField(record, CableColumn.DEVICE_B_CONNECTOR, null));
        final String endpointALabel = objectToString(getOptionalField(record, CableColumn.DEVICE_A_USER_LABEL, null));
        final String endpointBLabel = objectToString(getOptionalField(record, CableColumn.DEVICE_B_USER_LABEL, null));
        final Object installationBy = getOptionalField(record, CableColumn.INSTALLATION_DATE, null);
        final Object terminationBy = getOptionalField(record, CableColumn.TERMINATION_DATE, null);
        final Object baseLength = getOptionalField(record, CableColumn.BASELENGTH, null);
        final Object length = getOptionalField(record, CableColumn.LENGTH, null);
        final String routingsString = objectToString(getOptionalField(record, CableColumn.ROUTINGS, null));
        final String autoCalculatedString = objectToString(
                getOptionalField(record, CableColumn.AUTOCALCULATEDLENGTH, null));
        final String comments = objectToString(getOptionalField(record, CableColumn.COMMENTS, null));

        // revision is considered volatile column/field
        //     volatility handled by getOptionalField
        //     @see CableColumn#REVISION
        //     @see CableLoader#getOptionalField
        final String revision = objectToString(getOptionalField(record, CableColumn.REVISION, null));

        // validation
        if (systemLabel != null) {
            try {
                final String system = CableNumbering.getSystemNumber(systemLabel);
                if (!CableName.isValidSystem(system)) {
                    report.addMessage(getErrorMessage("SYSTEM field is not in the valid format.", record,
                            CableColumn.SYSTEM, system));
                } else if (!CableNumbering.isValidSystem(systemLabel)) {
                    report.addMessage(getErrorMessage("SYSTEM field is not valid system.", record, CableColumn.SYSTEM,
                            systemLabel));
                }
                validateStringSize(system, CableColumn.SYSTEM);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.SYSTEM, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SYSTEM, systemLabel));
            }
        }
        if (subsystemLabel != null) {
            try {
                final String subsystem = CableNumbering.getSubsystemNumber(subsystemLabel);
                if (!CableName.isValidSubsystem(subsystem)) {
                    report.addMessage(getErrorMessage("SUBSYSTEM field is not in the valid format.", record,
                            CableColumn.SUBSYSTEM, subsystem));
                } else if (!CableNumbering.isValidSubsystem(systemLabel, subsystemLabel)) {
                    report.addMessage(getErrorMessage("SUBSYSTEM field is not valid subsystem.", record,
                            CableColumn.SUBSYSTEM, subsystemLabel));
                }
                validateStringSize(subsystem, CableColumn.SUBSYSTEM);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.SUBSYSTEM, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SUBSYSTEM, subsystemLabel));
            }
        }

        if (cableClassLabel != null) {
            final String cableClass = CableNumbering.getCableClassLetter(cableClassLabel);
            try {
                if (!CableName.isValidCableClass(cableClass)) {
                    report.addMessage(getErrorMessage("CABLE CLASS is not in the valid format.", record,
                            CableColumn.CLASS, cableClass));
                } else if (!CableNumbering.isValidCableClass(cableClassLabel)) {
                    report.addMessage(getErrorMessage("CABLE CLASS is not valid cable class.", record,
                            CableColumn.CLASS, cableClassLabel));
                }
                validateStringSize(cableClass, CableColumn.CLASS);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.CLASS, e);
                report.addMessage(
                        getErrorMessage(e.getMessage(), record, CableColumn.CLASS, objectToString(cableClass)));
            }
        }

        if (owners != null) {
            try {
                if (isNullOrEmpty(owners)) {
                    report.addMessage(getErrorMessage("Cable OWNERS are not set.", record, CableColumn.OWNERS));
                } else {
                    if (!validOwners(owners)) {
                        report.addMessage(getErrorMessage("Cable OWNERS are not present in the user directory.", record,
                                CableColumn.OWNERS, String.join(" , ", owners)));
                    }
                }
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.OWNERS, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.OWNERS, objectToString(owners)));
            }
        }

        if (status != null) {
            if (!isNullOrEmpty(status)) {
                if (CableStatus.convertToCableStatus(status) == null) {
                    report.addMessage(getErrorMessage("Cable STATUS is invalid.", record, CableColumn.STATUS, status));
                }
            } else {
                report.addMessage(getErrorMessage("Cable STATUS is not set.", record, CableColumn.STATUS));
            }
        }

        if (endpointADeviceName != null) {
            try {
                if (isNullOrEmpty(endpointADeviceName)) {
                    report.addMessage(getErrorMessage("ENDPOINT A DEVICE NAME is not specified.", record,
                            CableColumn.DEVICE_A_NAME));
                } else {
                    if (!validEndpointNames.contains(endpointADeviceName)) {
                        report.addMessage(
                                getErrorMessage("ENDPOINT A DEVICE NAME is not registered in the Naming System.",
                                        record, CableColumn.DEVICE_A_NAME, endpointADeviceName));
                    }
                    validateEndpointStringSize(endpointADeviceName, CableColumn.DEVICE_A_NAME);
                }
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.DEVICE_A_NAME, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_A_NAME,
                        objectToString(endpointADeviceName)));
            }
        }

        if (endpointBDeviceName != null) {
            try {
                if (isNullOrEmpty(endpointBDeviceName)) {
                    report.addMessage(getErrorMessage("ENDPOINT B DEVICE NAME is not specified.", record,
                            CableColumn.DEVICE_B_NAME));
                } else {
                    if (!validEndpointNames.contains(endpointBDeviceName)) {
                        report.addMessage(
                                getErrorMessage("ENDPOINT B DEVICE NAME is not registered in the Naming System.",
                                        record, CableColumn.DEVICE_B_NAME, endpointBDeviceName));
                    }
                    validateEndpointStringSize(endpointBDeviceName, CableColumn.DEVICE_B_NAME);
                }
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.DEVICE_B_NAME, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_B_NAME,
                        objectToString(endpointBDeviceName)));
            }
        }

        if (cableType != null
                && cableTypes.get(cableType) == null) {
            report.addMessage(getErrorMessage("Specified CABLE TYPE does not exit.", record, CableColumn.CABLE_TYPE,
                    cableType));
        }

        if (!isNullOrEmpty(connectorAname)
                && !connectors.containsKey(connectorAname)) {
            report.addMessage(getErrorMessage("CONNECTOR A is not registered present in the cable database.",
                    record, CableColumn.DEVICE_A_CONNECTOR, connectorAname));
        }

        if (connectorBname != null
                && !isNullOrEmpty(connectorBname) && DSCommand.CREATE.equals(record.getCommand())
                && !connectors.containsKey(connectorBname)) {
            report.addMessage(getErrorMessage("CONNECTOR B is not registered present in the cable database.",
                    record, CableColumn.DEVICE_B_CONNECTOR, connectorBname));
        }

        if (endpointALabel != null && endpointALabel.length() > Endpoint.MAX_LABEL_SIZE) {
            report.addMessage(
                    getErrorMessage("ENDPOINT A LABEL is over maximum allowed length " + Endpoint.MAX_LABEL_SIZE + ".",
                            record, CableColumn.DEVICE_A_USER_LABEL, endpointALabel));
        }

        if (endpointALabel != null && endpointBLabel.length() > Endpoint.MAX_LABEL_SIZE) {
            report.addMessage(
                    getErrorMessage("ENDPOINT B LABEL is over maximum allowed length " + Endpoint.MAX_LABEL_SIZE + ".",
                            record, CableColumn.DEVICE_B_USER_LABEL, endpointBLabel));
        }

        try {
            toDate(installationBy);
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid INSTALLATION_BY", e);
            report.addMessage(getErrorMessage("INSTALLATION BY field is not in the valid format.", record,
                    CableColumn.INSTALLATION_DATE,
                    objectToString(getOptionalField(record, CableColumn.INSTALLATION_DATE, null))));
        }
        try {
            toDate(terminationBy);
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid TERMINATION BY", e);
            report.addMessage(getErrorMessage("TERMINATION BY field is not in the valid format.", record,
                    CableColumn.TERMINATION_DATE,
                    objectToString(getOptionalField(record, CableColumn.TERMINATION_DATE, null))));
        }
        try {
            toFloat(baseLength);
        } catch (NumberFormatException e) {
            LOGGER.log(Level.FINEST, "Invalid BASELENGTH", e);
            report.addMessage(getErrorMessage("BASELENGTH field is not in the valid format.", record,
                    CableColumn.BASELENGTH, objectToString(getOptionalField(record, CableColumn.BASELENGTH, null))));
        }
        try {
            toFloat(length);
        } catch (NumberFormatException e) {
            LOGGER.log(Level.FINEST, "Invalid LENGTH", e);
            report.addMessage(getErrorMessage("LENGTH field is not in the valid format.", record, CableColumn.LENGTH,
                    objectToString(getOptionalField(record, CableColumn.LENGTH, null))));
        }

        if (!isNullOrEmpty(routingsString)) {
            final List<String> routingList = Arrays.asList(routingsString.split(WHITESPACES_COMMA_WHITESPACES));
            for (String routing : routingList) {
                if (routings.get(routing) == null) {
                    report.addMessage(getErrorMessage("ROUTING is not registered in the database.", record,
                            CableColumn.ROUTINGS, routing));
                }
            }

        }

        if (autoCalculatedString != null) {
            CableAutoCalculatedLength isAutoCalc = CableAutoCalculatedLength
                    .convertToCableAutoCalculatedLength(autoCalculatedString);
            if (isAutoCalc == null) {
                report.addMessage(getErrorMessage("AUTOCALCULATED field is not in the valid format.", record,
                        CableColumn.AUTOCALCULATEDLENGTH));

            } else if (isAutoCalc == CableAutoCalculatedLength.YES) {
                try {
                    if (toFloat(getOptionalField(record, CableColumn.BASELENGTH, null)) == null) {
                        // base length cannot be empty if autocalculated == YES
                        report.addMessage(
                                getErrorMessage("BASELENGTH field is not in the valid format based on AUTOCALCULATED.",
                                        record, CableColumn.BASELENGTH));
                    }
                } catch (NumberFormatException e) {
                    report.addMessage(getErrorMessage("BASELENGTH field is not in the valid format.", record,
                            CableColumn.BASELENGTH));
                }
            }
        }

        try {
            validateStringSize(objectToString(comments), CableColumn.COMMENTS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableColumn.COMMENTS, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableColumn.COMMENTS, objectToString(comments)));
        }

        try {
            validateStringSize(objectToString(revision), CableColumn.REVISION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableColumn.REVISION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableColumn.REVISION, objectToString(revision)));
        }
    }

    /**
     * Checks the validity of cable name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkCableName(DSRecord record) {
        final String cableName = Utility
                .formatWhitespaces(objectToString(getOptionalField(record, CableColumn.NAME, null)));
        if (!CableName.isValidName(cableName)) {
            report.addMessage(
                    getErrorMessage("Cable NAME is not in the valid format.", record, CableColumn.NAME, cableName));
            return false;
        }
        final Cable cable = originalCables.get(cableName);
        if (cable == null) {
            report.addMessage(getErrorMessage(getCableNameInfo(cableName) + " does not exist in the database.", record,
                    CableColumn.NAME, cableName));
            return false;
        }
        return true;
    }

    private String getUserAndOwnersInfo(List<String> owners) {
        return "(logged in user: " + sessionService.getLoggedInName() + ", owners: " + String.join(" , ", owners) + ")";
    }

    private String getCableNameInfo(String cableName) {
        return "Cable with name " + cableName;
    }

    private void validateStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Cable.class);
    }

    private void validateEndpointStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Endpoint.class);
    }

    /**
     * Check validity of owners
     *
     * @param owners
     *            usernames of the owners to check
     * @return true if all usernames of the owners are correct else false
     */
    private boolean validOwners(List<String> owners) {
        for (String user : owners) {
            if (!validUsernames.contains(user)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieves data from the mandatory field, or throws an exception if column could not be found.
     *
     * @param record
     *            the record containing data in interest
     * @param CableColumn
     *            column the column of the field we want to retrieve record to check.
     * @return value of the filed
     * @throws IllegalArgumentException
     *             if column could not be found
     */
    private Object getMandatoryField(DSRecord record, CableColumn column) throws IllegalArgumentException {
        try {
            return record.getField(column);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, "Column not found", e);
            throw new IllegalArgumentException("Could not find mandatory column '" + column + "'.", e);
        }
    }

    /**
     * Retrieves data from the optional field, or returns default value if column could not be found.
     *
     * @param record
     *            the record containing data in interest
     * @param CableColumn
     *            column the column of the field we want to retrieve record to check.
     * @param defaultValue
     *            the default value to return if column is not present
     * @return value of the filed or default value if column could not be found
     */
    private Object getOptionalField(DSRecord record, CableColumn column, Object defaultValue) {
        try {
            return record.getField(column);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, "Column not found, will use default value.", e);
            return defaultValue;
        }
    }
}
