/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the spreadsheet Routing template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum RoutingColumn implements FieldIntrospection {

    /*
     * REVISION is considered volatile column/field.
     */

    NAME("NAME", "name", "Name", true, false, true),
    DESCRIPTION("DESCRIPTION", "description", "Description", true, false, true),
    CLASSES("CLASSES", "cableClasses", "Classes", true, false, true),
    LOCATION("LOCATION", "location", "Location", true, false, true),
    LENGTH("LENGTH (m)", "length", "Length (m)", true, false, false),
    MODIFIED("MODIFIED", "modified", "Modified", false, false, false),
    OWNER("OWNER", "owner", "Owner", false, false, true),
    REVISION("REVISION", "revision", "Revision", true, true, true);

    /*
     * isExcelColumn
     *      presence as column/field in Excel sheet
     * isExcelVolatileColumn
     *      presence as column/field in Excel sheet but volatile, i.e. presence not guaranteed
     *      if not considered column in Excel, then value does not matter
     */

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isExcelVolatileColumn;
    private final boolean isStringComparisonOperator;

    private RoutingColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isExcelVolatileColumn, boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isExcelVolatileColumn = isExcelVolatileColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isExcelVolatileColumn() {
        return isExcelVolatileColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public static RoutingColumn convertColumnLabel(String columnLabel) {
        if (MODIFIED.getColumnLabel().equals(columnLabel)) {
            return MODIFIED;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (CLASSES.getColumnLabel().equals(columnLabel)) {
            return CLASSES;
        } else if (LOCATION.getColumnLabel().equals(columnLabel)) {
            return LOCATION;
        } else if (LENGTH.getColumnLabel().equals(columnLabel)) {
            return LENGTH;
        } else if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (OWNER.getColumnLabel().equals(columnLabel)) {
            return OWNER;
        } else if (REVISION.getColumnLabel().equals(columnLabel)) {
            return REVISION;
        }
        return null;
    }
}
