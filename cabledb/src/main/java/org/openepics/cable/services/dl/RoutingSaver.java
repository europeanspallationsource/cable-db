/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;

import org.openepics.cable.model.Routing;

/**
 * This saves {@link Routing} instances to Excel spreadsheet.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class RoutingSaver extends DataSaverExcel<Routing> {

    /**
     * Constructs an instance to save the given {@link Routing} objects.
     *
     * @param objects
     *            the objects to save
     */
    public RoutingSaver(Iterable<Routing> objects) {
        super(objects);
    }

    @Override
    protected void save(Routing routing) {
        setCommand(DSCommand.UPDATE);

        setValue(RoutingColumn.NAME, routing.getName());
        setValue(RoutingColumn.DESCRIPTION, routing.getDescription());
        setValue(RoutingColumn.CLASSES, routing.getCableClassesAsString());
        setValue(RoutingColumn.LOCATION, routing.getLocation());
        setValue(RoutingColumn.LENGTH, routing.getLength());

        // revision is considered volatile column/field
        //     @see RoutingColumn#REVISION
        //     column/field presence in template not guaranteed
        //     value not to be set
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_routings.xlsx");
    }
}
