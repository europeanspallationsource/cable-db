/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.CableTypeManufacturer;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.util.Utility;

/**
 * <code>CableTypeLoader</code> is {@link DataLoader} that creates and persists cable types from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableTypeLoader extends DataLoader<CableType> {

    private static final Logger LOGGER = Logger.getLogger(CableTypeLoader.class.getName());

    private static final String INVALID = "Invalid ";

    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private ManufacturerService manufacturerService;

    @Inject
    private SessionService sessionService;

    @Override
    public void updateRecord(DSRecord record) {

        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
        case CREATE:
            createCableType(record);
            break;
        case UPDATE:
            updateCableType(record);
            break;
        case DELETE:
            deleteCableType(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    /** Processes the record for cable type creation. */
    private void createCableType(DSRecord record) {

        checkCableTypeAttributeValidity(record);

        if (report.isError())
            return;

        final String name = Utility.formatWhitespaces(record.getField(CableTypeColumn.NAME).toString());

        if (cableTypeService.getCableType(name) != null) {
            report.addMessage(getErrorMessage("CableType with name " + name + " already exists in the database.",
                    record, CableTypeColumn.NAME, name));
            return;
        }

        final List<String> manufacturerNames = Utility.splitStringIntoList(
                Utility.formatWhitespaces(String.valueOf(record.getField(CableTypeColumn.MANUFACTURERS))));
        final InstallationType installationType = InstallationType
                .convertToInstallationType(record.getField(CableTypeColumn.INSTALLATION_TYPE).toString().toUpperCase());
        final String description = Utility.formatWhitespaces(record.getField(CableTypeColumn.DESCRIPTION).toString());
        final String service = Utility.formatWhitespaces(record.getField(CableTypeColumn.SERVICE).toString());
        final Integer voltage = toInteger(record.getField(CableTypeColumn.VOLTAGE_RATING));
        final String insulation = Utility.formatWhitespaces(record.getField(CableTypeColumn.INSULATION).toString());
        final String jacket = Utility.formatWhitespaces(record.getField(CableTypeColumn.JACKET).toString());
        final String flammability = Utility.formatWhitespaces(record.getField(CableTypeColumn.FLAMABILITY).toString());
        final Float tid = toFloat(record.getField(CableTypeColumn.RADIATION_RESISTANCE));
        final Float weight = toFloat(record.getField(CableTypeColumn.WEIGHT));
        final Float diameter = toFloat(record.getField(CableTypeColumn.DIAMETER));
        final List<CableTypeManufacturer> manufacturers = new ArrayList<>();
        for (String manufacturerName : manufacturerNames) {
            Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
            if (manufacturer != null) {
                manufacturers.add(new CableTypeManufacturer(null, manufacturer, null, manufacturers.size()));
            }
        }
        final String comments = Utility.formatWhitespaces(record.getField(CableTypeColumn.COMMENTS).toString());

        // revision is considered volatile column/field
        //     @see CableTypeColumn#REVISION
        String revision = null;
        try {
            revision = Utility.formatWhitespaces(record.getField(CableTypeColumn.REVISION).toString());
        } catch (IllegalArgumentException e) {
            revision = null;
        }

        if (!test) {
            final CableType cableType = cableTypeService.createCableType(name, installationType, description, service,
                    voltage, insulation, jacket, flammability, tid, weight, diameter, manufacturers, comments,
                    revision, sessionService.getLoggedInName());
            manufacturers.stream().forEach(m -> m.setCableType(cableType));
            createRows++;
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Adding new cable type '" + name + "'", false, record.getRowLabel(), null));

    }

    /** Processes the record for cable type update. */
    private void updateCableType(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial validation

        checkCableTypeAttributeValidity(record);

        if (report.isError())
            return;

        if (!checkCableTypeName(record))
            return;

        final CableType oldCableType = getCableTypeFromRecord(record);
        cableTypeService.detachCableType(oldCableType);
        final CableType cableType = getCableTypeFromRecord(record);
        final String name = cableType.getName();

        if (!cableType.isActive()) {
            report.addMessage(new ValidationMessage("CableType '" + name + "' cannot be updated as it is obsolete.",
                    true, record.getRowLabel(), null, name));
            return;
        }

        if (report.isError())
            return;

        List<String> manufacturerNames = new ArrayList<>(Utility.splitStringIntoList(
                Utility.formatWhitespaces(String.valueOf(record.getField(CableTypeColumn.MANUFACTURERS)))));
        final List<CableTypeManufacturer> existingManufacturers = cableType.getManufacturers();
        final List<CableTypeManufacturer> manufacturers = new ArrayList<>();
        for (String manufacturerName : manufacturerNames) {
            Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
            if (manufacturer != null) {
                manufacturers.add(new CableTypeManufacturer(cableType, manufacturer, null, manufacturers.size()));
            }
        }
        // Iterate through manufacturers on the cable type and preserve ones that are specified in the import file
        for (CableTypeManufacturer existingCableTypeManufacturer : existingManufacturers) {
            final String existingCableTypeManufacturerName = existingCableTypeManufacturer.getManufacturer().getName();
            // Iterate through manufacturers specified in the import file to check if current existing manufacturer is
            // amongst them. It yes swap the new manufacturer with the existing one, preserving the imported order.
            for (int i = 0; i < manufacturers.size(); i++) {
                if (existingCableTypeManufacturerName.equals(manufacturers.get(i).getManufacturer().getName())) {
                    manufacturers.set(i, existingCableTypeManufacturer);
                    break;
                }
            }
        }

        cableType.setDescription(Utility.formatWhitespaces(record.getField(CableTypeColumn.DESCRIPTION).toString()));
        cableType.setService(Utility.formatWhitespaces(record.getField(CableTypeColumn.SERVICE).toString()));
        cableType.setVoltage(toInteger(record.getField(CableTypeColumn.VOLTAGE_RATING)));
        cableType.setInsulation(Utility.formatWhitespaces(record.getField(CableTypeColumn.INSULATION).toString()));
        cableType.setJacket(Utility.formatWhitespaces(record.getField(CableTypeColumn.JACKET).toString()));
        cableType.setFlammability(Utility.formatWhitespaces(record.getField(CableTypeColumn.FLAMABILITY).toString()));
        cableType.setInstallationType(InstallationType.convertToInstallationType(Utility
                .formatWhitespaces(record.getField(CableTypeColumn.INSTALLATION_TYPE).toString().toUpperCase())));
        cableType.setTid(toFloat(record.getField(CableTypeColumn.RADIATION_RESISTANCE)));
        cableType.setWeight(toFloat(record.getField(CableTypeColumn.WEIGHT)));
        cableType.setDiameter(toFloat(record.getField(CableTypeColumn.DIAMETER)));
        cableType.setManufacturers(manufacturers);
        cableType.setComments(Utility.formatWhitespaces(record.getField(CableTypeColumn.COMMENTS).toString()));

        // revision is considered volatile column/field
        //     @see CableTypeColumn#REVISION
        String revision = null;
        try {
            revision = Utility.formatWhitespaces(record.getField(CableTypeColumn.REVISION).toString());
        } catch (IllegalArgumentException e) {
            revision = null;
        }
        if (revision != null) {
            cableType.setRevision(revision);
        }

        if (!test) {
            cableTypeService.updateCableType(cableType, oldCableType, sessionService.getLoggedInName());
            updateRows++;
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Updating cable type '" + name + "'.", false, record.getRowLabel(), null));
    }

    /** Processes the record for cable type deletion. */
    private void deleteCableType(DSRecord record) {

        if (!checkCableTypeName(record))
            return;

        final String name = Utility.formatWhitespaces(record.getField(CableTypeColumn.NAME).toString());
        if (isNullOrEmpty(name)) {
            report.addMessage(getErrorMessage("Cable type NAME/CODE not specified.", record, CableTypeColumn.NAME));
            return;
        }

        final CableType cableType = getCableTypeFromRecord(record);
        if (cableType == null) {
            report.addMessage(getErrorMessage("Cable type '" + name + "' does not exist in the database.", record,
                    CableTypeColumn.NAME, name));
            return;
        }

        if (!cableType.isActive()) {
            report.addMessage(new ValidationMessage("Cable '" + name + "' is already obsolete.", true,
                    record.getRowLabel(), null, name));
        }

        if (report.isError())
            return;

        if (!test) {
            cableTypeService.deleteCableType(cableType, sessionService.getLoggedInName());
            deleteRows++;
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Obsoleting cable type '" + name + "'.", false, record.getRowLabel(), null));
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {

        for (final CableTypeColumn column : CableTypeColumn.getColumns()) {
            if (column.isExcelVolatileColumn()) {
                // if volatile column/field, presence in Excel not guaranteed
                //     @see CableTypeColumn#REVISION
                continue;
            }

            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by using an old template or importing a wrong file.", true));
                break;
            }
        }
    }

    /** Check that the cable type attributes are valid. */
    private void checkCableTypeAttributeValidity(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
                                                                    // validation

        final String name = objectToString(record.getField(CableTypeColumn.NAME));
        try {
            if (isNullOrEmpty(name)) {
                report.addMessage(getErrorMessage("NAME/CODE is not specified.", record, CableTypeColumn.NAME));
            }
            validateStringSize(name, CableTypeColumn.NAME);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.NAME, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableTypeColumn.NAME, objectToString(name)));
        }

        final String installationType = objectToString(record.getField(CableTypeColumn.INSTALLATION_TYPE));
        if (isNullOrEmpty(installationType)) {
            report.addMessage(
                    getErrorMessage("INSTALLATION TYPE is not specified.", record, CableTypeColumn.INSTALLATION_TYPE));
        } else {
            if (InstallationType.convertToInstallationType(installationType.toUpperCase()) == null) {
                LOGGER.log(Level.FINEST, "Invalid INSTALLATION TYPE");
                report.addMessage(
                        getErrorMessage("INSTALLATION TYPE '" + installationType + "' is not of a known type.", record,
                                CableTypeColumn.INSTALLATION_TYPE, installationType));
            }
        }

        try {
            toInteger(record.getField(CableTypeColumn.VOLTAGE_RATING));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("VOLTAGE RATING must be a number.", record,
                    CableTypeColumn.VOLTAGE_RATING, objectToString(record.getField(CableTypeColumn.VOLTAGE_RATING))));
        }

        try {
            toFloat(record.getField(CableTypeColumn.RADIATION_RESISTANCE));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("RADIATION RESISTANCE must be a number.", record,
                    CableTypeColumn.RADIATION_RESISTANCE,
                    objectToString(record.getField(CableTypeColumn.RADIATION_RESISTANCE))));
        }

        try {
            toFloat(record.getField(CableTypeColumn.WEIGHT));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("WEIGHT must be a number.", record, CableTypeColumn.WEIGHT,
                    objectToString(record.getField(CableTypeColumn.WEIGHT))));
        }

        try {
            toFloat(record.getField(CableTypeColumn.DIAMETER));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("DIAMETER must be a number.", record, CableTypeColumn.DIAMETER,
                    objectToString(record.getField(CableTypeColumn.DIAMETER))));
        }

        final List<String> manufacturerNames = Utility.splitStringIntoList(
                Utility.formatWhitespaces(String.valueOf(record.getField(CableTypeColumn.MANUFACTURERS))));
        if (!isNullOrEmpty(manufacturerNames)) {
            for (String manufacturer : manufacturerNames) {
                if (!StringUtils.isEmpty(manufacturer) && manufacturerService.getManufacturer(manufacturer) == null) {
                    report.addMessage(getErrorMessage("Specified MANUFACTURER does not exit.", record,
                            CableTypeColumn.MANUFACTURERS, manufacturer));
                }
            }
            final HashSet<String> manufacturerNamesSet = new HashSet<String>(manufacturerNames);
            if (manufacturerNamesSet.size() != manufacturerNames.size()) {
                report.addMessage(getErrorMessage("Only one MANUFACTURER of each type can be added.", record,
                        CableTypeColumn.MANUFACTURERS));
            }
        }

        final Object description = record.getField(CableTypeColumn.DESCRIPTION);
        try {
            validateStringSize(objectToString(description), CableTypeColumn.DESCRIPTION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.DESCRIPTION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.DESCRIPTION, objectToString(description)));
        }

        final Object service = record.getField(CableTypeColumn.SERVICE);
        try {
            validateStringSize(objectToString(service), CableTypeColumn.SERVICE);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.SERVICE, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.SERVICE, objectToString(service)));
        }

        final Object insulation = record.getField(CableTypeColumn.INSULATION);
        try {
            validateStringSize(objectToString(insulation), CableTypeColumn.INSULATION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.INSULATION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.INSULATION, objectToString(insulation)));
        }

        final Object jacket = record.getField(CableTypeColumn.JACKET);
        try {
            validateStringSize(objectToString(jacket), CableTypeColumn.JACKET);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.JACKET, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableTypeColumn.JACKET, objectToString(jacket)));
        }

        final Object flamability = record.getField(CableTypeColumn.FLAMABILITY);
        try {
            validateStringSize(objectToString(flamability), CableTypeColumn.FLAMABILITY);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.FLAMABILITY, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.FLAMABILITY, objectToString(flamability)));
        }

        final Object comments = record.getField(CableTypeColumn.COMMENTS);
        try {
            validateStringSize(objectToString(comments), CableTypeColumn.COMMENTS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.COMMENTS, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.COMMENTS, objectToString(comments)));
        }

        // revision is considered volatile column/field
        //     @see CableTypeColumn#REVISION
        Object revision = null;
        try {
            revision = record.getField(CableTypeColumn.REVISION);
            validateStringSize(objectToString(revision), CableTypeColumn.REVISION);
        } catch (IllegalArgumentException e) {
            revision = null;
        } catch (DbFieldLengthViolationException e) {
            revision = null;
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.REVISION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.REVISION, objectToString(revision)));
        }
    }

    /**
     * Returns the cable type corresponding to the name of the given record.
     *
     * @param record
     *            the record
     * @return the corresponding cable type or null if none
     */
    private CableType getCableTypeFromRecord(DSRecord record) {
        return cableTypeService.getCableType(record.getField(CableTypeColumn.NAME).toString().trim());
    }

    private void validateStringSize(final String value, final CableTypeColumn column) {
        super.validateStringSize(value, column, CableType.class);
    }

    /**
     * Checks the validity of cableType name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkCableTypeName(DSRecord record) {
        final String name = Utility.formatWhitespaces(record.getField(CableTypeColumn.NAME).toString());
        if (isNullOrEmpty(name)) {
            report.addMessage(getErrorMessage("CableType name must be specified", record, CableTypeColumn.NAME));
            return false;
        }
        final CableType cableType = cableTypeService.getCableType(name);
        if (cableType == null) {
            report.addMessage(getErrorMessage("CableType with name " + name + " does not exist in the database.",
                    record, CableTypeColumn.NAME, name));
            return false;
        }
        return true;
    }

}
