/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.openepics.cable.services.dl.DSCommand;

/**
 * This is a base class for integration tests using arquillian framework. It contains common functionality used in
 * tests.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public abstract class TestBase {

    /** Initialization for testing expected exceptions that are wrapped. */
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Creates a list of CREATE DSCommands
     *
     * @param length
     *            the number of commands required
     * @return a list of length length
     */
    protected static List<DSCommand> getCreateCommands(int length) {
        List<DSCommand> commands = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            commands.add(DSCommand.CREATE);
        }
        return commands;
    }
}
