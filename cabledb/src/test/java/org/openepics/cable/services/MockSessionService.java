/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptor;

import org.openepics.cable.CableProperties;

import se.esss.ics.rbac.loginmodules.service.Message;

@RequestScoped
@Alternative
@Priority(Interceptor.Priority.APPLICATION + 20)
public class MockSessionService implements SessionService {

    private String username;
    private boolean loggedIn;
    private boolean admin;

    public MockSessionService() {
        username = null;
        loggedIn = false;
        admin = false;
    }

    @Override
    public Message login(String username, String password) {
        this.username = username;
        loggedIn = true;
        if (CableProperties.getInstance().getTestAdminUser().equals(username)) {
            admin = true;
        }
        return new Message("Sign in successful.", true);
    }

    @Override
    public Message logout() {
        username = null;
        loggedIn = false;
        admin = false;
        return new Message("Sign out successful.", true);
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public String getVisibleName() {
        return username;
    }

    @Override
    public String getLoggedInName() {
        return username;
    }

    @Override
    public boolean canAdminister() {
        return admin && loggedIn;
    }

    @Override
    public boolean canManageOwnedCables() {
        return loggedIn;
    }
}
