/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

/**
 * This is a static helper class that packages cable application into {@link WebArchive} appropriate for running
 * integration tests.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CablePackager {

    /** @return An archive packaging the whole cable application. */
    public static WebArchive createWebArchive() {
        File[] libraries = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
                .withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "cable.war").addAsLibraries(libraries)
                .addPackages(true, "org.openepics.cable.model", "org.openepics.cable.services",
                        "org.openepics.cable.services.dl", "org.openepics.cable.util")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("templates/cdb_cable_types.xlsx").addAsResource("templates/cdb_cables.xlsx")
                .addAsWebInfResource("jboss-all.xml").addAsWebInfResource("jboss-web.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        return war;
    }
}
