/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.StringUtil;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.services.dl.CableImportExportService;
import org.openepics.cable.services.dl.CableSaver;
import org.openepics.cable.services.dl.DSCommand;
import org.openepics.cable.services.dl.LoaderResult;

/**
 * Tests the {@link CableImportExportService} using arquillian framework integration test. This class tests the import
 * and export functionality for valid and invalid datasets.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableImportExportServiceIT extends CableImportExportServiceBase {

    @Inject
    private CableService cableService;
    @Inject
    private CableTypeService cableTypeService;

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    @Before
    public void setUp() {
        sessionService.login(CableProperties.getInstance().getTestAdminUser(),
                CableProperties.getInstance().getTestAdminPassword());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow adding an instance.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_addInstance() {
        final InputStream inputStream = convertToExcel(getCreateCommands(1),
                getExampleCable(CableProperties.getInstance().getTestCableUser()));
        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow adding an instance with all
     * optional data omitted.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_addInstanceNoOptionalData() {
        final InputStream inputStream = convertToExcel(getCreateCommands(1),
                getExampleCable(CableProperties.getInstance().getTestCableUser()));
        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow adding an instance with wiring
     * drawing URL with ending slash.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_addInstanceEndingSlash() {
        final CableSaver cableSaver = getCableSaver(getCreateCommands(1),
                getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "http://example.org/");
        final InputStream inputStream = cableSaver.save();
        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow adding an instance with wiring
     * drawing URL as Excel link containing whitespace.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_addInstanceWhitespace() {
        final CableSaver cableSaver = getCableSaver(getCreateCommands(1),
                getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "      http://example.org ");
        final InputStream inputStream = cableSaver.save();
        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with system
     * undefined.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noSystem() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.SYSTEM, "");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * subsystem undefined.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noSubsytem() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.SUBSYSTEM, "");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with class
     * undefined.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noClass() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.CLASS, "");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with owner
     * undefined.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noOwner() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.OWNERS, "");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * non-approved cable type.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_nonApprovedCableType() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.CABLE_TYPE, "nonexisting");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * non-existing endpoint device A.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noExistingEndpointADevice() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_NAME, "nonexisting");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * non-existing endpoint device B.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noExistingEndpointBDevice() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_B_NAME, "nonexisting");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * endpoint A with label exceeding the limit.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_endpointALabelExceedLimit() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_NAME, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * endpoint B with label exceeding the limit.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_endpointBLabelExceedLimit() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_B_NAME, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * installation by date in illegal format.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_illegalInstallation() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.INSTALLATION_DATE, "01. 01. 2015");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * termination by date in illegal format.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_illegalTermination() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.TERMINATION_DATE, "01. 01. 2015");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with
     * non-numeric length.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_nonnumericLength() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.LENGTH, "abc");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with a
     * malformed URL for wiring drawing A.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_malformedWiringDrawingA() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "http://exampleorg/");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with a
     * malformed URL for wiring drawing B.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_malformedWiringDrawingB() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "http://exampleorg/");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with wiring
     * drawing A not being an URL.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_wiringDrawingANotURL() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "Example");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow adding instance with wiring
     * drawing B not being an URL.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_wiringDrawingBNotURL() {
        final CableSaver cableSaver = getCableSaver(getExampleCable(CableProperties.getInstance().getTestCableUser()));
        cableSaver.setValue(0, CableColumn.DEVICE_B_WIRING, "Example");
        final InputStream inputStream = cableSaver.save();
        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow updating an instance.
     */
    @Test
    @UsingDataSet({ "cabletypes.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_updateInstance() throws MalformedURLException, ParseException {
        final String number = "12C000001";
        final String newNumber = "23D000002";
        final Cable cable = cableService.getCableByName(number);

        final String system = "2";
        final String subsystem = "3";
        final String cableClass = "D";
        final Integer seqNumber = 2;
        final CableType cableType2 = cableTypeService.getCableType("cabletype2");
        final String routing = "updated routing";
        final Date installationBy = DateUtil.parse("2017-03-01 10:00:00");
        final Date terminationBy = DateUtil.parse("2017-04-01 10:00:00");
        final Float length = 12.3f;
        final String deviceA = "LEBT-01:PBI-NPM-02";
        final String buildingA = "updated buildingA";
        final String rackA = "updated rackA";
        final String deviceB = "LEBT-01:Ctrl-Box-01";
        final String labelA = "updated labelA";
        final String buildingB = "updated buildingA";
        final String rackB = "updated rackB";
        final String labelB = "updated labelB";
        final String comments = "updated comments";

        cable.setSystem(subsystem);
        cable.setSubsystem(subsystem);
        cable.setCableClass(cableClass);
        cable.setSeqNumber(seqNumber);
        cable.setOwners(Arrays.asList("rbactester1"));
        cable.setStatus(CableStatus.APPROVED);
        cable.setCableType(cableType2);
        cable.setInstallationBy(installationBy);
        cable.setTerminationBy(terminationBy);
        cable.setAutoCalculatedLength(CableAutoCalculatedLength.NO);
        cable.setLength(length);
        cable.setComments(comments);
        cable.getEndpointA().update(deviceA, buildingA, rackA, null, null, labelA);
        cable.getEndpointB().update(deviceB, buildingB, rackB, null, null, labelB);
        final InputStream inputStream = convertToExcel(cable);

        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());

        final Cable updatedCable = cableService.getCableByName(newNumber);

        assertEquals(system, updatedCable.getSystem());
        assertEquals(subsystem, updatedCable.getSubsystem());
        assertEquals(cableClass, updatedCable.getCableClass());
        assertEquals(seqNumber, updatedCable.getSeqNumber());
        assertEquals(cableType2, updatedCable.getCableType());
        assertEquals(routing, updatedCable.getRoutingRows());
        assertEquals(installationBy, updatedCable.getInstallationBy());
        assertEquals(terminationBy, updatedCable.getTerminationBy());
        assertEquals(length, updatedCable.getLength());
        assertEquals(comments, updatedCable.getComments());

        final Endpoint endpointA = cable.getEndpointA();
        assertEquals(deviceA, endpointA.getDevice());
        assertEquals(buildingA, endpointA.getBuilding());
        assertEquals(rackA, endpointA.getRack());
        assertEquals(labelA, endpointA.getLabel());

        final Endpoint endpointB = cable.getEndpointB();
        assertEquals(deviceB, endpointB.getDevice());
        assertEquals(buildingB, endpointB.getBuilding());
        assertEquals(rackB, endpointB.getRack());
        assertEquals(labelB, endpointB.getLabel());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow updating an instance by
     * clearing all optional data.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_updateClearOptionalData() {
        final Cable cable = cableService.getCableByName("12C000001");
        cable.setSystem("1");
        cable.setSubsystem("2");
        cable.setCableClass("C");
        cable.setSeqNumber(1);
        cable.setOwners(Arrays.asList("rbactester1"));
        cable.setStatus(CableStatus.APPROVED);
        cable.setAutoCalculatedLength(CableAutoCalculatedLength.YES);
        cable.getEndpointA().update("LEBT-01:PBI-NPM-02", null, null, null, null, "Endopoint 1");
        cable.getEndpointB().update("LEBT-01:Ctrl-Box-01", null, null, null, null, "Endopoint 2");
        final InputStream inputStream = convertToExcel(cable);

        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow updating an instance with
     * wiring drawing URL as Excel link containing whitespace.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_updateURLWhitespace() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "http://example.org/updatedDrawingA/");
        final InputStream inputStream = cableSaver.save();

        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * malformed cable number.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateMalformedCableName() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.NAME, "12C00001");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating a non-existent
     * instance.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateNonexistent() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.NAME, "12C000002");
        cableSaver.setValue(0, CableColumn.DEVICE_B_WIRING, "http://exampleorg");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow changing an instance owner.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateOwner() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.OWNERS, "testuser2");
        cableSaver.setValue(0, CableColumn.DEVICE_A_WIRING, "Example");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * installation by date in illegal format.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateIllegalInstallationDate() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.INSTALLATION_DATE, "01.01.2015");
        cableSaver.setValue(0, CableColumn.DEVICE_B_WIRING, "Example");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * termination by date in illegal format.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateIllegalTerminationDate() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.TERMINATION_DATE, "01.01.2015");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * non-numeric length.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateNonnumericLength() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setValue(0, CableColumn.LENGTH, "abc");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with a
     * malformed URL for wiring drawing A.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateMalformedDrawingA() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with a
     * malformed URL for wiring drawing B.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateMalformedDrawingB() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * wiring drawing B not being an URL.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateNonURLDrawingA() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow updating an instance with
     * installation by date in illegal format.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noUpdateNonURLDrawingB() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Allow deleting an instance.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_deleteInstance() {
        final String number = "12C000001";
        final Cable cable = cableService.getCableByName(number);
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setCommand(0, DSCommand.DELETE);
        final InputStream inputStream = cableSaver.save();

        LoaderResult<Cable> result = cableImportExportService.importCables(inputStream);
        assertFalse(result.toString(), result.isError());

        assertTrue(cableService.getCableByName(number).getStatus() == CableStatus.DELETED);
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow deleting an instance with
     * cable number not specified.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noDeleteNumberUndefined() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setCommand(0, DSCommand.DELETE);
        cableSaver.setValue(0, CableColumn.NAME, "");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }

    /**
     * Test for {@link CableImportExportService#importCables(java.io.InputStream)}. Disallow deleting an instance with
     * malformed cable number.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable_bare.yml" })
    @ApplyScriptAfter(value = "truncate_database.sql")
    public void importCablesTest_noDeleteNonexistingNumber() {
        final Cable cable = cableService.getCableByName("12C000001");
        final CableSaver cableSaver = getCableSaver(cable);
        cableSaver.setCommand(0, DSCommand.DELETE);
        cableSaver.setValue(0, CableColumn.NAME, "nonexisting");
        final InputStream inputStream = cableSaver.save();

        assertTrue(cableImportExportService.importCables(inputStream).isError());
    }
}
