/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.openepics.cable.CableProperties;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.services.dl.CableImportExportService;
import org.openepics.cable.services.dl.CableSaver;
import org.openepics.cable.services.dl.DSCommand;

/**
 * This is a base class for integration tests for {@link CableImportExportService}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public abstract class CableImportExportServiceBase extends TestBase {

    /** The session service to use for authentication during tests. */
    @Inject
    protected SessionService sessionService;
    /** The service to test. */
    @Inject
    protected CableImportExportService cableImportExportService;

    /**
     * Returns an example of a cable
     *
     * @param owner
     *            the cable owner
     *
     * @return the cable
     */
    protected static Cable getExampleCable(String owner) {
        try {
            return new Cable("4", "2", "A", 1, Arrays.asList(CableProperties.getInstance().getTestCableUser()),
                    CableStatus.ROUTED, new CableType("cabletype1", InstallationType.OUTDOOR), null,
                    new Endpoint(CableProperties.getInstance().getTestDeviceName(), "G02", "Rack 322", null, null,
                            null),
                    new Endpoint(CableProperties.getInstance().getTestDeviceName(), "G01", null, null, null, null),
                    new Date(), new Date(), null, DateUtil.parse("2015-03-01 10:00:00"),
                    DateUtil.parse("2015-03-10 10:00:00"), null, CableAutoCalculatedLength.NO, 1.0f, null, null);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Helper method that creates a {@link CableSaver} that can save a set of cables.
     *
     * @param cables
     *            the cables to save
     * @return the saver
     */
    protected static CableSaver getCableSaver(Cable... cables) {
        return new CableSaver(Arrays.asList(cables));
    }

    /**
     * Helper method that creates a {@link CableSaver} that can save a set of cables.
     *
     * @param commands
     *            the commands to set for handling cables
     * @param cables
     *            the cables to save
     * @return the saver
     */
    protected static CableSaver getCableSaver(List<DSCommand> commands, Cable... cables) {
        final CableSaver cableSaver = getCableSaver(cables);
        if (commands != null) {
            for (int i = 0; i < commands.size(); i++) {
                cableSaver.setCommand(i, commands.get(i));
            }
        }
        return cableSaver;
    }

    /**
     * Helper method that an Excel file containing a set of cables.
     *
     * @param commands
     *            the commands wanted in excel file
     * @param cables
     *            the cables
     * @return a stream representing the Excel file
     */
    protected static InputStream convertToExcel(List<DSCommand> commands, Cable... cables) {
        return getCableSaver(commands, cables).save();
    }

    /**
     * Helper method that an Excel file containing a set of cables.
     *
     * @param cables
     *            the cables
     * @return a stream representing the Excel file
     */
    protected static InputStream convertToExcel(Cable... cables) {
        return getCableSaver(cables).save();
    }

    /**
     * Returns Excel file with an example cable.
     *
     * @param owner
     *            the owner of the cable
     *
     * @return a stream representing the Excel file
     */
    protected static InputStream getExampleExcel(String owner) {
        return convertToExcel(getCreateCommands(1), getExampleCable(owner));
    }
}
