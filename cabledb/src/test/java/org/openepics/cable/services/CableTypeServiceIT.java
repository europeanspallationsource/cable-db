/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.core.IsInstanceOf;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.InstallationType;

/**
 * Tests the {@link CableTypeService} using arquillian framework integration test. Data sets for the tests are stored in
 * resources package.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableTypeServiceIT extends TestBase {

    @Inject
    private CableTypeService cableTypeService;

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    /** Tests {@link CableTypeService#getCableType(String)} by retrieving some existing and non-existing types. */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getCableTypeTest() {

        final CableType cableType2 = cableTypeService.getCableType("cabletype2");
        assertNotNull(cableType2);
        assertEquals("cabletype2", cableType2.getName());

        final CableType cableType4 = cableTypeService.getCableType("cabletype4");
        assertNull(cableType4);
    }

    /**
     * Tests {@link CableTypeService#getAllCableTypes()} by checking the number of retrieved cable types and the
     * attribute values.
     */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getAllCableTypesTest() {

        final List<CableType> cableTypes = cableTypeService.getAllCableTypes();

        assertEquals(3, cableTypes.size());

        final CableType cableType = cableTypes.get(0);
        assertNotNull(cableType);

        assertEquals("cabletype1", cableType.getName());
        assertEquals("description", cableType.getDescription());
        assertEquals("service", cableType.getService());
        assertEquals(Integer.valueOf(24), cableType.getVoltage());
        assertEquals("insulation", cableType.getInsulation());
        assertEquals("jacket", cableType.getJacket());
        assertEquals("flammability", cableType.getFlammability());
        assertEquals(InstallationType.OUTDOOR, cableType.getInstallationType());
        assertEquals(1, cableType.getTid(), 0);
        assertEquals(32.1, cableType.getWeight(), 0.00001);
        assertEquals(12.3, cableType.getDiameter(), 0.00001);
        assertEquals("comments", cableType.getComments());
        assertEquals("revision", cableType.getRevision());
        assertTrue(cableType.isActive());
    }

    /**
     * Tests {@link CableTypeService#createCableType} by creating a cable type and checking data stored in the database.
     */
    @Test
    @ShouldMatchDataSet("CableTypeServiceTest#createCableTypeTest.yml")
    public void createCableTest() throws ParseException, MalformedURLException {

        cableTypeService.createCableType("name", InstallationType.OUTDOOR, "description", "service", 12, "insulation",
                "jacket", "flammability", 100f, 32.1f, 12.3f, null, "new comments", "new revision", "test");
    }

    /** Tests that {@link CableTypeService#createCableTypeCable} fails when creating a cable type with the same name. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void createCableTypeTest_nonUniqueName() throws MalformedURLException {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalArgumentException.class));

        cableTypeService.createCableType("cabletype1", InstallationType.UNKNOWN, "description", "service", 12,
                "insulation", "jacket", "flammability", 100f, 32.1f, 12.3f, null, "new comments", "new revision",
                "test");
    }

    /**
     * Tests {@link CableTypeService#updateCableType} by updating a cable type and checking that attributes changed in
     * the database.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    @ShouldMatchDataSet("CableTypeServiceTest#updateCableTypeTest.yml")
    public void updateCableTypeTest() throws ParseException, MalformedURLException {

        final CableType oldCableType = cableTypeService.getAllCableTypes().get(0);
        cableTypeService.detachCableType(oldCableType);
        final CableType cableType = cableTypeService.getAllCableTypes().get(0);
        cableType.setDescription("new description");
        cableType.setService("new service");
        cableType.setVoltage(24);
        cableType.setInsulation("new insulation");
        cableType.setJacket("new jacket");
        cableType.setFlammability("new flammability");
        cableType.setInstallationType(InstallationType.INDOOR);
        cableType.setTid(122f);
        cableType.setWeight(43.21f);
        cableType.setDiameter(12.34f);
        cableType.setComments("new comments");
        cableType.setRevision("new revision");

        cableTypeService.updateCableType(cableType, oldCableType, "test");
    }

    /** Tests that {@link CableTypeService#upcreateCableTypeCable} fails when not specifying installationType. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void updateCableTypeTest_undefinedInstallationType() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalArgumentException.class));
        cableTypeService.createCableType("cabletype1", null, "description", "service", 12, "insulation", "jacket",
                "flammability", 100f, 32.1f, 12.3f, null, "comments", "revision", "test");
    }

    /**
     * Tests {@link CableTypeService#deleteCableType} by deleting a cable type and checking that its state is set to
     * inactive.
     */
    @Test
    @UsingDataSet({ "cabletype.yml" })
    @ShouldMatchDataSet("CableTypeServiceTest#deleteCableTypeTest.yml")
    public void deleteCableTypeTest() {

        final CableType cableType = cableTypeService.getAllCableTypes().get(0);
        assertTrue(cableTypeService.deleteCableType(cableType, "test"));
    }

    /**
     * Tests that {@link CableTypeService#deleteCableType} returns false when deleting an already deleted cable type.
     */
    @Test
    @UsingDataSet({ "cabletype.yml" })
    public void deleteCableTypeTest_alreadyDeleted() {
        final CableType cableType = cableTypeService.getAllCableTypes().get(0);
        assertTrue(cableTypeService.deleteCableType(cableType, "test"));
        assertFalse(cableTypeService.deleteCableType(cableType, "test"));
    }

    /** Tests {@link CableTypeService#getVoltageValues()} by retrieving unique stored values. */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getVoltageValuesTest() {

        final Iterator<String> voltages = cableTypeService.getVoltageValues(CableTypeActiveFilter.ALL).iterator();
        assertEquals(24, voltages.next());
        assertEquals(12, voltages.next());
        assertFalse(voltages.hasNext());
    }

    /** Tests {@link CableTypeService#getInsulationValues()} by retrieving unique stored values. */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getInsulationValuesTest() {

        final Iterator<String> insulations = cableTypeService.getInsulationValues(CableTypeActiveFilter.ALL).iterator();
        assertEquals("insulation", insulations.next());
        assertEquals("another insulation", insulations.next());
        assertFalse(insulations.hasNext());
    }

    /** Tests {@link CableTypeService#getJacketValues()} by retrieving unique stored values. */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getJacketValuesTest() {

        final Iterator<String> jackets = cableTypeService.getJacketValues(CableTypeActiveFilter.ALL).iterator();
        assertEquals("jacket", jackets.next());
        assertEquals("another jacket", jackets.next());
        assertFalse(jackets.hasNext());
    }

    /** Tests {@link CableTypeService#getFlammabilityValues()} by retrieving unique stored values. */
    @Test
    @UsingDataSet({ "cabletypes.yml" })
    public void getFlammabilityValuesTest() {

        final Iterator<String> flammability = cableTypeService.getFlammabilityValues(CableTypeActiveFilter.ALL)
                .iterator();
        assertEquals("flammability", flammability.next());
        assertEquals("another flammability", flammability.next());
        assertFalse(flammability.hasNext());
    }
}
