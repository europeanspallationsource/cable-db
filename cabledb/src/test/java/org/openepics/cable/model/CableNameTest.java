/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import org.junit.Test;

/**
 * Tests the {@link CableName}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableNameTest {

    /** Tests constructing a valid number. */
    @Test
    public void createValidNumber() {
        new CableName("12c000001");
    }

    /** Tests that constructing a number with invalid system fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSystem() {
        new CableName("a2c000001");
    }

    /** Tests that constructing a number with invalid subsystem fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSubsystem() {
        new CableName("1-c000001");
    }

    /** Tests that constructing a number with invalid class fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidClass() {
        new CableName("121000001");
    }

    /** Tests that constructing a number with invalid sequential number length fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSeqNumber() {
        new CableName("12c00001");
    }
}
