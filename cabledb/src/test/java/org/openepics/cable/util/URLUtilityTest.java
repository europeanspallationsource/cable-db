/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author lajoh3
 * Created 26 okt. 2018
 */
public class URLUtilityTest {

    @Test
    public void testEncodeURLNull() {
        String url = null;
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLEmpty() {
        String url = "";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLWhitespace() {
        String url = " ";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLString() {
        String url = "abcd1234";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPath() {
        String url = "abcd.xhtml";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQuery() {
        String url = "abcd.xhtml?efgh";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryPlus() {
        String url = "abcd.xhtml?efg+h";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryWhitespace() {
        String url = "abcd.xhtml?ef gh";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue1Pair() {
        String url = "abcd.xhtml?efgh=ijkl";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue1PairEmpty() {
        String url = "abcd.xhtml?efgh=";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue1PairComplex() {
        String url = "abcd.xhtml?efgh=ijkl mnop+qrst+ uvwx";
        String expected = "abcd.xhtml?efgh=ijkl+mnop%2Bqrst%2B+uvwx";
        assertEquals(expected, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue2Pairs() {
        String url = "abcd.xhtml?efgh=ijkl&mnop=qrst";
        assertEquals(url, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue2PairsComplex() {
        String url = "abcd.xhtml?efgh=i+kl&mnop=qr t";
        String expected = "abcd.xhtml?efgh=i%2Bkl&mnop=qr+t";
        assertEquals(expected, URLUtility.encodeURL(url));
    }

    @Test
    public void testEncodeURLPathQueryAttributeValue3PairsComplex() {
        String url = "abcd.xhtml?efgh&uvwx=yzabc  def++ghijklm+ nop&abcdefgh=";
        String expected = "abcd.xhtml?efgh&uvwx=yzabc++def%2B%2Bghijklm%2B+nop&abcdefgh=";
        assertEquals(expected, URLUtility.encodeURL(url));
    }

}
