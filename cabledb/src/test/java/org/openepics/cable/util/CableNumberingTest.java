/*
 *  Copyright (c) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author georgweiss
 * Created Sep 19, 2018
 */
public class CableNumberingTest {
    
    @Test
    public void testGetSystemNumbers(){
        assertEquals(9, CableNumbering.getSystemNumbers().length);
    } 
    
    @Test
    public void testGetSubSystemNumbers(){
        assertEquals(10, CableNumbering.getSubsystemNumbers().length);
    } 
    
    @Test
    public void testGetClassLetters(){
        assertEquals(7, CableNumbering.getClassLetters().length);
    } 
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetSystemLabel(){
        assertFalse(CableNumbering.getSystemLabel("2").isEmpty());
        assertTrue(CableNumbering.getSystemLabel("A").isEmpty());
        
        // This will throw an IndexOutOfBoundsException
        CableNumbering.getSystemLabel("-1");
    } 
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetSubSystemLabel(){
        assertFalse(CableNumbering.getSubsystemLabel("2", "2").isEmpty());
        assertTrue(CableNumbering.getSubsystemLabel("A", "2").isEmpty());
        assertTrue(CableNumbering.getSubsystemLabel("2", "A").isEmpty());
        assertTrue(CableNumbering.getSubsystemLabel("A", "A").isEmpty());

        
        // This will throw an IndexOutOfBoundsException
        CableNumbering.getSubsystemLabel("2", "-1");
    } 
    
     
    @Test
    public void testGetClassLabel(){
        assertFalse(CableNumbering.getClassLabel("A").isEmpty());
        assertTrue(CableNumbering.getClassLabel("1").isEmpty());

    } 
    
    @Test
    public void testGetSystemNumber(){
        
        assertTrue(CableNumbering.getSystemNumber(null).isEmpty());
        assertTrue(CableNumbering.getSystemNumber("").isEmpty());
        assertEquals("1", CableNumbering.getSystemNumber("1xxxx"));
    }
    
    @Test
    public void testGetSubSystemNumber(){
        
        assertTrue(CableNumbering.getSubsystemNumber(null).isEmpty());
        assertTrue(CableNumbering.getSubsystemNumber("").isEmpty());
        assertEquals("1", CableNumbering.getSubsystemNumber("1xxxx"));
    }
    
    @Test
    public void testGetCableClassLettter(){
        
        assertTrue(CableNumbering.getCableClassLetter(null).isEmpty());
        assertTrue(CableNumbering.getCableClassLetter("").isEmpty());
        assertEquals("A", CableNumbering.getCableClassLetter("A7777"));
    }
    
    @Test
    public void testIsValidSystem(){
        
        assertTrue(CableNumbering.isValidSystem("1"));
        assertFalse(CableNumbering.isValidSystem(null));
        assertFalse(CableNumbering.isValidSystem(""));
        assertFalse(CableNumbering.isValidSystem("-1"));
    }
    
    @Test
    public void testIsValidSubSystem(){
        
        assertTrue(CableNumbering.isValidSubsystem("1", "1"));
        assertFalse(CableNumbering.isValidSubsystem(null, null));
        assertFalse(CableNumbering.isValidSubsystem("", ""));
        assertFalse(CableNumbering.isValidSubsystem("-1", "-1"));
        assertFalse(CableNumbering.isValidSubsystem("1", "A"));
        assertFalse(CableNumbering.isValidSubsystem("A", "1"));
        assertFalse(CableNumbering.isValidSubsystem("A", "A"));
    }
    
    @Test
    public void testIsValidCableClass(){
        assertTrue(CableNumbering.isValidCableClass("A111"));
        assertFalse(CableNumbering.isValidCableClass(""));
        assertFalse(CableNumbering.isValidCableClass(null));
        assertFalse(CableNumbering.isValidCableClass("1"));
    }
    
}
