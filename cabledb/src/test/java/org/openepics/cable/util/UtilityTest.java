/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author lajoh3
 * Created 7 nov. 2018
 */
public class UtilityTest {

    @Test
    public void testEqualsFloatNull() {
        Float f = null;
        assertFalse(Utility.equalsFloat(f));
    }

    @Test
    public void testEqualsFloat1Param() {
        Float f = 1f;
        assertFalse(Utility.equalsFloat(f));
    }

    @Test
    public void testEqualsFloat2Params1() {
        Float f1 = null;
        Float f2 = null;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    @Test
    public void testEqualsFloat2Params2() {
        Float f1 = null;
        Float f2 = 1f;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    @Test
    public void testEqualsFloat2Params3() {
        Float f1 = 1f;
        Float f2 = null;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    @Test
    public void testEqualsFloat2Params4() {
        Float f1 = 1f;
        Float f2 = 2f;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    @Test
    public void testEqualsFloat2Params5() {
        Float f1 = 1f;
        Float f2 = 1f;
        assertTrue(Utility.equalsFloat(f1, f2));
    }

    @Test
    public void testEqualsFloat3Params1() {
        Float f1 = 1.5f;
        Float f2 = null;
        Float f3 = 3.5f;
        assertFalse(Utility.equalsFloat(f1, f2, f3));
    }

    @Test
    public void testEqualsFloat3Params2() {
        Float f1 = 1.5f;
        Float f2 = 1.5f;
        Float f3 = 3.5f;
        assertFalse(Utility.equalsFloat(f1, f2, f3));
    }

    @Test
    public void testEqualsFloat3Params3() {
        Float f1 = 3.5f;
        Float f2 = 3.5f;
        Float f3 = 3.5f;
        assertTrue(Utility.equalsFloat(f1, f2, f3));
    }

}
