# Cable Database

The Cable Database (CDB) is meant to properly manage the information about cables that the ESS Machine Directorate’s Divisions (and, eventually, its in-kind collaborators) will be responsible for.

More specifically, the CDB supports the tracking, configuration and labeling of (thousands of) cables in all phases of the ESS project (design, installation, maintenance and troubleshooting).

This information is then consumed both by end-users and other ICS applications (e.g. CCDB) to enable these to successfully perform their domain specific businesses.

## Docker

The application can be built and run as a Docker container. The image is based on the jboss/wildfly image from Docker Hub.

### How to use this image

```
$ docker run registry.esss.lu.se/ics-software/cable-db
```

Environment variables that can be set when running a container based on this image:

| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| JBOSS_BIND_ADDRESS_MANAGEMENT | 127.0.0.1 | Network interface address to bind Management and JMX port to |
| CABLEDB_DEPLOYMENT_CONTEXT_ROOT | / | Context root used for the application |
| CABLEDB_DATABASE_URL | jdbc:postgresql://cabledb-postgres:5432/cabledb | JDBC url for the database connection |
| CABLEDB_DATABASE_USERNAME | cabledb | Username used for the database connection |
| CABLEDB_DATABASE_PASSWORD | cabledb | Password used for the database connection |
| CABLEDB_APPLICATION_BASE_URL |  | URL for the web application |
| CABLEDB_CABLE_NUMBERING_DOCUMENT_URL | https://chess.esss.lu.se/enovia/tvc-action/showObject/dmg_AssignmentSpecification/ESS-0015420/1 | Numbering document in Chess |
| CABLEDB_CCDB_URL | https://ccdb.esss.lu.se/ | URL for CCDB |
| CABLEDB_CHESS_URL | https://chess.esss.lu.se/enovia/link/cableName/ | URL for Chess |
| CABLEDB_MAIL_NOTIFICATION | false | Enable mail notification |
| CABLEDB_SUPPORT_EMAIL | Icsscontrolsystemsupport@esss.se | Support email |
| CABLEDB_DATABASE_MIGRATION | true | Perform Flyway data migration |
| NAMES_SERVICES_BASE_URL | https://naming.esss.lu.se/rest | URL for Naming REST services |
| NAMES_APP_URL | https://naming.esss.lu.se | URL for Naming Web application |
| RBAC_PRIMARY_URL | https://rbac.esss.lu.se:8443/service | URL for primary RBAC service |
| RBAC_PRIMARY_SSL_HOST | rbac.esss.lu.se | SSL host for primary RBAC service |
| RBAC_PRIMARY_SSL_PORT | 8443 | SSL port for primary RBAC service |
| RBAC_SINGLE_SIGNON | false | Use single sign-on |
| RBAC_SECONDARY_URL | https://localhost:8443/service | URL for secondary RBAC service |
| RBAC_SECONDARY_SSL_HOST | localhost | SSL host for secondary RBAC service |
| RBAC_SECONDARY_SSL_PORT | 8443 | SSL port for secondary RBAC service |
| RBAC_HANDSHAKE | true | Perform SSL handshake with RBAC |
| RBAC_HANDSHAKE_TIMEOUT | 2000 | Timeout for SSL handshake with RBAC |
| RBAC_INACTIVITY_TIMEOUT_DEFAULT | 900 | Inactivity timeout for RBAC client |
| RBAC_INACTIVITY_RESPONSE_GRACE | 30 | Inactivity response grace period for RBAC client |
| RBAC_SHOW_ROLE_SELECTOR | false | Show role selector in RBAC client |
| RBAC_VERIFY_SIGNATURE | false | Verify signature of RBAC |
| RBAC_PUBLIC_KEY_LOCATION | ~/.rbac/rbac.key | Public key for RBAC |
| RBAC_LOCAL_SERVICES_PORT | 9421 | Port for local RBAC service |
| RBAC_USE_LOCAL_SERVICE | false | Use local RBAC service |
| RBAC_CERTIFICATE_STORE | ~/.rbac | SSL certificate store for RBAC client |
| RBAC_LDAP_SECURITY_CREDENTIALS |  | LDAP bind password |

### Docker Compose

For convenience, the application comes with a docker-compose.yml file, which can be used to run the application with required services and configuration:

```
$ docker-compose up
```
Note: To be able to run the application, the environment variable RBAC_LDAP_SECURITY_CREDENTIALS must be added with the correct password for the LDAP bind.
