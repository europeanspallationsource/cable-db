/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This represents an instance of display view column.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class DisplayViewColumn extends Persistable implements Comparable<DisplayViewColumn> {

    private static final long serialVersionUID = -6007734251143927822L;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_COLUMN_displayView"),
        name = "displayView", referencedColumnName = "id")
    private DisplayView displayView;

    private String columnName;
    private Integer position;

    /** Constructor for JPA entity. */
    public DisplayViewColumn() {}

    /**
     * Constructs new display view condition.
     *
     * @param displayView
     *            display view
     * @param columnName
     *            name of the column
     * @param position
     *            position
     */
    public DisplayViewColumn(DisplayView displayView, String columnName, Integer position) {
        this.displayView = displayView;
        this.columnName = columnName;
        this.position = position;
    }

    /** @return display view condition parent (display view) */
    public DisplayView getDisplayView() {
        return displayView;
    }

    /** @return display view condition column name */
    public String getColumnName() {
        return columnName;
    }

    /** @return display view condition position for execution */
    public Integer getPosition() {
        return position;
    }

    public void setDisplayView(DisplayView displayView) {
        this.displayView = displayView;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public int compareTo(DisplayViewColumn o) {
        if (this.position < o.getPosition()) {
            return -1;
        } else if (this.position > o.getPosition()) {
            return 1;
        }
        return 0;
    }

    /**
     * Override "equals(Object obj)" to comply with the contract of the "compareTo(T
     * o)" method.
     *
     * <ul>
     * <li>It is strongly recommended, but not strictly required that
     * (x.compareTo(y)==0) == (x.equals(y)).
     * </ul>
     *
     * @see Comparable#compareTo(Object)
     * @see Object#hashCode()
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object obj) {
        // overridden to compl
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        DisplayViewColumn other = (DisplayViewColumn) obj;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        return true;
    }

    /**
     * This class overrides "equals()" and should therefore also override
     * "hashCode()".
     *
     * <ul>
     * <li>If two objects are equal according to the equals(Object) method, then
     * calling the hashCode method on each of the two objects must produce the same
     * integer result.
     * <li>It is not required that if two objects are unequal according to the
     * equals(java.lang.Object) method, then calling the hashCode method on each of
     * the two objects must produce distinct integer results.
     * </ul>
     *
     * @see Comparable#compareTo(Object)
     * @see Object#hashCode()
     * @see Object#equals(Object)
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        return result;
    }

}
