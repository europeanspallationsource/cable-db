/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;

import com.google.common.base.Preconditions;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * This represents an instance of cable.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Cable.findAll", query = "SELECT c FROM Cable c"),
})
public class Cable extends Persistable {

    private static final long serialVersionUID = -7654156206144341815L;

    /** This represents the data validity of the cable. */
    public enum Validity {
        /** The cable state approved, this is the default status. */
        VALID,
        /** One of the cable endpoints is not in valid state. */
        DANGLING
    }

    private String system;
    private String subsystem;
    private String cableClass;
    private Integer seqNumber;
    @ElementCollection(fetch = FetchType.LAZY)
    @OrderColumn(name = "position")
    private List<String> owners = new ArrayList<>();
    private String ownersString;

    @ManyToOne
    private CableType cableType;
    private String container;
    @OneToOne
    private Endpoint endpointA;
    @OneToOne
    private Endpoint endpointB;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "cable")
    @OrderColumn(name = "position")
    private List<RoutingRow> routingRows = new ArrayList<>();
    private String routingsString;

    private Date installationBy;
    private Date terminationBy;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "artifact_id")
    private GenericArtifact qualityReport;

    @Enumerated(EnumType.STRING)
    private CableAutoCalculatedLength autoCalculatedLength = CableAutoCalculatedLength.NO;
    private Float baseLength;
    private Float length;
    private String comments;
    private Date created;
    private Date modified;
    @Enumerated(EnumType.STRING)
    private CableStatus status = CableStatus.INSERTED;
    @Enumerated(EnumType.STRING)
    private Validity validity = Validity.VALID;
    private String revision;

    /** Constructor for JPA entity. */
    private Cable() {}

    /**
     * Creates a new instance of a cable.
     *
     * @param system
     *            the cable system digit
     * @param subsystem
     *            the cable subsystem digit
     * @param cableClass
     *            the cable class letter
     * @param seqNumber
     *            the cable sequential number
     * @param owners
     *            the cable owners
     * @param created
     *            the creation date of this cable
     * @param modified
     *            the modification date of this cable
     */
    public Cable(String system, String subsystem, String cableClass, Integer seqNumber, List<String> owners,
            Date created, Date modified) {
        this(system, subsystem, cableClass, seqNumber, owners, CableStatus.INSERTED, null, null, null, null, created,
                modified, null, null, null, null, CableAutoCalculatedLength.NO, null, null, null);
    }

    /**
     * Creates a new instance of a cable.
     *
     * @param system
     *            the cable system digit
     * @param subsystem
     *            the cable subsystem digit
     * @param cableClass
     *            the cable class letter
     * @param seqNumber
     *            the cable sequential number
     * @param owners
     *            the cable owners
     * @param status
     *            the cable status
     * @param cableType
     *            the cable type
     * @param container
     *            the container cable number of this cable
     * @param endpointA
     *            endpoint a of the cable
     * @param endpointB
     *            endpoint b of the cable
     * @param created
     *            the creation date of this cable
     * @param modified
     *            the modification date of this cable
     * @param routingRows
     *            the routing value for this cable instance
     * @param installationBy
     *            the date by which this cable is installed
     * @param terminationBy
     *            the date by which this cable is terminated
     * @param qualityReport
     *            the qualityReport artifact
     * @param autoCalculatedLength
     *            boolean indicating if length was auto calculated
     * @param length
     *            the user specified cable length
     * @param comments
     * 			  the comments of this cable
     * @param revision
     *            the revision of this cable
     */
    public Cable(String system, String subsystem, String cableClass, Integer seqNumber, List<String> owners,
            CableStatus status, CableType cableType, String container, Endpoint endpointA, Endpoint endpointB,
            Date created, Date modified, List<RoutingRow> routingRows, Date installationBy, Date terminationBy,
            GenericArtifact qualityReport, CableAutoCalculatedLength autoCalculatedLength, Float length,
            String comments, String revision) {
        Preconditions.checkArgument(owners != null && !owners.isEmpty());
        Preconditions.checkNotNull(status);
        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        final CableName cableName = new CableName(system, subsystem, cableClass, seqNumber != null ? seqNumber : 0);
        this.system = cableName.getSystem();
        this.subsystem = cableName.getSubsystem();
        this.cableClass = cableName.getCableClass();
        this.seqNumber = seqNumber != null ? cableName.getSeqNumber() : null;

        this.owners.addAll(owners);
        updateOwnersString();
        this.status = status;
        this.cableType = cableType;
        this.container = container;
        this.endpointA = endpointA;
        this.endpointB = endpointB;
        this.created = created; // NOSONAR
        this.modified = modified; // NOSONAR

        setRoutingRows(routingRows);

        this.installationBy = installationBy; // NOSONAR
        this.terminationBy = terminationBy; // NOSONAR
        this.qualityReport = qualityReport;
        this.autoCalculatedLength = autoCalculatedLength;
        this.baseLength = 0f;
        this.length = length;
        this.comments = comments;
        this.revision = revision;
    }

    /**
     * Updates routings.
     */
    public void updateRoutings() {
        if (routingRows != null) {
            // Sort by position
            Collections.sort(routingRows, (a, b) -> a.getPosition() - b.getPosition());
            List<String> routingNames = new ArrayList<String>();
            for (RoutingRow routing : routingRows) {
                if (routing.getRouting().isActive()) {
                    routingNames.add(routing.getRouting().getName());
                } else {
                    routingNames.add("<strike>" + routing.getRouting().getName() + "</strike>");
                }
            }
            this.routingsString = String.join(", ", routingNames);
        } else {
            this.routingsString = "";
        }
    }

    /**
     * Sets the modified date of this cable instance.
     *
     * @param modified
     *            the modified date to set
     */
    public void setModified(Date modified) {
        this.modified = modified; // NOSONAR
    }

    /**
     * Sets the status of this cable instance.
     *
     * @param status
     *            the status to set
     */
    public void setStatus(CableStatus status) {
        Preconditions.checkNotNull(status);
        this.status = status;
    }

    /**
     * Sets the validity of this cable instance.
     *
     * @param validity
     *            the validity to set
     */
    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    /** @return the cable number (example: 12A012345) */
    public String getName() {
        return CableName.asString(system, subsystem, cableClass, seqNumber);
    }

    /** @return the cable system digit */
    public String getSystem() {
        return system;
    }

    /** @return the cable subsystem digit */
    public String getSubsystem() {
        return subsystem;
    }

    /** @return the cable class letter */
    public String getCableClass() {
        return cableClass;
    }

    /** @return the cable sequential number */
    public Integer getSeqNumber() {
        return seqNumber;
    }

    /** @return the cable owners */
    public List<String> getOwners() {
        return owners;
    }

    /** Updates owners string. */
    public void updateOwnersString() {
        ownersString = String.join(", ", owners);
    }

    /** @return the cable owners joined in a string */
    public String getOwnersString() {
        return ownersString;
    }

    /** @return the routing value for this cable instance */
    public List<RoutingRow> getRoutingRows() {
        return routingRows;
    }

    /** @return the date by which this cable is installed */
    public Date getInstallationBy() {
        return installationBy; // NOSONAR
    }

    /** @return the date by which this cable is terminated */
    public Date getTerminationBy() {
        return terminationBy; // NOSONAR
    }

    /** @return the base length used when using autocalculated length */
    public Float getBaseLength() {
        return baseLength;
    }

    /**
     * @param baseLength
     *            the base length used when using autocalculated length
     */
    public void setBaseLength(Float baseLength) {
        this.baseLength = baseLength;
    }

    /** @return the user specified cable length */
    public Float getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(Float length) {
        this.length = length;
    }

    /** @return the creation date of this cable */
    public Date getCreated() {
        return created; // NOSONAR
    }

    /** @return the modification date of this cable */
    public Date getModified() {
        return modified; // NOSONAR
    }

    /** @return the cable status */
    public CableStatus getStatus() {
        return status;
    }

    /** @return the cable type */
    public CableType getCableType() {
        return cableType;
    }

    /** @return the endpoint a of the cable */
    public Endpoint getEndpointA() {
        return endpointA;
    }

    /** @return the endpoint b of the cable */
    public Endpoint getEndpointB() {
        return endpointB;
    }

    /** @return the data validity of the cable */
    public Validity getValidity() {
        return validity;
    }

    /** @return true if the cable data is valid, else false */
    public boolean isValid() {
        return validity == Validity.VALID;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public CableAutoCalculatedLength getAutoCalculatedLength() {
        return autoCalculatedLength;
    }

    public void setAutoCalculatedLength(CableAutoCalculatedLength autoCalculatedLength) {
        this.autoCalculatedLength = autoCalculatedLength;
    }

    /**
     * @return concatenated string of the routing value for this cable instance
     */
    public String getRoutingsString() {
        return routingsString;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((cableClass == null) ? 0 : cableClass.hashCode());
        result = prime * result + ((cableType == null) ? 0 : cableType.hashCode());
        result = prime * result + ((container == null) ? 0 : container.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((endpointA == null) ? 0 : endpointA.hashCode());
        result = prime * result + ((endpointB == null) ? 0 : endpointB.hashCode());
        result = prime * result + ((installationBy == null) ? 0 : installationBy.hashCode());
        result = prime * result + ((autoCalculatedLength == null) ? 0 : autoCalculatedLength.hashCode());
        result = prime * result + ((baseLength == null) ? 0 : baseLength.hashCode());
        result = prime * result + ((length == null) ? 0 : length.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((owners == null) ? 0 : owners.hashCode());
        result = prime * result + ((routingRows == null) ? 0 : routingRows.hashCode());
        result = prime * result + ((routingsString == null) ? 0 : routingsString.hashCode());
        result = prime * result + ((seqNumber == null) ? 0 : seqNumber.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((subsystem == null) ? 0 : subsystem.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        result = prime * result + ((terminationBy == null) ? 0 : terminationBy.hashCode());
        result = prime * result + ((qualityReport == null) ? 0 : qualityReport.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cable other = (Cable) obj;
        if (cableClass == null) {
            if (other.cableClass != null)
                return false;
        } else if (!cableClass.equals(other.cableClass))
            return false;
        if (this.id != null)
            return id.equals(other.id);
        if (cableType == null) {
            if (other.cableType != null)
                return false;
        } else if (!cableType.equals(other.cableType))
            return false;
        if (container == null) {
            if (other.container != null)
                return false;
        } else if (!container.equals(other.container))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (endpointA == null) {
            if (other.endpointA != null)
                return false;
        } else if (!endpointA.equals(other.endpointA))
            return false;
        if (endpointB == null) {
            if (other.endpointB != null)
                return false;
        } else if (!endpointB.equals(other.endpointB))
            return false;
        if (installationBy == null) {
            if (other.installationBy != null)
                return false;
        } else if (!installationBy.equals(other.installationBy))
            return false;
        if (baseLength == null) {
            if (other.baseLength != null)
                return false;
        } else if (!baseLength.equals(other.baseLength))
            return false;
        if (length == null) {
            if (other.length != null)
                return false;
        } else if (!length.equals(other.length))
            return false;
        if (autoCalculatedLength == null) {
            if (other.autoCalculatedLength != null)
                return false;
        } else if (!autoCalculatedLength.equals(other.autoCalculatedLength))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (owners == null) {
            if (other.owners != null)
                return false;
        } else if (!owners.equals(other.owners))
            return false;
        if (routingRows == null) {
            if (other.routingRows != null)
                return false;
        } else if (!routingRows.equals(other.routingRows))
            return false;
        if (routingsString == null) {
            if (other.routingsString != null)
                return false;
        } else if (!routingsString.equals(other.routingsString))
            return false;
        if (seqNumber == null) {
            if (other.seqNumber != null)
                return false;
        } else if (!seqNumber.equals(other.seqNumber))
            return false;
        if (status != other.status)
            return false;
        if (subsystem == null) {
            if (other.subsystem != null)
                return false;
        } else if (!subsystem.equals(other.subsystem))
            return false;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        if (terminationBy == null) {
            if (other.terminationBy != null)
                return false;
        } else if (!terminationBy.equals(other.terminationBy))
            return false;
        if (qualityReport == null) {
            if (other.qualityReport != null)
                return false;
        } else if (!qualityReport.equals(other.qualityReport))
            return false;
        if (validity != other.validity)
            return false;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (revision == null) {
            if (other.revision != null)
                return false;
        } else if (!revision.equals(other.revision))
            return false;
        return true;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public void setSubsystem(String subsystem) {
        this.subsystem = subsystem;
    }

    public void setCableClass(String cableClass) {
        this.cableClass = cableClass;
    }

    public void setSeqNumber(Integer seqNumber) {
        this.seqNumber = seqNumber;
    }

    public void setOwners(List<String> owners) {
        if (this.owners != owners) {
            this.owners.clear();
            if (owners != null) {
                this.owners.addAll(owners);
            }
            updateOwnersString();
        }
    }

    public void setOwnersString(String ownersString) {
        this.ownersString = ownersString;
    }

    public void setCableType(CableType cableType) {
        this.cableType = cableType;
    }

    public void setEndpointA(Endpoint endpointA) {
        this.endpointA = endpointA;
    }

    public void setEndpointB(Endpoint endpointB) {
        this.endpointB = endpointB;
    }

    public void setRoutingRows(List<RoutingRow> routingRows) {
        if (this.routingRows != routingRows) {
            this.routingRows.clear();
            if (routingRows != null) {
                this.routingRows.addAll(routingRows);
            }
            updateRoutings();
        }
    }

    public void setRoutingsString(String routingsString) {
        this.routingsString = routingsString;
    }

    public void setInstallationBy(Date installationBy) {
        this.installationBy = installationBy;
    }

    public void setTerminationBy(Date terminationBy) {
        this.terminationBy = terminationBy;
    }

    public void setQualityReport(GenericArtifact qualityReport) {
        this.qualityReport = qualityReport;
    }

    public GenericArtifact getQualityReport(){
        return qualityReport;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return getName();
    }



}
