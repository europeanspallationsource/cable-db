/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import com.google.common.base.Preconditions;

/**
 * This represents an approved cable type.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class CableType extends Persistable {

    public static final int DESCRIPTION_FIELD_LENGTH = 65535;

    private static final long serialVersionUID = -5380881256777101018L;
    private String name;
    @Column(length = DESCRIPTION_FIELD_LENGTH)
    private String description;
    private String service;
    private Integer voltage;
    private String insulation;
    private String jacket;
    private String flammability;
    @Enumerated(EnumType.STRING)
    private InstallationType installationType;
    private Float tid;
    private Float weight;
    private Float diameter;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "cableType")
    @OrderColumn(name = "position")
    private List<CableTypeManufacturer> manufacturers = new ArrayList<>();
    private String comments;
    private boolean active = true;
    private String revision;

    /** Constructor for JPA entity. */
    public CableType() {}

    /**
     * Creates a new instance of cable type.
     *
     * @param name
     *            the name/code
     * @param installationType
     *            the allowed installation type
     */
    public CableType(String name, InstallationType installationType) {
        this(name, installationType, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    /**
     * Creates a new instance of cable type.
     *
     * @param name
     *            the name/code
     * @param installationType
     *            the allowed installation type
     * @param description
     *            the description
     * @param service
     *            the service/function description
     * @param voltage
     *            the voltage rating description
     * @param insulation
     *            the insulation material
     * @param jacket
     *            the type of jacket
     * @param flammability
     *            the flammability classification
     * @param tid
     *            the total ionizing dose in mrad
     * @param weight
     *            the weight in kg/meter
     * @param diameter
     *            the outer diameter in mm
     * @param manufacturers
     *            the manufacturers
     * @param comments
     *            the comments
     * @param revision
     *            the revision
     */
    public CableType(String name, InstallationType installationType, String description, String service,
            Integer voltage, String insulation, String jacket, String flammability, Float tid, Float weight,
            Float diameter, List<CableTypeManufacturer> manufacturers, String comments, String revision) {

        Preconditions.checkArgument(name != null && !name.isEmpty());
        Preconditions.checkNotNull(installationType);

        this.name = name;
        this.installationType = installationType;
        this.description = description;
        this.service = service;
        this.voltage = voltage;
        this.insulation = insulation;
        this.jacket = jacket;
        this.flammability = flammability;
        this.tid = tid;
        this.weight = weight;
        this.diameter = diameter;
        this.manufacturers.clear();
        if (manufacturers != null) {
            for (CableTypeManufacturer m : manufacturers) {
                this.manufacturers.add(m);
            }
        }
        this.comments = comments;
        this.revision = revision;
    }

    /**
     * Set the cable type active state.
     *
     * @param active
     *            true if this cable type is in active use, false if it was obsoleted
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /** @return true if this cable type is in active use, false if it was obsoleted */
    public boolean isActive() {
        return active;
    }

    /** @return the name/code */
    public String getName() {
        return name;
    }

    /** @return the description */
    public String getDescription() {
        return description;
    }

    /** @return the service/function description */
    public String getService() {
        return service;
    }

    /** @return the voltage rating description */
    public Integer getVoltage() {
        return voltage;
    }

    /** @return the insulation material */
    public String getInsulation() {
        return insulation;
    }

    /** @return the type of jacket */
    public String getJacket() {
        return jacket;
    }

    /** @return the flammability classification */
    public String getFlammability() {
        return flammability;
    }

    /** @return the allowed installation type */
    public InstallationType getInstallationType() {
        return installationType;
    }

    /** @return the total ionizing dose in mrad */
    public Float getTid() {
        return tid;
    }

    /** @return the weight in kg/meter */
    public Float getWeight() {
        return weight;
    }

    /** @return the outer diameter in mm */
    public Float getDiameter() {
        return diameter;
    }

    /** @return the comments */
    public String getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * @return the manufacturers
     */
    public List<CableTypeManufacturer> getManufacturers() {
        return manufacturers;
    }

    /**
     * Joins the names of all manufacturers and datasheets of this cable type for filtering/sorting purposes.
     *
     * @param addDatasheet
     *            boolean indicating whether or not to add datasheets to the string.
     * @return a concated string of manufacturer names and datasheets
     */
    public String getManufacturersAsString(boolean addDatasheet) {
        final StringBuilder builder = new StringBuilder();
        int i = 1;
        for (CableTypeManufacturer manufacturer : manufacturers) {
            builder.append(manufacturer.getManufacturer().getName());
            if (addDatasheet && manufacturer.getDatasheet() != null && manufacturer.getDatasheet().getName() != null
                    && !manufacturer.getDatasheet().getName().isEmpty()) {
                builder.append(" (");
                builder.append(manufacturer.getDatasheet().getName());
                builder.append(")");
            }
            if (i < manufacturers.size()) {
                builder.append(", ");
            }
            i++;
        }

        return builder.toString();

    }

    /**
     * @param manufacturers
     *            the manufacturers to set
     */
    public void setManufacturers(List<CableTypeManufacturer> manufacturers) {
        this.manufacturers.clear();
        if (manufacturers != null) {
            for (CableTypeManufacturer m : manufacturers) {
                this.manufacturers.add(m);
            }
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((diameter == null) ? 0 : diameter.hashCode());
        result = prime * result + ((flammability == null) ? 0 : flammability.hashCode());
        result = prime * result + ((installationType == null) ? 0 : installationType.hashCode());
        result = prime * result + ((insulation == null) ? 0 : insulation.hashCode());
        result = prime * result + ((jacket == null) ? 0 : jacket.hashCode());
        result = prime * result + ((manufacturers == null) ? 0 : manufacturers.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((service == null) ? 0 : service.hashCode());
        result = prime * result + ((tid == null) ? 0 : tid.hashCode());
        result = prime * result + ((voltage == null) ? 0 : voltage.hashCode());
        result = prime * result + ((weight == null) ? 0 : weight.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CableType other = (CableType) obj;
        if (this.id != null)
            return id.equals(other.id);
        if (active != other.active)
            return false;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (diameter == null) {
            if (other.diameter != null)
                return false;
        } else if (!diameter.equals(other.diameter))
            return false;
        if (flammability == null) {
            if (other.flammability != null)
                return false;
        } else if (!flammability.equals(other.flammability))
            return false;
        if (installationType != other.installationType)
            return false;
        if (insulation == null) {
            if (other.insulation != null)
                return false;
        } else if (!insulation.equals(other.insulation))
            return false;
        if (jacket == null) {
            if (other.jacket != null)
                return false;
        } else if (!jacket.equals(other.jacket))
            return false;
        if (manufacturers == null) {
            if (other.manufacturers != null)
                return false;
        } else if (!manufacturers.equals(other.manufacturers))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (service == null) {
            if (other.service != null)
                return false;
        } else if (!service.equals(other.service))
            return false;
        if (tid == null) {
            if (other.tid != null)
                return false;
        } else if (!tid.equals(other.tid))
            return false;
        if (voltage == null) {
            if (other.voltage != null)
                return false;
        } else if (!voltage.equals(other.voltage))
            return false;
        if (weight == null) {
            if (other.weight != null)
                return false;
        } else if (!weight.equals(other.weight))
            return false;
        if (revision == null) {
            if (other.revision != null)
                return false;
        } else if (!revision.equals(other.revision))
            return false;
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    public void setInsulation(String insulation) {
        this.insulation = insulation;
    }

    public void setJacket(String jacket) {
        this.jacket = jacket;
    }

    public void setFlammability(String flammability) {
        this.flammability = flammability;
    }

    public void setInstallationType(InstallationType installationType) {
        Preconditions.checkNotNull(installationType);
        this.installationType = installationType;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setTid(Float tid) {
        this.tid = tid;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

}
