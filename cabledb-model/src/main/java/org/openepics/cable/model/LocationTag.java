/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of location tag.
 *
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Entity
public class LocationTag {

    @Id
    private String tag;
    private String description;

    /** Constructor for JPA entity. */
    private LocationTag() {}

    public LocationTag(String tag, String description) {
        Preconditions.checkNotNull(tag);
        Preconditions.checkNotNull(description);
        this.tag = tag;
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return tag + " (" + description + ")";
    }

}
