/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This represents an instance of routing row.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class RoutingRow extends Persistable {

    private static final long serialVersionUID = -6007734251143927822L;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_COLUMN_Routing"), name = "Routing", referencedColumnName = "id")
    private Routing routing;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_COLUMN_Cable"), name = "Cable", referencedColumnName = "id")
    private Cable cable;

    private int position;

    /** Constructor for JPA entity. */
    public RoutingRow() {}

    /**
     * Constructs new routing row.
     *
     * @param routing
     *            routing
     * @param cable
     *            cable
     * @param position
     *            position
     */
    public RoutingRow(Routing routing, Cable cable, int position) {
        this.routing = routing;
        this.cable = cable;
        this.position = position;
    }

    /** @return routing row (display view) */
    public Routing getRouting() {
        return routing;
    }

    /** @return routing row cable */
    public Cable getCable() {
        return cable;
    }

    /** @return routing row position */
    public int getPosition() {
        return position;
    }

    /**
     * @param position
     *            routing row position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * @param routing
     *            to set
     */
    public void setRouting(Routing routing) {
        this.routing = routing;
    }

    public void setCable(Cable cable) {
        this.cable = cable;
    }

    @Override
    public String toString() {
        return routing.getName();
    }

}
