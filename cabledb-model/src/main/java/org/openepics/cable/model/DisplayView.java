/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

/**
 * This represents an instance of display view.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class DisplayView extends Persistable {

    private static final long serialVersionUID = -1758834602114783386L;

    private String description;
    private String owner;
    private Date created;
    private Date executed;
    @Enumerated(EnumType.STRING)
    private EntityType entityType;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name="displayview_displayviewcolumn",
            joinColumns = @JoinColumn( name="displayview_id"),
            inverseJoinColumns = @JoinColumn( name="columns_id")
        )
    private List<DisplayViewColumn> columns;

    /** Constructor for JPA entity. */
    public DisplayView() {}

    /**
     * Constructs display view.
     *
     * @param description
     *            display view description
     * @param entityType
     *            entity type
     * @param owner
     *            displayView owner
     * @param created
     *            display view creation date
     * @param columns
     *            display view columns
     */
    public DisplayView(String description, EntityType entityType, String owner, Date created,
            List<DisplayViewColumn> columns) {
        this.description = description;
        this.entityType = entityType;
        this.owner = owner;
        this.created = created;
        this.columns = columns;
    }

    /**
     * Updates display view execution date.
     *
     * @param executed
     *            executed
     */
    public void updateExecutionDate(Date executed) {
        this.executed = executed;
    }

    /** @return display view description. */
    public String getDescription() {
        return description;
    }

    /** @return display view owner. */
    public String getOwner() {
        return owner;
    }

    /** @return display view creation date. */
    public Date getCreated() {
        return created;
    }

    /** @return display view execution date. */
    public Date getExecuted() {
        return executed;
    }

    /** @return entityType */
    public EntityType getEntityType() {
        return entityType;
    }

    /** @return set of display view columns. */
    public List<DisplayViewColumn> getColumns() {
        return columns;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setExecuted(Date executed) {
        this.executed = executed;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public void setColumns(List<DisplayViewColumn> columns) {
        this.columns = columns;
    }

}
