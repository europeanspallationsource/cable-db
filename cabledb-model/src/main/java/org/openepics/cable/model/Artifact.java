/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.io.File;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * An artifact is a user-defined value that is attached to database entities such as a file
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
@Table(name = "artifact")
public class Artifact extends Persistable {

    private static final long serialVersionUID = 2926588619140123269L;

    private String name;
    private String description;
    private String uri;
    private String rooturi;
    protected Date modifiedAt = new Date(0L);
    protected String modifiedBy;

    @Transient
    private String newName;
    @Transient
    private byte[] importData;

    /** Constructor. */
    public Artifact() {}

    /**
     * Constructs a new artifact
     * 
     * @param name
     *            the name of the artifact
     * @param description
     *            the user specified description
     * @param uri
     *            the user specified URL
     * @param rootUri
     *            the uri to the storage folder
     */
    public Artifact(String name, String description, String uri, String rootUri) {
        this.name = name;
        this.description = description;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getRootUri() {
        return rooturi != null ? rooturi : File.separator;
    }

    public void setRootUri(String rootUri) {
        this.rooturi = rootUri;
    }

    /** @return The timestamp of the last modification of this database entity */
    public Date getModifiedAt() {
        return new Date(modifiedAt.getTime());
    }

    /**
     * The setter stores a new copy of the param.
     *
     * @param modifiedAt
     *            The timestamp of the last modification of this database entity
     */
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = new Date(modifiedAt.getTime());
    }

    /** @return The user performing the last modification of the database entity */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The user performing the last modification of the database entity
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public boolean equals(Object object) {
        if ((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }

        Artifact other = (Artifact) object;
        if (this.id == null && other.id != null) {
            return false;
        }

        // return true for the same DB entity
        if (this.id != null) {
            return this.id.equals(other.id);
        }

        return this == object;
    }

    /** @return newName the name that will be set to artifact after save */
    public String getNewName() {
        return newName != null ? newName : getName();
    }

    /** Sets artifact name to name of uploaded file. */
    public void updateName() {
        setName(newName);
    }

    public byte[] getImportData() {
        return importData;
    }

    /**
     * Uploads file to be saved in the Artifact
     * 
     * @param data
     *            the file data
     * @param fileName
     *            the file name
     */
    public void uploadFile(byte[] data, String fileName) {
        this.importData = data;
        this.newName = fileName;
    }
    

    @Override
    public String toString() {
        return getName();
    }

}
